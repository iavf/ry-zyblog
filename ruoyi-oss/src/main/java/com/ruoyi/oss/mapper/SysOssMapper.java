package com.ruoyi.oss.mapper;

import com.ruoyi.common.core.dao.BaseMapper;
import com.ruoyi.oss.domain.SysOss;
import org.springframework.stereotype.Component;

/**
 * 文件上传
 */
@Component
public interface SysOssMapper extends BaseMapper<SysOss>
{
}
