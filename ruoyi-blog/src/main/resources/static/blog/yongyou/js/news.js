;var baseUrl = "https://mk.yonyou.com/content-service/";
function getNewsList(pages, size, categories, isAuthor) {
  $.ajax({
    "type": "GET",
    "url": baseUrl + "cnt_article_net/page?status=1&categories=" + categories + "&page=" + pages + "&size=" + size,
    "success": function(res) {
      if(res.code == '0'){
        var newsHtml = "";
        res.data.content.forEach(function(item, index){
          newsHtml += '<div class="section2-item section2-item-screen-420">';
          newsHtml += '<div class="section2-item-left">';
          var linkurl = '';
          if (item.linkUrl == '') {
            linkurl = '<a href="/news/news_detail.html?id='+ item.id +'">';
          } else {
            linkurl = '<a onclick="viewNews(\''+ item.id +'\');"  target="_blank" href="'+ item.linkUrl +'">';
          }
          newsHtml += '<i class="section2-item-icon"></i>' + linkurl + item.title + '</a>';
          newsHtml += '</div>';
          var publishName = '';
          if (isAuthor) {
            publishName = '<span class="section2-item-publishing">'+ item.authorNickName +'</span>';
          } else {
            // publishName = '<span class="section2-item-publishing">'+ item.authorNickName +'浏览</span>';
          }
          newsHtml += '<div class="section2-item-right section2-item-right-screen-420">' + publishName + convertToDatetime(item.gmtCreate, 5) +'</div>';
          newsHtml += '</div>';
        });
        $('#news_list').html(newsHtml);
        if(res.data.totalElements > size) {
          function yeshu(all) {
            var aa = Math.ceil(all / size)
            return aa
          }
          // 设置分页
          var h_options = {
            currentPage: pages, //当前的请求页面。
            totalPages: Math.ceil(res.data.totalElements / size), //一共多少页。
            bootstrapMajorVersion: 3, //bootstrap的版本要求。
            alignment: 'center',
            numberOfPages: yeshu(res.data.totalElements), //控制分页按钮数量
            itemTexts: function(type, page, current) {
                //如下的代码是将页眉显示的中文显示我们自定义的中文。
                switch (type) {
                    case 'first':
                        return '首页'
                    case 'prev':
                        return '上一页'
                    case 'next':
                        return '下一页'
                    case 'last':
                        return '末页'
                    case 'page':
                        return page
                }
            },
            onPageClicked: function(event, originalEvent, type, page) {
              getNewsList(page, size, categories, isAuthor);
            }
          }
          $('#searchPage').bootstrapPaginator(h_options);
        }
      }
    }
  })
};
function getNewsDetail(id) {
  $.ajax({
    "type": "GET",
    "url": baseUrl + "cnt_article_net/" + id,
    "success": function(res) {
      if(res.code == '0'){
        $('#news_title').html(res.data.title);
        $('#news_date').html(convertToDatetime(res.data.gmtCreate, 5));
        $('#news_content').html(res.data.content);

        if (res.data.files.length > 0) {
          $('#pdf_button').attr('href', res.data.files[0].furl);
          $('#pdf_button').attr('target', '_blank');
        }
      }
    }
  })
  viewNews(id);
};
function getMaterialList(page, size, categories, id) {
  $.ajax({
    "type": "GET",
    "url": baseUrl + "doc_article_net/page?status=1&categories=" + categories + "&page=" + page + "&size=" + size,
    "success": function(res) {
      if(res.code == '0'){
        var materialHtml = "";
        var className = "";
        if (id == 'b2') {
          className = "yonyou-leader"
        }
        res.data.content.forEach(function(item, index){
          materialHtml += '<div class="section3-item '+ className +'  section3-item-screen-420">';
          materialHtml += '<div class="section3-logo">';
          materialHtml += '<img class="section3-img" src="'+ item.coverImg +'" alt="">';
          materialHtml += '</div>';
          materialHtml += '<div class="section3-bottom">';
          materialHtml += '<div class="section3-bottom-text1">'+ item.title +'</div>';
          materialHtml += '<div class="section3-bottom-text2"><i class="section3-download"></i><a target="_blank" href="'+ item.files[0].furl +'">下载</a></div>';
          // materialHtml += '<div class="section3-bottom-text3">文件大小 '+ item.files[0].fsize +'</div>';
          materialHtml += '</div></div>';
        });
        $('#' + id).html(materialHtml);
      }
    }
  })
};
function viewNews(id) {
  $.ajax({
    type: 'post',
    url: "https://mk.yonyou.com/marketcenter/log/guest/visit",
    contentType: 'application/json',
    data: JSON.stringify({
      c_id: id,
      c_type: 'article',
      type: 'browse'
    }),
    success: function(res) {

    }
  })
}
function GetQueryString (name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]);
  return null;
};
function format(value){
  return value >= 10 ? value : '0' + value
};
function convertToDatetime(time, type) {
  var result;
  if (time === '') {
    return result = '';
  }
  var date = new Date(time);
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var second = date.getSeconds();
  switch (type) {
    case 0:
      result = format(month) + '-' + format(day);
      break
    case 1:
      result = format(hours) + '-' + format(minutes);
      break
    case 2:
      result = year + '-' + format(month) + '-' + format(day);
      break
    case 3:
      result = year + '-' + format(month) + '-' + format(day) + ' ' + format(hours)+ ':' +format(minutes);
      break
    case 4:
      result = year + '-' + format(month) + '-' + format(day) + ' ' + format(hours)+ ':' +format(minutes)+ ':' +format(second);
      break
    case 5: // 2015年01月05日
      result = year + '年' + format(month) + '月' + format(day) + '日';
      break
  }
  return result;
};