// var _baseUrl = '//mk.yonyou.com:8080/';
function GetQueryString (name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]);
  return null;
};

function getQueryVariable (variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) { return pair[1]; }
  }
  return (false);
};

if (window.addEventListener) {
  window.addEventListener('message', function (event) {
    try {
      window.parent.postMessage({height: document.body.scrollHeight}, '*');
    }
    catch(err) { }
  }, false);
}
var _sys = GetQueryString('sys');

var _zhengshi = window.location.href.indexOf('https') == 0 ? '//mk.yonyou.com/' : '//mk.yonyou.com:8080/'

var _baseUrl = _sys ? '//10.10.17.15:50003/' : _zhengshi;
var _getInfo = 'formbuilder/applyform/get/';
var _submitInfo = 'formbuilder/applyFormSubmit/save';
var _outCity = 'opservice/opbasedocarea/allcityAbroad.json'
var _inCity = 'opservice/opbasedocarea/allcity.json';
var _channel = 'marketcenter/urlChannel/increase/urlChannel?channelkey=';


// 判断IE版本
function IEVersion () {
  var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
  var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
  var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
  var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
  if (isIE) {
    var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
    reIE.test(userAgent);
    var fIEVersion = parseFloat(RegExp["$1"]);
    if (fIEVersion == 7) {
      return 7;
    } else if (fIEVersion == 8) {
      return 8;
    } else if (fIEVersion == 9) {
      return 9;
    } else if (fIEVersion == 10) {
      return 10;
    } else {
      return 6;//IE版本<=7
    }
  } else if (isEdge) {
    return 'edge';//edge
  } else if (isIE11) {
    return 11; //IE11  
  } else {
    return -1;//不是ie浏览器
  }
}

// 判断是什么浏览器
function getBrowserName () {
  var explorer = navigator.userAgent;
  //firefox 
  if (explorer.indexOf("Firefox") >= 0) {
    return "Firefox";
  }
  //Chrome
  else if (explorer.indexOf("Chrome") >= 0) {
    return "Chrome";
  }
  //Opera
  else if (explorer.indexOf("Opera") >= 0) {
    return "Opera";
  }
  //Safari
  else if (explorer.indexOf("Safari") >= 0) {
    return "Safari";
  }
  //Netscape
  else if (explorer.indexOf("Netscape") >= 0) {
    return "Netscape";
  } else {
    return "PC";
  }
}

//判断是否是pc
function IsPC () {
  var userAgentInfo = navigator.userAgent;
  var Agents = ["Android", "iPhone",
    "SymbianOS", "Windows Phone",
    "iPad", "iPod"
  ];
  var flag = true;
  for (var v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      device_info = Agents[v];
      flag = false;
      break;
    }
  }
  // 再根据屏幕尺寸来进行判断
  return flag;
}


var fillInTime = new Date(); // 表单开始时间
var returnUrl = decodeURIComponent(getQueryVariable('returnUrl'));
var id = GetQueryString('id');
var channelId = GetQueryString('cid') ? GetQueryString('cid') : '';
var hasLoction = false;
var shengData = []; // 省数据
var cityData = []; // 城市数据

/**
 * 设置表单dom
 */
function makeDom (data) {
  if (data.dr == 1) {
    setErrorInfo('当前表单已删除，不可用!', '');
    return false;
  }
  if (data.status != '已发布') {
    setErrorInfo('当前表单未发布，不可用!', '');
    return false;
  }
  var _isSubmit = localStorage.getItem('issubmit' + data.id);
  if (_isSubmit && !data.multiple) {
    if (returnUrl && returnUrl != '' && returnUrl.indexOf('http') > -1) {
      window.location.href = returnUrl ;
    } else {
      $('#noRepeat').show();
      setErrorInfo('您已提交，无须重复提交!', '');
    }
    return false;
  }
  // 设置title
  $('#title').html(data.title);
  $('#des').html(data.des);
  $('#submitBtn').show();
  $('#submitBtn').html(data.btnText);

  if (data.webpage_title) {
    document.title = data.webpage_title;
  }

  if (data.headImg) {
    var img = '<img src="https://mk.yonyou.com/' + data.headImg + '" class="header-img" />';
    $('#headerImg').append(img);
    $('#main').css('padding-top', '0px');
  } else {
    $('#headerImg').remove();
  }

  var _afterPstr = data.commit_after_note ? data.commit_after_note : '感谢您对用友的关注，我们将在2个工作日内安排专家跟您联系。'
  $('#subAfterPstr').append(_afterPstr);

  /**
   * 制作选择城市
   */
  function makeLocation () {
    var _dataItems = '';
    // 国内国外
    _dataItems += '<select onchange="inCountryChange()" class="jilian" name="countrySelect" id="countrySelect">';
    _dataItems += '<option value="国内">国内</option>';
    _dataItems += '<option value="国外">国外</option>';
    _dataItems += '</select>';
    // 选择国家
    _dataItems += '<select class="jilian" onchange="inShengChange()" name="shengSelect" id="shengSelect">';
    _dataItems += '</select>';
    // 选择城市
    _dataItems += '<select class="jilian" name="citySelect" id="citySelect">';
    _dataItems += '</select>';
    return _dataItems;
  }

  /**
   * 制作下拉框
   */
  function makeSelect (item) {
    var _dataItems = '';
    _dataItems += '<select class="dange" name="' + item.i_id + '" id="' + item.i_id + '">';
    for (var index = 0; index < item.data.length; index++) {
      var element = item.data[index];
      _dataItems += '<option value="' + element.lable + '">' + element.lable + '</option>';
    }
    _dataItems += '</select>';
    return _dataItems;
  }

  /**
   * 制作 单选和多选
   */
  function makeChoose (id, item, isCheckbox) {
    var _dataItems = '';
    for (var index = 0; index < item.data.length; index++) {
      var element = item.data[index];
      var _type = isCheckbox ? 'checkbox' : 'radio';
      _dataItems += '<label title="' + element.lable + '" class="label-font"><input name="' + id + '" type="' + _type + '" value="' + index + '" />' + element.lable + '</label> ';
    }
    return _dataItems;
  }

  var _dataItems = "";
  for (var index = 0; index < data.item.length; index++) {
    // 不是对用户隐藏的才可以进行展示
    var item = data.item[index];
    if (!item.userHide) {
      _dataItems += '<div class="option">';
      _dataItems += '<div>';
      if (IsPC() || item.type == 'radio' || item.type == 'sex' || item.type == 'select' || item.type == 'checkbox') {
        // 是否必填
        if (item.required) {
          _dataItems += '<span class="required-xing">*</span>';
        }
        if (item.type != 'explain') {
          _dataItems += '<span>' + item.title + '</span>';
        } else {
          _dataItems += '<span class="explain-title">说明控件</span>';
        }

      }
      _dataItems += '</div>';
      _dataItems += '<div>';
      var _placehoder = item.data[0].emptyText ? item.data[0].emptyText : '请输入' + item.title;
      // 输入框
      if (item.type == 'name'
        || item.type == 'phone'
        || item.type == 'email'
        || item.type == 'company'
        || item.type == 'age'
        || item.type == 'job'
        || item.type == 'input') {
        _dataItems += '<input id="' + item.i_id + '" type="text" placeholder="' + _placehoder + '">';
      }
      // 地区三级联动
      if (item.type == 'location') {
        hasLoction = true;
        _dataItems += makeLocation(item);
      }
      // 说明
      if (item.type == 'explain') {
        hasLoction = true;
        _dataItems += item.title;
      }
      // 单选和多选
      if (item.type == 'radio' || item.type == 'sex' || item.type == 'checkbox') {
        _dataItems += makeChoose(item.i_id, item, item.type == 'checkbox' ? true : false);
      }
      // select
      if (item.type == 'select') {
        _dataItems += makeSelect(item);
      }

      // date
      if (item.type == 'date') {
        _dataItems += '<input id="' + item.i_id + '" type="date" placeholder="' + _placehoder + '">';
      }

      // texteare
      if (item.type == 'textarea') {
        _dataItems += '<textarea id="' + item.i_id + '" placeholder="' + _placehoder + '" rows="3" cols="20"></textarea>';
      }

      _dataItems += '</div>';
      _dataItems += '<div></div>';
      _dataItems += '</div>';
    }
  }
  $('#mainContent').html(_dataItems);
  // 如果有地区选项就进行获取接口数据
  if (hasLoction) {
    $.ajax({
      url: _baseUrl + _inCity,
      type: 'GET',
      contentType: 'application/json',
      success: function (citydata) {
        if (citydata.flag == 0) {
          // 序列化数据
          for (var index = 0; index < citydata.data.length; index++) {
            var element = citydata.data[index];
            for (var j = 0; j < element.child.length; j++) {
              var initem = element.child[j];
              if (initem.name == '市辖区') {
                element.child = [
                  {
                    code: "zhixia",
                    id: "zhixia",
                    name: element.name,
                  }
                ]
                break;
              }
            }
          }
          shengData = citydata.data;
          makeInSheng();
          makeInCity(0);
        }
      }
    });
  }
  try {
    window.parent.postMessage({height:document.body.scrollHeight}, '*');
  }
  catch(err) { }
};

/**
 * 制作国内省份
 */
function makeInSheng () {
  var _str = '';
  for (var index = 0; index < shengData.length; index++) {
    var element = shengData[index];
    _str += '<option value="' + index + '">' + element.name + '</option>';
  }
  $('#shengSelect').html(_str);
};

/**
 * 制作国内城市
 */
function makeInCity (index) {
  var _str = '';
  for (var j = 0; j < shengData[index].child.length; j++) {
    var element = shengData[index].child[j];
    _str += '<option value="' + element.name + '">' + element.name + '</option>';
  }
  $('#citySelect').html(_str);
};
/**
 * 国内选择省份
 */
function inShengChange () {
  var _index = $('#shengSelect')[0].value;
  makeInCity(_index);
};


/**
 * 国内外切换
 */
function inCountryChange () {
  var _type = $('#countrySelect')[0].value;
  if (_type == '国内') {
    $("#shengSelect").show();
    makeInSheng();
    makeInCity(0);
  } else {
    $("#shengSelect").hide();
    // 获取国外信息
    $.ajax({
      url: _baseUrl + _outCity,
      type: 'GET',
      contentType: 'application/json',
      success: function (result) {
        if (result.flag == 0) {
          var _str = '';
          for (var j = 0; j < result.data.length; j++) {
            var element = result.data[j];
            _str += '<option value="' + element.name + '">' + element.name + '</option>';
          }
          $('#citySelect').html(_str);
        }
      }
    });
  }
};
// 整个表单的实体
var bigData = {};
/**
 * 提交信息
 */
$("#submitBtn").click(function () {
  for (var index = 0; index < bigData.item.length; index++) {
    var item = bigData.item[index];
    // 不是对用户隐藏的才可以进行展示
    if (!item.userHide) {
      // 输入框
      if (item.type == 'name'
        || item.type == 'email'
        || item.type == 'company'
        || item.type == 'age'
        || item.type == 'job'
        || item.type == 'input' || item.type == 'textarea' || item.type == 'select' || item.type == 'date') {
        if (item.required) {
          if (!$("#" + item.i_id)[0].value) {
            setErrorInfo(item.title + '不可为空!', item.i_id);
            return false;
          }
        }
        // 赋值
        item.data[0].value = $("#" + item.i_id)[0].value;
      }
      // 如果是手机号的话，进行精确的认证
      if (item.type == 'phone') {
        var phoneNum = $("#phone")[0].value;
        if (phoneNum.length < 6 || phoneNum.length > 15) {
          setErrorInfo('请输入正确的11位手机号!', item.i_id);
          return false;
        }
        /* var verifi = new RegExp("^0?(13[0-9]|15[012356789]|16[012356789]|17[013678]|18[0-9]|14[57])[0-9]{8}$");
        if (!verifi.test(phoneNum)) {
          setErrorInfo('请输入正确的11位手机号!', item.i_id);
          return false;
        } */
        // 赋值
        item.data[0].value = phoneNum;
      }
      // 地区
      if (item.type == 'location') {
        var _country = $("#countrySelect")[0].value;
        var _city = $("#citySelect")[0].value;
        // 赋值
        item.data[0].value = _country + '-' + _city;
      }
      // 单选框
      if (item.type == 'sex' || item.type == 'radio') {
        var _radioValue = $("input[name='" + item.i_id + "']:checked").length > 0 ? $("input[name='" + item.i_id + "']:checked")[0].value : '';
        if (item.required) {
          if (!_radioValue) {
            setErrorInfo('请选择' + item.title, '');
            return false;
          }
        }
        // 赋值
        if (_radioValue) {
          for (var j = 0; j < item.data.length; j++) {
            var element = item.data[j];
            element.checked = false;
          }
          item.data[_radioValue].checked = true;
        }
      }
      // 多选框
      if (item.type == 'checkbox') {
        var _checkboxData = $("input[name='" + item.i_id + "']:checked");
        if (_checkboxData.length > 0) {
          for (var p = 0; p < _checkboxData.length; p++) {
            var element = _checkboxData[p];
            // 赋值
            item.data[element.value].checked = true;
          }
        } else {
          // 多选的必填
          if (item.required) {
            setErrorInfo('请选择' + item.title, '');
            return false;
          }
        }
      }

    }
  }
  var browser = '';
  if (IsPC()) {
    var _version = IEVersion();
    if (_version > 1) {
      browser = "IE" + _version;
    } else {
      browser = getBrowserName();
    }
  } else {
    if (browser == "") {
      browser = "phone";
    }
  };
  // 设置提交表单信息
  setErrorInfo('', '');
  var _subInfo = {
    applyform_id: bigData.id, // 申请表单id
    browser: browser, // 所用浏览器
    fillInTime: fillInTime, // 开始时间
    fillOutTime: new Date(), // 结束时间
    item: bigData.item, // 问题列表
    channel: channelId // 渠道
  }
  // alert('符合要求，该请求了！');
  $.ajax({
    url: _baseUrl + _submitInfo,
    data: JSON.stringify(_subInfo),
    type: 'POST',
    EncType: 'text/plain',
    contentType: 'text/plain',
    success: function (result) {
      if (result.flag == 0) {
        // 这里设置已经提交过
        localStorage.setItem('issubmit' + bigData.id, 'true');
        // 调取父页面方法
        try {
          window.parent.postMessage({isSubmit: true}, '*');
        }
        catch(err) { }
        try {
          _hmt.push(['_trackEvent', 'Form', bigData.id, channelId ? channelId : '全渠道']);
          if (returnUrl && returnUrl != '' && returnUrl.indexOf('http') > -1) {
            window.location.href = returnUrl;
            alert('提交成功！');
          } else {
            $('#title').hide();
            $('#des').hide();
            $('#errorTips').hide();
            $('#mainContent').hide();
            $('#submitBtn').hide();
            // 显示结果
            $('#subAfter').show();
          }
        } catch (error) {
        }
      } else {
        alert(result.desc);
      }
    },
    error: function (res) {
      if (res.responseJSON) {
        if (res.responseJSON.message && res.responseJSON.message == '不允许重复提交' && returnUrl && returnUrl != '' && returnUrl.indexOf('http') > -1) {
          window.location.href = returnUrl;
        } else {
          alert(res.responseJSON.message);
        }
      }
    }
  });
  // alert('结束');
});



/**
 * 设置错误信息
 */
function setErrorInfo (textLable, id) {
  $('#errorTips').html(textLable);
  if (id) {
    $("#" + id).focus();
  }
};

(function () {
  $.ajax({
    url: _baseUrl + _getInfo + id,
    type: 'GET',
    contentType: 'text/plain',
    success: function (result) {
      if (result.flag == 0) {
        bigData = result.data;
        makeDom(result.data);
      }
    }
  });
  // 如果有渠道id，就进行提交浏览量
  if (channelId) {
    $.ajax({
      url: _baseUrl + _channel + channelId,
      type: 'GET',
      contentType: 'text/plain',
      success: function () { }
    });
  }
})()