/*
 * Legacy browser support
 */
[].map||(Array.prototype.map=function(g,h){for(var i=this,j=i.length,k=new Array(j),l=0;j>l;l++){l in i&&(k[l]=g.call(h,i[l],l,i))}return k}),[].filter||(Array.prototype.filter=function(h){if(null==this){throw new TypeError}var i=Object(this),j=i.length>>>0;if("function"!=typeof h){throw new TypeError}for(var k=[],l=arguments[1],m=0;j>m;m++){if(m in i){var n=i[m];h.call(l,n,m,i)&&k.push(n)}}return k}),[].indexOf||(Array.prototype.indexOf=function(f){if(null==this){throw new TypeError}var g=Object(this),h=g.length>>>0;if(0===h){return -1}var i=0;if(arguments.length>1&&(i=Number(arguments[1]),i!=i?i=0:0!==i&&i!=1/0&&i!=-(1/0)&&(i=(i>0||-1)*Math.floor(Math.abs(i)))),i>=h){return -1}for(var j=i>=0?i:Math.max(h-Math.abs(i),0);h>j;j++){if(j in g&&g[j]===f){return j}}return -1});
/*
 * Cross-Browser Split 1.1.1
 * Copyright 2007-2012 Steven Levithan <stevenlevithan.com>
 * Available under the MIT License
 * http://blog.stevenlevithan.com/archives/cross-browser-split
 */
var nativeSplit=String.prototype.split,compliantExecNpcg=void 0===/()??/.exec("")[1];String.prototype.split=function(k,l){var m=this;if("[object RegExp]"!==Object.prototype.toString.call(k)){return nativeSplit.call(m,k,l)}var n,o,p,q,r=[],s=(k.ignoreCase?"i":"")+(k.multiline?"m":"")+(k.extended?"x":"")+(k.sticky?"y":""),t=0;for(k=new RegExp(k.source,s+"g"),m+="",compliantExecNpcg||(n=new RegExp("^"+k.source+"$(?!\\s)",s)),l=void 0===l?-1>>>0:l>>>0;(o=k.exec(m))&&(p=o.index+o[0].length,!(p>t&&(r.push(m.slice(t,o.index)),!compliantExecNpcg&&o.length>1&&o[0].replace(n,function(){for(var b=1;b<arguments.length-2;b++){void 0===arguments[b]&&(o[b]=void 0)}}),o.length>1&&o.index<m.length&&Array.prototype.push.apply(r,o.slice(1)),q=o[0].length,t=p,r.length>=l)));){k.lastIndex===o.index&&k.lastIndex++}return t===m.length?(q||!k.test(""))&&r.push(""):r.push(m.slice(t)),r.length>l?r.slice(0,l):r};