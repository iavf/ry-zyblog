package com.ruoyi.cms.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.cms.domain.CmsDonate;
import com.ruoyi.cms.service.impl.DonateServiceImpl;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文章管理Controller
 *
 * @author wujiyue
 * @date 2019-10-28
 */
@Controller
@RequestMapping("/cms/donate")
public class CmsDonateController extends BaseController {
    private String prefix = "cms/donate";


    @Autowired
    DonateServiceImpl donateService;


    @RequiresPermissions("cms:donate:view")
    @GetMapping()
    public String article() {
        return prefix + "/donate";
    }

    /**
     * 查询文章管理列表
     */
    @RequiresPermissions("cms:donate:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsDonate cmsDonate) {
        startPage();
        List<CmsDonate> list = donateService.selectMoneyList(cmsDonate);
        return getDataTable(list);
    }

    /**
     * 导出文章管理列表
     */
    @RequiresPermissions("cms:donate:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsDonate cmsDonate) {
        List<CmsDonate> list = donateService.selectMoneyList(cmsDonate);
        ExcelUtil<CmsDonate> util = new ExcelUtil<CmsDonate>(CmsDonate.class);
        return util.exportExcel(list, "cmsDonate");
    }

    /**
     * 新增打赏管理
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {

        return prefix + "/add";
    }

    /**
     * 新增保存文章管理
     */
    @RequiresPermissions("cms:donate:add")
    @Log(title = "打赏管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsDonate cmsDonate) {
        return toAjax(donateService.insert(cmsDonate));
    }
//

    /**
     * 修改打赏管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        CmsDonate cmsDonate = donateService.selectByPrimaryKey(Integer.parseInt(id));
        mmap.put("cmsDonate", cmsDonate);
        return prefix + "/edit";

    }

    /**
     * 修改保存文章管理
     */
    @RequiresPermissions("cms:donate:edit")
    @Log(title = "打赏管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsDonate cmsDonate) {
        return toAjax(donateService.updateByPrimaryKeySelective(cmsDonate));
    }

    /**
     * 收款展示列表
     */
    @GetMapping("/donateList")
    @ResponseBody
    public JSONObject donateList(CmsDonate cmsDonate) {
        JSONObject javaResoultObj = new JSONObject();
        javaResoultObj.put("code", 0);
        javaResoultObj.put("msg", "true");
        javaResoultObj.put("count", 3);
        JSONArray data = new JSONArray();

        List<CmsDonate> list = donateService.selectDonateList(cmsDonate);
        JSONArray array= JSONArray.parseArray(JSON.toJSONString(list));
        javaResoultObj.put("data", array);
        return javaResoultObj;
    }

    /**
     * 收款展示列表
     */
    @GetMapping("/payList")
    @ResponseBody
    public JSONObject payList(CmsDonate cmsDonate) {
        JSONObject javaResoultObj = new JSONObject();
        javaResoultObj.put("code", 0);
        javaResoultObj.put("msg", "true");
        javaResoultObj.put("count", 3);
        JSONArray data = new JSONArray();

        cmsDonate.setMoneyType("-");
        List<CmsDonate> list = donateService.selectPayList(cmsDonate);
        JSONArray array= JSONArray.parseArray(JSON.toJSONString(list));
        javaResoultObj.put("data", array);
        return javaResoultObj;
    }



//
//    /**
//     * 删除文章管理
//     */
//    @RequiresPermissions("cms:donate:remove")
//    @Log(title = "文章管理", businessType = BusinessType.DELETE)
//    @PostMapping("/remove")
//    @ResponseBody
//    public AjaxResult remove(String ids) {
//        return toAjax(articleService.deleteArticleByIds(ids));
//    }
//
//    @RequestMapping("/detail/{id}")
//    public String articleDetail(@PathVariable String id, Model model) {
//
//        Article article = articleService.selectArticleById(id);
//        if (article == null) {
//            throw new BusinessException("您要访问的文章不存在!");
//        }
//        String articleModel = article.getArticleModel();//文章模型
//        if (ArticleModel.DUOGUYU.getVal().equals(articleModel)) {
//            List<Tags> fullTabs = tagsService.selectBlogTabs();
//            model.addAttribute("fullTabs", fullTabs);
//            String tagIds = article.getTags();
//            if (StringUtils.isNotEmpty(tagIds)) {
//                String[] arr = Convert.toStrArray(tagIds);
//                List<Tags> tagsList = new ArrayList<Tags>();
//                Tags tmp = null;
//                for (String tid : arr) {
//                    //检测每个标签再数据库cms_tag表中是否存在（根据名称），如果存在记下id，不存在则新增并记下id
//                    tmp = tagsService.selectTagsById(Long.valueOf(tid));
//                    if (tmp != null) {
//                        tagsList.add(tmp);
//                    }
//                }
//                model.addAttribute("tagsList", tagsList);//这个值用于输出模板文件的标签
//            }
//            Map dataMap = JSONObject.parseObject(JSON.toJSONString(article), Map.class);
//            model.addAllAttributes(dataMap);
//        }
//        return prefix + "/article-duoguyu";
//
//    }


}
