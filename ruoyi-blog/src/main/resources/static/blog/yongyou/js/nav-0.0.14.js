$(function(){
    var header = '<div class="container">'+
    '<div class="headers">'+
						'<div class="header-left pull-left">'+
							'<a class="yongyou-logo" href="/" title="用友"><img class="logo-pc-box" src="/static_new/img/icon/logo.png"/><img class="logo-web-box" src=""/></a>'+
						'</div>'+
						'<nav>'+
							'<ul class="menu list-unstyled list-inline">'+
								'<li dataNav="product"><a title="产品与服务">产品与服务 <img class="xiala-hui" src="/static_new/img/icon/xiala.png" /><img class="xiala-hong" src="/static_new/img/icon/xiala_xuanzhong.png" /></a></li>'+
								'<li dataNav="solve"><a title="行业">行业 <img class="xiala-hui" src="/static_new/img/icon/xiala.png" /><img class="xiala-hong" src="/static_new/img/icon/xiala_xuanzhong.png" /></a></li>'+
								'<li dataNav="client"><a title="案例中心" href="/case/case.html">案例中心</a></li>'+
								'<li dataNav="market"><a title="云市场" href="https://market.yonyoucloud.com" target="_blank">云市场</a></li>'+
								'<li dataNav="presscenter"><a title="资讯中心">资讯中心<img class="xiala-hui" src="/static_new/img/icon/xiala.png" /><img class="xiala-hong" src="/static_new/img/icon/xiala_xuanzhong.png" /></li>'+
								'<li dataNav="activity"><a title="活动中心" href="http://subject.yonyou.com/event/" target="_blank">活动中心 </a></li>'+
								'<li dataNav="AboutUs"><a title="关于我们">关于我们<img class="xiala-hui" src="/static_new/img/icon/xiala.png" /><img class="xiala-hong" src="/static_new/img/icon/xiala_xuanzhong.png" /></a></li>'+
								'<li dataNav="paynews"><a title="收费资讯" href="/news_pay/biznews.html">收费资讯</a></li>'+
								'</ul>'+
						'</nav>'+
    `<button onclick='window.open("/contact.html");'>试用或购买</button>` +
				'</div>'+
				'<div class="menulist">'+
					// 菜单一
					'<div class="list" id="product" dataNav="product">'+
						'<div class="container container-twomenulist">'+
							'<ul class="menulist-left-ul">' +
								'<li>云服务</li>' +
								'<li>软件</li>' +
								'<li>数智金服</li>' +
							'</ul>' +
							'<ul class="main main-one">'+
								'<li>'+
									'<ul class="row">'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="领域云">领域云</b>'+
										'<a title="" target="_blank" href="https://ncc.yonyou.com">NC Cloud</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=13">U8 Cloud</a>'+
										'<a title="" target="_blank" href="https://jingzhi.yonyoucloud.com/#/" style="width:200px;">精智|工业互联网平台</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=3">营销云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=7">采购云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=1">财务云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=9">税务云</a>'+
										'<a title="" target="_blank" href="https://jingzhi.yonyoucloud.com">制造云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=4">人力云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=6">协同云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=10">通信云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=8">工程云</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=11">数据服务</a>'+
										'<a title="" target="_blank" href="https://www.yonyoucloud.com/product.php?id=12">APM服务</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2" style="margin-left:20px;"><b title="YonSuite">YonSuite</b>'+
											'<a title="" target="_blank" href="https://yonsuite.diwork.com/#/product">营销电商一体化</a>'+
											'<a title="" target="_blank" href="https://yonsuite.diwork.com/#/product">产销供一体化</a>'+
											'<a title="" target="_blank" href="https://yonsuite.diwork.com/#/product">业财税一体化</a>'+
											'<a title="" target="_blank" href="https://yonsuite.diwork.com/#/product">人力协同一体化</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="iuap云平台">iuap云平台</b>'+
											'<a title="" target="_blank" href="https://iuap.yonyoucloud.com/">云平台</a>'+
											'<a title="" target="_blank" href="https://market.yonyoucloud.com">云市场</a>'+
											'<a title="" target="_blank" href="https://apcenter.yonyoucloud.com/apptenant/cloud">企业服务中心</a>'+
											'<a title="" target="_blank" href="http://udn.yyuap.com/">UDN社区</a>'+
											'<a title="" target="_blank" href="https://open.diwork.com">开放平台</a>'+
											'<a title="" target="_blank" href="https://rpa.diwork.com">智多星</a>'+
											'<a title="" target="_blank" href="http://www.ublinker.com">友企联</a>'+
											'<a title="" target="_blank" href="https://api.yonyoucloud.com">APILink</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="行业云">行业云</b>'+
											'<a title="" target="_blank" href="http://www.yonyougov.com">用友政务云</a>'+
											'<a title="" target="_blank" href="https://jingzhi.yonyoucloud.com">用友制造云</a>'+
											'<a title="" target="_blank" href="http://hit.yonyou.com">用友医疗云</a>'+
											'<a title="" target="_blank" href="https://yangke.yonyou.com/">国资企业云</a>'+
											'<a title="" target="_blank" href="https://www.yonyouccs.com/icop-website/">用友建造云</a>'+
											'<a style="display: none;" title="" target="_blank" href="http://www.yonyoufintech.com/">用友金融云</a>'+
											'<a title="" target="_blank" href="http://auto.yonyou.com">用友汽车云</a>'+
											'<a title="" target="_blank" href="http://www.seentao.com">用友教育云</a>'+
											'<a title="" target="_blank" href="http://www.honghuotai.com/#/index">用友餐饮云</a>'+
											'<a title="" target="_blank" href="https://www.uf-tobacco.com/index.html">用友烟草云</a>'+
											'<a title="" target="_blank" href="http://yonyouny.com/">用友能源云</a>'+
											'<a title="" target="_blank" href="https://market.yonyoucloud.com/market/shop/e7616d0d-9962-46eb-848f-0b306af0a767">用友广信</a>'+

										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="小微企业云">小微企业云</b>'+
											'<a title="" target="_blank" href="https://www.xinfushe.com">薪福社</a>'+
											'<a title="" target="_blank" href="https://www.chanpay.com/">畅捷支付</a>'+
											'<a title="" target="_blank" href="https://www.chanjet.com">畅捷通</a>'+
											'<a title="" target="_blank" href="https://h.chanjet.com">畅捷通好会计</a>'+
											'<a title="" target="_blank" href="https://hsy.chanjet.com">畅捷通好生意</a>'+
											'<a title="" target="_blank" href="https://tcloud.chanjet.com">畅捷通T+ Cloud</a>'+
											'<a title="" target="_blank" href="https://d.chanjet.com">畅捷贷</a>'+
										'</li>'+

									'</ul>'+
								'</li>'+
								'<li>'+
									'<ul class="row">'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="面向大型企业">面向大型企业</b>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/product.php?id=4">用友NC6</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/product.php?id=5">用友U9</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/product.php?id=6">用友智能工厂</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/product.php?id=7">用友HCM</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="面向中型企业">面向中型企业</b>'+
											'<a title="" target="_blank" href="http://u8.yonyou.com">用友U8+</a>'+
											'<a title="" target="_blank" href="http://u8.yonyou.com/index.php/Home/Product/detail.html?id=20">用友PLM</a>'+
											'<a title="" target="_blank" href="http://u8.yonyou.com/index.php/Home/Product/detail.html?id=19">用友CRM</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2"><b title="面向小微企业">面向小微企业</b>'+
											'<a title="" target="_blank" href="http://software.chanjet.com/rj-Tg.html">畅捷通T+</a>'+
											'<a title="" target="_blank" href="http://software.chanjet.com/rj-T1.html">畅捷通T1</a>'+
											'<a title="" target="_blank" href="http://software.chanjet.com/rj-T3.html">畅捷通T3</a>'+
											'<a title="" target="_blank" href="http://software.chanjet.com/rj-T6.html">畅捷通T6</a>'+
										'</li>'+
										'<li class="col-md-3 col-sm-3 col-xs-3"><b title="行业">行业</b>'+
										'<div class="maincol6 col-md-6 col-sm-6 col-xs-6">'+
											'<a title="" target="_blank" href="http://www.yonyougov.com">政务</a>'+
											'<a title="" target="_blank" href="https://jingzhi.yonyoucloud.com">工业</a>'+
											'<a title="" target="_blank" href="http://hit.yonyou.com">医疗</a>'+
											'<a title="" target="_blank" href="https://www.yonyou.com/jianzhu">建筑</a>'+
											'<a style="display: none;" title="" target="_blank" href="http://www.yonyoufintech.com">金融</a>'+
											'<a title="" target="_blank" href="http://auto.yonyou.com">汽车</a>'+
											'<a title="" target="_blank" href="http://www.seentao.com">教育</a>'+
										'</div>'+
										'<div class="maincol6 col-md-6 col-sm-6 col-xs-6">'+
											'<a title="" target="_blank" href="http://www.honghuotai.com/#/index">餐饮</a>'+
											'<a title="" target="_blank" href="https://www.uf-tobacco.com/index.html">烟草</a>'+
											'<a title="" target="_blank" href="http://yonyouny.com">能源</a>'+
											'<a title="" target="_blank" href="https://item.yonyoucloud.com/13445329-5747-4a96-be20-2fc37f63620b.html">广电</a>'+
											'<a title="" target="_blank" href="https://www.yonyouaud.com/">审计</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=5">公用事业</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=9">智慧园区</a>'+
										'</div>'+
									'</ul>'+
								'</li>'+
								//金融
								'<li>'+
									'<ul class="row">'+
										'<li class="col-md-2 col-sm-2 col-xs-2">'+
											'<a title="" target="_blank" href="https://www.shufafin.com/zf/account.html">支付结算</a>'+
											'<a title="" target="_blank" href="https://www.yyfaxgroup.com">友金所</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2">'+
											'<a title="" target="_blank" href="https://www.shufafin.com/xd/ysd.html">供应链</a>'+
											'<a title="" target="_blank" href="http://www.yonyoubao.com">友企保</a>'+
											
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2">>'+
											'<a title="" target="_blank" href="https://www.shufafin.com/xj/yin.html">现金管理</a>'+
											'<a title="" target="_blank" href="https://www.yyfaxgroup.com">用友产投</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2">>'+
											'<a title="" target="_blank" href="https://www.shufafin.com/zx/credit.html">企业征信</a>'+
											'<a title="" target="_blank" href="http://www.ytabx.com">友太安</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2">>'+
											'<a title="" target="_blank" href="https://www.shufafin.com/fk/linkdata.html">数据风控</a>'+
											'<a title="" target="_blank" href="http://www.yonyoufintech.com/">科技服务</a>'+
										'</li>'+
										'<li class="col-md-2 col-sm-2 col-xs-2">>'+
											'<a title="" target="_blank" href="https://www.imfbp.com/ifbp-mall/">链融云</a>'+
											
										'</li>'+
									'</ul>'+
								'</li>'+
								'<li>'+
								'</li>'+
							'</ul>'+
						'</div>'+
						'<div class="stern">'+
							'<h3>需要服务支持？</h3>'+
							'<ul id="server_txt">' +
								'<li><a target="_brank" href="https://euc.yonyoucloud.com/register?sysid=market&service=https://market.yonyoucloud.com/market/invite/invite_h5.html?invite">云市场入驻</a></li>' +
								'<li><a target="_brank" href="https://market.yonyoucloud.com">加入合作伙伴</a></li>' +
								'<li><a target="_brank" href="http://partner.yonyouup.cn/ForOtherSystem/PartnerSearchForOfficialWebSite.aspx">查询授权经销商</a></li>' +
								'<li><a target="_brank" href="https://developer.yonyoucloud.com/fe/fe-portal/index.html#/dashboard">开发者中心</a></li>' +
								'<li><a target="_brank" href="http://nc.yonyou.com/services.php">用友iSM Cloud</a></li>' +
								'<li><a target="_brank" href="http://fwq.yonyou.com/up_service/index.php?r=up_shop/index">用友U8服务圈</a></li>' +
								'<li><a target="_brank" href="https://service.chanjet.com/">畅捷通服务社区</a></li>' +
							'</ul>'+
						'</div>'+
					'</div>'+
					// 菜单二
					'<div class="list" id="solve" dataNav="solve" style="width: 570px; margin-left:100px;">'+
						'<div class="container container-twomenulist">'+
							'<ul class="menulist-left-ul">' +
								'<li>行业</li>' +
							'</ul>' +
							'<ul class="main main-two row main-right-ul">'+
								//行业
								'<li>'+
									'<ul class="row">'+
										'<li class="col-md-3 col-sm-2 col-xs-2">'+
											'<a title="" target="_blank" href="http://www.yonyougov.com">政务</a>'+
											'<a title="" target="_blank" href="https://jingzhi.yonyoucloud.com">工业</a>'+
											'<a title="" target="_blank" href="http://hit.yonyou.com">医疗</a>'+
											'<a title="" target="_blank" href="https://www.yonyou.com/jianzhu">建筑</a>'+
											'<a title="" target="_blank" href="http://www.seentao.com">教育</a>'+
										'</li>'+
										'<li class="col-md-3 col-sm-2 col-xs-2">'+
											
											'<a title="" target="_blank" href="http://www.honghuotai.com/#/index">餐饮</a>'+
											'<a title="" target="_blank" href="https://www.uf-tobacco.com/index.html">烟草</a>'+
											'<a title="" target="_blank" href="http://yonyouny.com">能源</a>'+
											'<a title="" target="_blank" href="https://item.yonyoucloud.com/13445329-5747-4a96-be20-2fc37f63620b.html">广电</a>'+
											'<a title="" target="_blank" href="https://www.yonyoucloud.com/cloud_sales.html">零售</a>'+
										'</li>'+
										'<li class="col-md-3 col-sm-2 col-xs-2">>'+
										
											
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=7">传媒</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=8">房地产</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=4">交通运输</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=42">现代农业</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=9">智慧园区</a>'+
										'</li>'+
										'<li class="col-md-3 col-sm-2 col-xs-2">>'+
											
											'<a title="" target="_blank" href="http://auto.yonyou.com">汽车</a>'+
											'<a title="" target="_blank" href="https://www.yonyouaud.com">审计</a>'+
											'<a title="" target="_blank" href="http://nc.yonyou.com/project.php?stype=1&id=5">公用事业</a>'+
										'</li>'+
									'</ul>'+
								'</li>'+
							'</ul>'+
						'</div>'+
					'</div>'+
					// 菜单三 客户成功
					// '<div class="list" id="client" dataNav="client">' + // id="customercase" dataNav="customercase"
						// '<div class="container container-twomenulist">'+
							// '<ul class="menulist-left-ul">' +
								// '<li>行业</li>' +
								// '<li>制造</li>' +
								// '<li>餐饮</li>' +
								// '<li>汽车</li>' +
								// '<li>电信</li>' +
								// '<li>金融</li>' +
								// '<li>城市建设</li>' +
								// '<li>应用</li>' +
							// '</ul>' +
							// '<ul class="main main-two row main-right-ul">'+
							// '</ul>'+
						// '</div>'+
					// '</div>'+
					// 菜单四
					'<div class="list" id="presscenter" dataNav="presscenter" style="width: 570px; margin-left: 400px;">'+
						'<div class="container container-twomenulist">'+
							'<ul class="menulist-left-ul">' +
								'<li>资讯中心</li>' +
							'</ul>' +
							'<ul class="main main-two row main-right-ul">'+
								'<li>'+
									'<ul class="row">'+
											'<li class="col-md-3 col-sm-2 col-xs-2">'+
												'<a href="/news/news.html">资讯发布</a>' +
											'</li>'+
											'<li class="col-md-3 col-sm-2 col-xs-2">'+
												'<a href="/news/news_base.html">媒体资料库</a>' +
											'</li>'+
											
											
											
									'</ul>'+
									
									'<ul class="row">'+
										'<li class="col-md-3 col-sm-2 col-xs-2">'+
											'<a href="/news/news_media.html">媒体报道</a>' +
										'</li>'+
										'<li class="col-md-3 col-sm-2 col-xs-2">'+
											'<a href="/news/news_video.html">用友视讯</a>' +
										'</li>'+
									'</ul>'+
									
									'<ul class="row">'+
										'<li class="col-md-3 col-sm-2 col-xs-2">'+
											'<a href="/news/news_social.html">社交媒体</a>' +
										'</li>'+
									'</ul>'+
								'</li>'+
							'</ul>'+
						'</div>'+
					'</div>'+
					// 菜单五
					'<div class="list" id="activity1" dataNav="activity1">'+
						'<div class="container container-twomenulist">'+
							'<ul class="menulist-left-ul">' +
								'<li>活动中心</li>' +
							'</ul>' +
							'<ul class="main main-two row main-right-ul">'+
								'<li>'+
									'<ul class="row">'+
											'<li class="col-md-2 col-sm-2 col-xs-2">'+
												'<a target="_brank" href="http://subject.yonyou.com/event">最新活动</a>' +
											'</li>'+
											'<li class="col-md-3 col-sm-3 col-xs-3">'+
												'<a target="_brank" href="http://subject.yonyou.com/zt/summit2020">2020企业数智化峰会</a>' +
											'</li>'+
											'<li class="col-md-2 col-sm-2 col-xs-2">'+
												'<a target="_brank" href="http://subject.yonyou.com/event/html/networklive.html">网络直播</a>' +
											'</li>'+
									'</ul>'+
								'</li>'+
							'</ul>'+
						'</div>'+
					'</div>'+
					// 菜单六
					'<div class="list" id="AboutUs" dataNav="AboutUs" style="width: 570px; margin-left: 600px;">'+
						'<div class="container container-twomenulist">'+
							'<ul class="menulist-left-ul">'+
								'<li>关于我们</li>' +
							'</ul>'+
							'<ul class="main main-two row main-right-ul">'+
								'<li>'+
									'<ul class="row">'+
											'<li class="col-md-4 col-sm-2 col-xs-2">'+
												'<a target="_brank" href="https://www.yonyou.com/yy/guanyu.html?id=2&v=1.0.0">经营公告</a>' +
											'</li>'+
											'<li class="col-md-4 col-sm-2 col-xs-2">'+
												'<a target="_brank" href="https://www.yonyou.com/yy/guanyu.html?id=3">社会责任</a>' +
											'</li>'+
											
									'</ul>'+									
									'<ul class="row">'+
										'<li class="col-md-4 col-sm-2 col-xs-2">'+
											'<a target="_brank" href="https://www.yonyou.com/yy/guanyu.html?id=0">了解用友</a>' +
										'</li>'+
										'<li class="col-md-4 col-sm-2 col-xs-2">'+
											'<a target="_brank" href="http://career.yonyou.com">加入用友</a>' +
										'</li>'+
									'</ul>'+
									
									'<ul class="row">'+
										'<li class="col-md-4 col-sm-2 col-xs-2">'+
											'<a target="_brank" href="https://www.yonyou.com/yy/lianxiyonyou.html">全球办公室</a>' +
										'</li>'+
										
										'<li class="col-md-4 col-sm-2 col-xs-2">'+
												'<a target="_brank" href="https://www.yonyou.com/yy/dem.html">数智企业体验馆</a>' +
										'</li>'+
										
									'</ul>'+
									'<ul class="row">'+
										'<li class="col-md-4 col-sm-2 col-xs-2">'+
												'<a target="_brank" href="https://www.yonyou.com/yy/guanyu.html?id=4">企业荣誉</a>' +
											'</li>'+	
									'</ul>'+
								'</li>'+
							'</ul>'+
						'</div>'+
					'</div>'+

	
				'</div>';

	// 移动端菜单
	var samll = `<div class="am-offcanvas-bar">
					<ul class="am-list admin-sidebar-list" id="collapase-nav-1">
						<!-- 产品与服务 -->
						<li class="am-panel">
							<a data-am-collapse="{parent: '#collapase-nav-1',target: '#product_web'}" href="#/"><i
									class="am-margin-left-sm"></i> 产品与服务<i
									class="am-icon-angle-right am-fr am-margin-right"></i></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="product_web">
								
								
								<li>
									<a data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1'}">
										<i class="am-margin-left-sm"></i>云服务<i class="am-icon-angle-right am-fr am-margin-right"></i>
									</a>
									
									<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1">
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1_1'}">
												<i class="am-margin-left-sm"></i>
												领域云
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1_1">
												<li><a href="https://ncc.yonyou.com/"><i class="am-margin-left-sm"></i>NC Cloud</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=13"><i class="am-margin-left-sm"></i>U8 Cloud</a></li>
												<li><a href="https://jingzhi.yonyoucloud.com/#/"><i class="am-margin-left-sm"></i>精智|工业互联网</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=3"><i class="am-margin-left-sm"></i>营销云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=7"><i class="am-margin-left-sm"></i>采购云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=1"><i class="am-margin-left-sm"></i>财务云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=9"><i class="am-margin-left-sm"></i>税务云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=4"><i class="am-margin-left-sm"></i>人力云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=6"><i class="am-margin-left-sm"></i>协同云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=10"><i class="am-margin-left-sm"></i>通信云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=8"><i class="am-margin-left-sm"></i>工程云</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=11"><i class="am-margin-left-sm"></i>数据服务</a></li>
												<li><a href="https://www.yonyoucloud.com/product.php?id=12"><i class="am-margin-left-sm"></i>APM服务</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1_2'}">
												<i class="am-margin-left-sm"></i>
												YonSuite
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1_2">
												<li><a href="https://yonsuite.diwork.com/#/product"><i class="am-margin-left-sm"></i>营销电商一体化</a></li>
												<li><a href="https://yonsuite.diwork.com/#/product"><i class="am-margin-left-sm"></i>营销供一体化</a></li>
												<li><a href="https://yonsuite.diwork.com/#/product"><i class="am-margin-left-sm"></i>业财税一体化</a></li>
												<li><a href="https://yonsuite.diwork.com/#/product"><i class="am-margin-left-sm"></i>人力协同一体化</a></li>
												
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1_3'}">
												<i class="am-margin-left-sm"></i>
												iuap云平台
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1_3">
												<li><a href="https://iuap.yonyoucloud.com/"><i class="am-margin-left-sm"></i>云平台</a></li>
												<li><a href="https://market.yonyoucloud.com/"><i class="am-margin-left-sm"></i>云市场</a></li>
												<li><a href="https://apcenter.yonyoucloud.com/apptenant/cloud"><i class="am-margin-left-sm"></i>企业服务中心</a></li>
												<li><a href="http://udn.yyuap.com/"><i class="am-margin-left-sm"></i>UDN社区</a></li>
												<li><a href="https://open.diwork.com"><i class="am-margin-left-sm"></i>开放平台</a></li>
												<li><a href="https://rpa.diwork.com"><i class="am-margin-left-sm"></i>智多星</a></li>
												<li><a href="http://www.ublinker.com"><i class="am-margin-left-sm"></i>友企联</a></li>
												<li><a href="https://api.yonyoucloud.com"><i class="am-margin-left-sm"></i>APILink</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1_4'}">
												<i class="am-margin-left-sm"></i>
												行业云
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1_4">
												<li><a href="http://www.yonyougov.com/"><i class="am-margin-left-sm"></i>用友政务云</a></li>
												<li><a href="https://jingzhi.yonyoucloud.com"><i class="am-margin-left-sm"></i>用友制造云</a></li>
												<li><a href="http://hit.yonyou.com"><i class="am-margin-left-sm"></i>用友医疗云</a></li>
												<li><a href="https://yangke.yonyou.com/"><i class="am-margin-left-sm"></i>国资企业云</a></li>
												<li><a href="https://www.yonyouccs.com/icop-website/"><i class="am-margin-left-sm"></i>用友建造云</a></li>
												<li style="display: none;"><a href="http://www.yonyoufintech.com/"><i class="am-margin-left-sm"></i>用友金融云</a></li>
												<li><a href="http://auto.yonyou.com/"><i class="am-margin-left-sm"></i>用友汽车云</a></li>
												<li><a href="http://www.seentao.com/"><i class="am-margin-left-sm"></i>用友教育云</a></li>
												<li><a href="http://www.honghuotai.com/#/index"><i class="am-margin-left-sm"></i>用友餐饮云</a></li>
												<li><a href="https://www.uf-tobacco.com/index.html"><i class="am-margin-left-sm"></i>用友烟草云</a></li>
												<li><a href="http://yonyouny.com/"><i class="am-margin-left-sm"></i>用友能源云</a></li>
												<li><a href="https://market.yonyoucloud.com/market/shop/e7616d0d-9962-46eb-848f-0b306af0a767"><i class="am-margin-left-sm"></i>用友广信</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1_5'}">
												<i class="am-margin-left-sm"></i>
												小微企业云
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1_5">
												
												
												<li><a href="https://www.xinfushe.com/"><i class="am-margin-left-sm"></i>薪福社</a></li>
												<li><a href="https://www.shufafin.com/index.html"><i class="am-margin-left-sm"></i>企业金融云</a></li>
												<li><a href="https://www.chanjet.com/"><i class="am-margin-left-sm"></i>畅捷通</a></li>
												<li><a href="https://h.chanjet.com"><i class="am-margin-left-sm"></i>畅捷通好会计</a></li>
												<li><a href="https://hsy.chanjet.com"><i class="am-margin-left-sm"></i>畅捷通好生意</a></li>
												<li><a href="https://tcloud.chanjet.com"><i class="am-margin-left-sm"></i>畅捷通T+ Cloud</a></li>
												<li><a href="https://d.chanjet.com"><i class="am-margin-left-sm"></i>畅捷贷</a></li>
											</ul>
										</li>
										
										
										
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_1_6'}">
												<i class="am-margin-left-sm"></i>
												服务支持
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_1_6">
												<li><a href="https://euc.yonyoucloud.com/register?sysid=market&service=https://market.yonyoucloud.com/marke"><i class="am-margin-left-sm"></i>云市场入驻</a></li>
												<li><a href="https://market.yonyoucloud.com/"><i class="am-margin-left-sm"></i>加入合作伙伴</a></li>
												<li><a href="http://partner.yonyouup.cn/ForOtherSystem/PartnerSearchForOfficialWebSite.aspx"><i class="am-margin-left-sm"></i>查询授权经销商</a></li>
												<li><a href="https://developer.yonyoucloud.com/fe/fe-portal/index.html#/dashboard"><i class="am-margin-left-sm"></i>开发者中心</a></li>
												<li><a href="http://nc.yonyou.com/services.php"><i class="am-margin-left-sm"></i>用友iSM Cloud</a></li>
												<li><a href="http://fwq.yonyou.com/up_service/index.php?r=up_shop/index"><i class="am-margin-left-sm"></i>用友U8服务圈</a></li>
												<li><a href="https://service.chanjet.com/"><i class="am-margin-left-sm"></i>畅捷通服务社区</a></li>
											</ul>
										</li>
										
										
									</ul>
								</li>
								
								
								
								<li>
									<a data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_2'}">
										<i class="am-margin-left-sm"></i>软件<i class="am-icon-angle-right am-fr am-margin-right"></i>
									</a>
									
									<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_2">
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_2_1'}">
												<i class="am-margin-left-sm"></i>
												面向大型企业
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_2_1">
												<li><a href="http://nc.yonyou.com/product.php?id=4"><i class="am-margin-left-sm"></i>用友NC6</a></li>
												<li><a href="http://nc.yonyou.com/product.php?id=5"><i class="am-margin-left-sm"></i>用友U9</a></li>
												<li><a href="http://nc.yonyou.com/product.php?id=6"><i class="am-margin-left-sm"></i>用友智能工厂</a></li>
												<li><a href="http://nc.yonyou.com/product.php?id=7"><i class="am-margin-left-sm"></i>用友HCM</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_2_2'}">
												<i class="am-margin-left-sm"></i>
												面向中型企业
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_2_2">
												<li><a href="http://u8.yonyou.com"><i class="am-margin-left-sm"></i>用友U8+</a></li>
												<li><a href="http://u8.yonyou.com/index.php/Home/Product/detail.html?id=20"><i class="am-margin-left-sm"></i>用友PLM</a></li>
												<li><a href="http://u8.yonyou.com/index.php/Home/Product/detail.html?id=19"><i class="am-margin-left-sm"></i>用友CRM</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_2_3'}">
												<i class="am-margin-left-sm"></i>
												面向小微企业
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_2_3">
												<li><a href="http://software.chanjet.com/rj-Tg.html"><i class="am-margin-left-sm"></i>畅捷通T+</a></li>
												<li><a href="http://software.chanjet.com/rj-T1.html"><i class="am-margin-left-sm"></i>畅捷通T1</a></li>
												<li><a href="http://software.chanjet.com/rj-T3.html"><i class="am-margin-left-sm"></i>畅捷通T3</a></li>
												<li><a href="http://software.chanjet.com/rj-T6.html"><i class="am-margin-left-sm"></i>畅捷通T6</a></li>
											</ul>
										</li>
										
										<li>
											<a href="#/userList/0" data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_2_4'}">
												<i class="am-margin-left-sm"></i>
												行业
												<i class="am-icon-angle-right am-fr am-margin-right"></i>
											</a>
											<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_2_4">
												<li><a href="http://www.yonyougov.com/"><i class="am-margin-left-sm"></i>政务</a></li>
												<li><a href="https://jingzhi.yonyoucloud.com/#/"><i class="am-margin-left-sm"></i>工业</a></li>
												<li><a href="http://hit.yonyou.com/"><i class="am-margin-left-sm"></i>医疗</a></li>
												<li><a href="https://www.yonyou.com/jianzhu/"><i class="am-margin-left-sm"></i>建筑</a></li>
												<li style="display: none;"><a href="http://www.yonyoufintech.com/"><i class="am-margin-left-sm"></i>金融</a></li>
												<li><a href="http://auto.yonyou.com/"><i class="am-margin-left-sm"></i>汽车</a></li>
												<li><a href="http://www.seentao.com/"><i class="am-margin-left-sm"></i>教育</a></li>
												<li><a href="http://www.honghuotai.com/#/index"><i class="am-margin-left-sm"></i>餐饮</a></li>
												<li><a href="https://www.uf-tobacco.com/index.html"><i class="am-margin-left-sm"></i>烟草</a></li>
												<li><a href="http://yonyouny.com/"><i class="am-margin-left-sm"></i>能源</a></li>
												<li><a href="https://item.yonyoucloud.com/13445329-5747-4a96-be20-2fc37f63620b.html"><i class="am-margin-left-sm"></i>广电</a></li>
												<li><a href="https://www.yonyouaud.com/"><i class="am-margin-left-sm"></i>审计</a></li>
												<li><a href="http://nc.yonyou.com/project.php?stype=1&id=5"><i class="am-margin-left-sm"></i>公用事业</a></li>
												<li><a href="http://nc.yonyou.com/project.php?stype=1&id=9"><i class="am-margin-left-sm"></i>智慧园区</a></li>
											</ul>
										</li>
										
									</ul>
								</li>
								
								
								<li>
									<a data-am-collapse="{parent: '#product_web', target: '#product_web-nav1_3'}">
										<i class="am-margin-left-sm"></i>金融<i class="am-icon-angle-right am-fr am-margin-right"></i>
									</a>

									<ul class="am-list am-collapse admin-sidebar-sub" id="product_web-nav1_3">
										<li><a href="https://www.shufafin.com/zf/account.html"><i class="am-margin-left-sm"></i>支付结算</a></li>
										<li><a href="https://www.shufafin.com/xd/ysd.html"><i class="am-margin-left-sm"></i>供应链</a></li>
										<li><a href="https://www.shufafin.com/xj/yin.html"><i class="am-margin-left-sm"></i>现金管理</a></li>
										<li><a href="https://www.shufafin.com/zx/credit.html"><i class="am-margin-left-sm"></i>企业征信</a></li>
										<li><a href="https://www.shufafin.com/fk/linkdata.html"><i class="am-margin-left-sm"></i>数据风控</a></li>
										<li><a href="http://www.yonyoufintech.com/"><i class="am-margin-left-sm"></i>链融云</a></li>
										<li><a href="https://www.yyfaxgroup.com/"><i class="am-margin-left-sm"></i>友金所</a></li>
										<li><a href="http://www.yonyoubao.com/"><i class="am-margin-left-sm"></i>友企保</a></li>
										<li><a href="https://www.yyfaxgroup.com/"><i class="am-margin-left-sm"></i>用友产投</a></li>
										<li><a href="http://www.ytabx.com/"><i class="am-margin-left-sm"></i>友太安</a></li>
									</ul>
								</li>
								
								
							</ul>
						</li>
						
						
						<!-- 行业 -->
						<li class="am-panel">
							<a data-am-collapse="{parent: '#collapase-nav-1',target: '#solve_web'}" href="#/"><i
								class="am-margin-left-sm"></i> 行业<i
								class="am-icon-angle-right am-fr am-margin-right"></i></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="solve_web">
								<li><a href="http://www.yonyougov.com/"><i class="am-margin-left-sm"></i>政务</a></li>
								<li><a href="https://jingzhi.yonyoucloud.com/#/"><i class="am-margin-left-sm"></i>工业</a></li>
								<li><a href="http://hit.yonyou.com/"><i class="am-margin-left-sm"></i>医疗</a></li>
								<li><a href="https://www.yonyou.com/jianzhu/"><i class="am-margin-left-sm"></i>建筑</a></li>
								<li style="display: none;"><a href="http://www.yonyoufintech.com/"><i class="am-margin-left-sm"></i>金融</a></li>
								<li><a href="http://auto.yonyou.com/"><i class="am-margin-left-sm"></i>汽车</a></li>
								<li><a href="http://www.seentao.com/"><i class="am-margin-left-sm"></i>教育</a></li>
								<li><a href="http://www.honghuotai.com/#/index"><i class="am-margin-left-sm"></i>餐饮</a></li>
								<li><a href="https://www.uf-tobacco.com/index.html"><i class="am-margin-left-sm"></i>烟草</a></li>
								<li><a href="http://yonyouny.com/"><i class="am-margin-left-sm"></i>能源</a></li>
								<li><a href="https://item.yonyoucloud.com/13445329-5747-4a96-be20-2fc37f63620b.html"><i class="am-margin-left-sm"></i>广电</a></li>
								<li><a href="https://www.yonyouaud.com/"><i class="am-margin-left-sm"></i>审计</a></li>
								<li><a href="https://www.yonyoucloud.com/cloud_sales.html"><i class="am-margin-left-sm"></i>零售</a></li>
								<li><a href="http://nc.yonyou.com/project.php?stype=1&id=7"><i class="am-margin-left-sm"></i>传媒</a></li>
								<li><a href="http://nc.yonyou.com/project.php?stype=1&id=8"><i class="am-margin-left-sm"></i>房地产</a></li>
								<li><a href="http://nc.yonyou.com/project.php?stype=1&id=4"><i class="am-margin-left-sm"></i>交通运输</a></li>
								<li><a href="http://nc.yonyou.com/project.php?stype=1&id=42"><i class="am-margin-left-sm"></i>现代农业</a></li>
								<li><a href="http://nc.yonyou.com/project.php?stype=1&id=5"><i class="am-margin-left-sm"></i>公用事业</a></li>
								<li><a href="http://nc.yonyou.com/project.php?stype=1&id=9"><i class="am-margin-left-sm"></i>智慧园区</a></li>
							</ul>
						</li>
						
						<!-- 客户成功 -->
						<li class="am-panel">
							<a data-am-collapse="{parent: '#collapase-nav-1',target: '#client'}" href="/case/case.html"><i
								class="am-margin-left-sm"></i> 案例中心</a>
						</li>
						
						
						<!-- 云市场 -->
						<li class="am-panel">
							<a target="_blank" data-am-collapse="{parent: '#collapase-nav-1',target: '#client'}" href="https://market.yonyoucloud.com"><i
								class="am-margin-left-sm"></i> 云市场</a>
						</li>
						
						
						
						<!-- 新闻中心 -->
						<li class="am-panel">
							<a data-am-collapse="{parent: '#collapase-nav-1',target: '#news_web'}" href="#/"><i
								class="am-margin-left-sm"></i> 资讯中心<i
								class="am-icon-angle-right am-fr am-margin-right"></i></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="news_web">
								<li><a href="/news/news.html"><i class="am-margin-left-sm"></i>资讯发布</a></li>
								<li><a href="/news/news_media.html"><i class="am-margin-left-sm"></i>媒体报道</a></li>
								<li><a href="/news/news_video.html"><i class="am-margin-left-sm"></i>用友视讯</a></li>
								<li><a href="/news/news_social.html"><i class="am-margin-left-sm"></i>社交媒体</a></li>
								<li><a href="/news/news_base.html"><i class="am-margin-left-sm"></i>媒体资料库</a></li>
							</ul>
						</li>
						
						
						<!-- 活动中心 -->
						<li class="am-panel">
							<a data-am-collapse="{parent: '#collapase-nav-1',target: '#act_web'}" href="#/"><i
								class="am-margin-left-sm"></i> 活动中心<i
								class="am-icon-angle-right am-fr am-margin-right"></i></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="act_web">
								<li><a href="http://subject.yonyou.com/event"><i class="am-margin-left-sm"></i>最新活动</a></li>
								<li><a href="http://subject.yonyou.com/event/html/networklive.html"><i class="am-margin-left-sm"></i>网络直播</a></li>
								<li><a href="http://subject.yonyou.com/zt/summit2020/"><i class="am-margin-left-sm"></i>2020企业数智化</a></li>
							</ul>
						</li>
						
						<!-- 关于我们 -->
						<li class="am-panel">
							<a data-am-collapse="{parent: '#collapase-nav-1',target: '#about_web'}" href="#/"><i
								class="am-margin-left-sm"></i>关于我们<i
								class="am-icon-angle-right am-fr am-margin-right"></i></a>
							<ul class="am-list am-collapse admin-sidebar-sub" id="about_web">
								<li><a href="https://www.yonyou.com/yy/guanyu.html?id=0"><i class="am-margin-left-sm"></i>了解用友</a></li>
								<li><a href="https://www.yonyou.com/yy/lianxiyonyou.html"><i class="am-margin-left-sm"></i>全球办公室</a></li>
								<li><a href="https://www.yonyou.com/yy/guanyu.html?id=2&v=1.0.0"><i class="am-margin-left-sm"></i>经营公告</a></li>
								<li><a href="https://www.yonyou.com/yy/dem.html"><i class="am-margin-left-sm"></i>数字企业体验馆</a></li>
								<li><a href="http://career.yonyou.com/"><i class="am-margin-left-sm"></i>加入用友</a></li>
								<li><a href="https://www.yonyou.com/yy/guanyu.html?id=3"><i class="am-margin-left-sm"></i>社会责任</a></li>
								<li><a href="https://www.yonyou.com/yy/guanyu.html?id=4"><i class="am-margin-left-sm"></i>企业荣誉</a></li>
							</ul>
						</li>

						<!-- 收费资讯 -->
						<li class="am-panel">
							<a  data-am-collapse="{parent: '#collapase-nav-1',target: '#client'}" href="/news_pay/biznews.html"><i
								class="am-margin-left-sm"></i> 收费资讯</a>
						</li>
						
						
						
						
					</ul>
				</div>`
	$("#navjs").html(header);
	$("#doc-oc").html(samll);
	
	
	
	var server_tag = document.getElementById('product').children[0].children[0].children;
    var server_txt = {
      0: [{
        txt: '云市场入驻',
        href: 'https://euc.yonyoucloud.com/register?sysid=market&service=https://market.yonyoucloud.com/market/invite/invite_h5.html?inviterCode=YN4414'
      }, {
        txt: '加入合作伙伴',
        href: 'https://market.yonyoucloud.com/'
      }, {
        txt: '查询授权经销商',
        href: 'http://partner.yonyouup.cn/ForOtherSystem/PartnerSearchForOfficialWebSite.aspx'
      },{
        txt: '开发者中心',
        href: 'https://developer.yonyoucloud.com/fe/fe-portal/index.html#/dashboard'
      },{
        txt: '用友iSM Cloud',
        href: 'http://nc.yonyou.com/services.php'
      }, {
        txt: '用友U8服务圈',
        href: 'http://fwq.yonyou.com/up_service/index.php?r=up_shop/index'
      }, {
        txt: '畅捷通服务社区',
        href: 'https://service.chanjet.com/'
      }],
      1: [{
        txt: '用友iSM Cloud',
        href: 'http://nc.yonyou.com/services.php'
      }, {
        txt: '用友U8服务圈',
        href: 'http://fwq.yonyou.com/up_service/index.php?r=up_shop/index'
      }, {
        txt: '畅捷通服务社区',
        href: 'https://service.chanjet.com/'
      }],
      2: [{
        txt: '中小企业保险服务',
        href: 'http://www.yonyoubao.com/index.php/tuan/index/doc_down'
      },
	  {
        txt: '友太安服务',
        href: 'http://www.ytabx.com/service-flow.html'
      }],
      3: [{
        txt: '政务云服务',
        href: 'javascript:;'
      },
	  {
        txt: '小微企业服务',
        href: 'javascript:;'
      },{
        txt: '更多行业服务支持',
        href: 'javascript:;'
      }]
    };
    for (var i = 0, len = server_tag.length; i < len; i++) {
      server_tag[i].onmouseover = function () {
        var idx = this.getAttribute('hover_index');
        Elementcreate(server_txt[idx]);

      };
    }

    //创建需要服务的元素
    function Elementcreate(data) {
      var html = [];
      for (var i = 0, len = data.length; i < len; i++) {
        var _div = document.createElement('div');
        var _li = document.createElement('li');
        var _a = document.createElement('a');
        _a.innerText = data[i].txt;
        _a.href = data[i].href;
        _li.appendChild(_a);
        _div.appendChild(_li);
        html.push(_div.innerHTML);
      }
      document.getElementById('server_txt').innerHTML = html.join('');
    }
	
})
