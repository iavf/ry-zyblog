## 平台简介
## ruoyi内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## ruoyi-cms新增功能
1. ehcache缓存模块 ruoyi-ehcache 并配备页面监控
2. redis缓存模块 ruoyi-redis 并配备页面监控
3. 数据库备份操作页面
4. 慢请求响应记录，系统响应时间超过500毫秒时，后台方法将被记录！
5. 爬虫模块 ruoyi-spider :爬虫任务和爬虫配置
6. 内容管理 ruoyi-cms :

    a). 文章管理 b). 素材分钟管理 c). 栏目分类 d). 标签管理 e). 邮件发送管理 f). 附件管理  g). 素材管理  h). 相册管理  i). 广告位管理
    
    j). 友情链接管理  k). 模板管理 l). 评论管理  m). 资源管理  n). 链接分类管理 o). 链接管理 p). PV页面访问管理 q). 其它

7. 博客主题界面。配备三套前台博客页面主题随意切换。(pblog,pnews,avatar)在sys_config表中配置主题。 **avatar完整模板等更多免费模板请进群下载。** 

## 在线体验

**Ruoyi-plus演示地址：http://106.13.164.230/blog

文档地址：http://doc.ruoyi.vip

## 开发代办

### 导航

#### 需求

- [x] 一个大分类一个页面
- [x] 多功能搜索框
- [ ] 一个小分类一些链接
- [ ] 网站鼠标悬浮、显示详情
- [ ] 链接详情页
- [ ] 链接点赞
- [ ] 链接热门排行页面
- [ ] 软件工具导航

### 后台
#### bug修复

#### 完善功能
####  合并主分支情况
https://gitee.com/y_project/RuoYi/commits/master?end_date=2020-09-10&page=5&start_date=2019-10-13


d56d6ac
浏览代码Excel支持sort导出排序
44895c3
浏览代码update ruoyi-admin/src/main/resources/static/ruoyi/css/ry-ui.css.
68b5b80
浏览代码冻结列右侧自适应
9627ba9
浏览代码支持openOptions方法最大化
7bca4fc
浏览代码关闭顶部tab页时，左侧菜单定位到当前显示页
829dfaf
新增isLinkage支持页签与菜单联动
5a59670
修改代码生成导入表结构出现异常页面不提醒问题
2499685
浏览代码如果加载头像时发生了错误，则显示一个默认头像
a3bb603
浏览代码Excel支持dictType读取字符串组内容
3846329
浏览代码代码生成主子表序号调整
82d913c
浏览代码代码生成显示类型支持复选框
84e447e
浏览代码用户信息添加输入框组图标&鼠标按下显示密码
942e609
浏览代码常量接口修改为常量类
9f248e9
浏览代码前端表单样式修改成圆角

9c763dc
浏览代码ajaxSuccess判断修正

e4919f1
浏览代码HTML过滤器不替换&实体
052ab88
浏览代码代码生成模板支持主子表

128b35c
浏览代码新增示例（主子表提交）
 若依 提交于 2020-06-15 14:03
 fedb39c
 浏览代码主子表示例添加序号&防止insertRow数据被初始化
  若依 提交于 2020-06-16 13:11

捐赠页面



### argon主题




文章保存 按钮 浮动

下载地址隐藏的div 添加 "下载地址"的醒目提示

#### SEO

- [x] sitemap

#### 扩充功能

#### 下载页面

关于  页面

https://new.shuge.org/about/shuge/

#### 赞赏页面
https://zhong1.org/donation
https://new.shuge.org/donate/
如您觉得我们的内容对您有所帮助，希望支持我们，欢迎打赏捐助。
所有捐赠将被使用于网站日常网络运营和维护，有了您的支持，我们将努力做地更好！

您的捐赠用于何处
服务器：为了保障网站的高效运作及资料采集，我们需要几台高效的服务器以支撑。 [4]（约占开支的 50%）

CDN：为了提升各区域用户的访问体验，网站使用了 CDN 服务。（约占开支的 30%）

域名：为了区分各种用途，我们除了使用 shuge.org 的二级域名，我们还使用了：daoon.com, daoing.com, artview.org（约占开支的 1%）

互联网服务：为了方便不同用户的使用习惯及数据存储，我们使用了较多云端及会员服务，例如：微博会员、百度网盘、Office 365 E3、G Suite、奶牛会员等等（约占开支的 10%）

其它开销：包括一些活动设备、备份系统 [5] 等（约占开支的 9%）


「说明」
1、无论金额多少都是您的一片心意，打赏1元与100元我们同样心存感恩。

2、您的捐赠会被录入本网页捐赠名单中。

3、我们会详细记录每一笔捐赠金额和捐赠人，如以下名单有误，可以通过电子邮件与我们核实。
##### 统计功能



- [ ] 在线人数  
- [x] 总浏览量
- [x] 热门排行榜  

#####相册

- [x] 解决分页bug

- [x] 页面布局

代办

- [x] 404 处理

- [x] 下载地址添加加群 class 

保存按钮浮动

- [x] 热门


##### 移植arg

搜索

分页


友链

赞赏页面

#### 性能优化

##### 前端
###### cdn

##### 后端


##### 缓存

 - def
 - article(60*60,100),文章缓存1h消失，最大100条
 - blogInfo (60*60), 站点信息，12h，
 - category(60*60*12), 所有类别
 - tag(60*60*12), 所有标签
 - money(60*60*12), 财务信息
 - config(60*60*12), 所有配置
 - 

       
        
        
       
       
       
        
文章

热门



##### seo
##### 表 索引 优化


#### bug记录

  

## ruoyi-plus新增功能演示图
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090811_47d8563a_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090954_1cf23ae8_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091030_92471a5c_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091108_8d470b37_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091141_ac064647_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091415_f92464ed_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092642_d112390c_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092730_196ee826_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092815_8077e037_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092845_94536ad4_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092913_9546de2a_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092939_bc6be9ff_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093001_198199f7_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093028_b95622f8_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093057_9246cb3c_528854.png "/></td>
        <td></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093213_b4d429a4_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093235_e0ed62c1_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093348_e8c7ed2c_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093408_086be17c_528854.png "/></td>
    </tr>
<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093800_5d6fbeac_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093823_a514c451_528854.png "/></td>
    </tr>
<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093846_075bb6da_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093905_e30594b8_528854.png "/></td>
    </tr>
<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093930_38d41d49_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/093950_acd73179_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/095538_dae0e203_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/095605_6ca01ed5_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/095636_bd90c4cd_528854.png "/></td>
        <td></td>
    </tr>
</table>
## ruoyi演示图

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_54d9bf66_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_7ab01b74_528854.jpeg"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_5edb60b9_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_c5100096_528854.jpeg"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_a9be8b64_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_d6405543_528854.jpeg"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_e5b14307_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_57323758_528854.jpeg"/></td>
    </tr>	 
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_1165f493_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_9909abae_528854.jpeg"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_79420ac0_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_77ca73e6_528854.jpeg"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_59c33ec6_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_7b46a78b_528854.jpeg"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_3fc5a56f_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_13c754da_528854.jpeg"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_c3b78930_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_00254551_528854.jpeg"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_98e47528_528854.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090623_bddd8793_528854.jpeg"/></td>
    </tr>
</table>


