package com.ruoyi.cms.service.impl;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.ruoyi.cms.domain.CmsDonate;
import com.ruoyi.cms.mapper.CmsDonateMapper;
import com.ruoyi.cms.service.IDonateService;
import com.ruoyi.common.cache.bean.CacheConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打赏管理Service业务层处理
 *
 * @author wujiyue
 * @date 2019-10-28
 */
@Service
public class DonateServiceImpl implements IDonateService {



    @Autowired
    CmsDonateMapper cmsDonateMapper;

    private static final Log log = LogFactory.get();


    @Override
    //@CacheEvict(cacheNames = CacheConstant.MONEY_CACHE_NAME,allEntries = true)
    public int deleteByPrimaryKey(Integer id) {
        return cmsDonateMapper.deleteByPrimaryKey(id);
    }

    @Override
   // @CacheEvict(cacheNames = CacheConstant.MONEY_CACHE_NAME,allEntries = true)
    public int insert(CmsDonate record) {
        return cmsDonateMapper.insert(record);
    }

    @Override
    //@CacheEvict(cacheNames = CacheConstant.MONEY_CACHE_NAME,allEntries = true)
    public int insertSelective(CmsDonate record) {
        return cmsDonateMapper.insertSelective(record);
    }

    @Override
    public CmsDonate selectByPrimaryKey(Integer id) {
        return cmsDonateMapper.selectByPrimaryKey(id);
    }

    @Override
    //@CacheEvict(cacheNames = CacheConstant.MONEY_CACHE_NAME,allEntries = true)
//    @CacheEvict(cacheNames = CacheConstant.MONEY_CACHE_NAME,key = CacheConstant.YH + CacheConstant.MONEY_GETMONEYS_KEY +CacheConstant.YH )
    public int updateByPrimaryKeySelective(CmsDonate record) {
        return cmsDonateMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    //@CacheEvict(cacheNames = CacheConstant.MONEY_CACHE_NAME,allEntries = true)
    public int updateByPrimaryKey(CmsDonate record) {
        return cmsDonateMapper.updateByPrimaryKey(record);
    }

    //@Cacheable(cacheNames = CacheConstant.MONEY_CACHE_NAME,key = "#root.methodName")
    @Cached(name= CacheConstant.DONATE+CacheConstant.D,key="#cmsDonate.id",expire = 3600,cacheType = CacheType.REMOTE)
    public List<CmsDonate> selectDonateList(CmsDonate cmsDonate) {
        return cmsDonateMapper.selectDonateList(cmsDonate);
    }
    //@Cacheable(cacheNames = CacheConstant.MONEY_CACHE_NAME,key = "#root.methodName")
    @Cached(name= CacheConstant.DONATE+CacheConstant.D,key="#cmsDonate.id",expire = 3600,cacheType = CacheType.REMOTE)
    public List<CmsDonate> selectPayList(CmsDonate cmsDonate) {
        return cmsDonateMapper.selectPayList(cmsDonate);
    }
    @Cached(name= CacheConstant.DONATE+CacheConstant.D,key="#cmsDonate.id",expire = 3600,cacheType = CacheType.REMOTE)
    public List<CmsDonate> selectMoneyList(CmsDonate cmsDonate) {
        return cmsDonateMapper.selectMoneyList(cmsDonate);
    }
}

