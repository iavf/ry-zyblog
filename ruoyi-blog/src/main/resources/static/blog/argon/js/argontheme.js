if (void 0 === argonConfig) var argonConfig = {};

function shortcode() {
	var t = $("#post_content").html();
	Math.round(1e4 * Math.random()), $("#post_content").html(t.replace(pattern, '<div collapse-id="1689266529" class="collapse-block shadow-sm collapse-block-transparent hide-border-left"><div class="collapse-block-title" collapse-id="1689266529"><span class="collapse-block-title-inner">默认折叠区块</span><i class="collapse-icon fa fa-angle-down"></i></div><div class="collapse-block-body" style="height: 68px; padding-top: 20px; margin-top: 0px; padding-bottom: 20px; margin-bottom: 0px;">折叠的内容</div></div>'))
}

function setClipboardText(t) {
	$("#articleId").html(), currentUrl = window.location.href, t.preventDefault();
	var e = document.createElement("div");
	e.appendChild(window.getSelection().getRangeAt(0).cloneContents());
	var o = "<div>" + e.innerHTML + "<br /><br />著作权归作者所有。<br />商业转载请联系作者获得授权，非商业转载请注明出处。<br />作者：清欢<br /><br />来源：" + currentUrl + "<br /></div>",
		n = window.getSelection().getRangeAt(0) + "\n\n著作权归作者所有。\n商业转载请联系作者获得授权，非商业转载请注明出处。\n作者：清欢\n来源：" + currentUrl + "\n\n";
	if (t.clipboardData) t.clipboardData.setData("text/html", o), t.clipboardData.setData("text/plain", n); else if (window.clipboardData) return window.clipboardData.setData("text", n)
}

function setCookie(t, e, o) {
	var n = new Date;
	n.setTime(n.getTime() + 24 * o * 60 * 60 * 1e3);
	var a = "expires=" + n.toUTCString();
	document.cookie = t + "=" + e + ";" + a + ";path=/"
}

function getCookie(t) {
	for (var e = t + "=", o = decodeURIComponent(document.cookie).split(";"), n = 0; n < o.length; n++) {
		for (var a = o[n]; " " == a.charAt(0);) a = a.substring(1);
		if (0 == a.indexOf(e)) return a.substring(e.length, a.length)
	}
	return ""
}

void 0 === argonConfig.wp_path && (argonConfig.wp_path = "/"), $("#verifybtn").click(function () {
	var t = $("#articleId").html();
	layui.use("layer", function () {
		var e = layui.layer;
		e.ready(function () {
			var o = e.load(2);
			top.layer.prompt({title: "输入公众号回复的验证码", formType: 1}, function (o, n, a) {
				$.ajax({
					url: "/blog/getResourcePassword",
					type: "post",
					dataType: "json",
					data: {typeResourcePassword: o},
					success: function (o) {
						0 != o.code && "0" != o.code ? e.msg(o.msg, {icon: 5}) : ($.ajax({
							url: "/blog/resourceContent",
							type: "post",
							dataType: "json",
							data: {articleId: t},
							success: function (t) {
								0 != t.code && "0" != t.code || ($("#resouceGuide").remove(),
									$("#resourceContent").html("<br>" + t.data),
									$("#resourceContentBox").css("display", "block"),
									$("html,body").animate(
										{scrollTop: $("#resourceContent").offset().top}, 800))
							}
						}), e.close(n))
					}
				})
			}), e.close(o)
		})
	})
}), $("#passwordButton").click(function () {
	var t = $("#articleId").html(), e = $("#passwordContent").val();
	$.ajax({
		url: "/blog/getArticlePassword",
		type: "post",
		dataType: "json",
		data: {articleId: t, typePassword: e},
		success: function (t) {
			0 != t.code && "0" != t.code ? (layui.use("layer", function () {
				var t = layui.layer;
				t.ready(function () {
					var e = t.load(2);
					t.msg("密码不对，，扫个码关注下很难嘛", {icon: 5}), t.close(e)
				})
			}), console.log(t.msg)) : ($("#post_content").html(t.data.content), $("#post_content").addClass("animated wobble"), $(document).headIndex({
				articleWrapSelector: "#post_content",
				indexBoxSelector: "#leftbar_catalog",
				subItemBoxClass: "index-subItem-box",
				itemClass: "index-item",
				linkClass: "index-link",
				offset: 80
			}))
		}
	})
}),
	$("#edit_article").click(function () {
		var t = $("#articleId").html();
		layui.use("layer", function () {
			var e = layui.layer;
			e.ready(function () {
				var o = e.load(2);
				e.open({
					type: 2,
					skin: "layui-layer-lan",
					title: "修改文章",
					area: ["99%", "99%"],
					shade: .2,
					id: "LAY_layuipr",
					resize: !0,
					btn: ["关闭"],
					btnAlign: "w",
					moveType: 1,
					content: "/cms/article/edit/" + t,
					yes: function (t) {
						e.close(t)
					}
				}), e.close(o)
			})
		})
	}),
	$("#add_article").click(function () {
		// var t = $("#articleId").html();
		layui.use("layer", function () {
			var e = layui.layer;
			e.ready(function () {
				var o = e.load(2);
				e.open({
					type: 2,
					skin: "layui-layer-lan",
					title: "新增文章",
					area: ["99%", "99%"],
					shade: .2,
					id: "LAY_layuipr",
					resize: !0,
					btn: ["关闭"],
					btnAlign: "w",
					moveType: 1,
					content: "/cms/article/add",
					yes: function (t) {
						e.close(t)
					}
				}), e.close(o)
			})
		})
	}),
	$(".thumbs-button").click(function () {
		$.ajax({
			url: ctx + "blog/article/upVote",
			type: "post",
			dataType: "json",
			data: {articleId: tid},
			success: function (t) {
				0 != t.code && "0" != t.code ? console.log(t.msg) : $("#loveCount").text(parseInt($("#loveCount").text()) + 1)
			}
		})
	}), $(function () {
	$(document).headIndex({
		articleWrapSelector: "#post_content",
		indexBoxSelector: "#leftbar_catalog",
		subItemBoxClass: "index-subItem-box",
		itemClass: "index-item",
		linkClass: "index-link",
		offset: 80
	})
}), document.addEventListener("copy", function (t) {
	setClipboardText(t)
});
var translation = {};

function __(t) {
	let e = argonConfig.language;
	return void 0 === translation[e] ? t : void 0 === translation[e][t] ? t : translation[e][t]
}

if (translation.en_US = {
	"确定": "OK",
	"清除": "Clear",
	"恢复博客默认": "Set To Default",
	"评论内容不能为空": "Comment content cannot be empty",
	"昵称不能为空": "Name cannot be empty",
	"邮箱或 QQ 号格式错误": "Incorrect email or QQ format",
	"邮箱格式错误": "Incorrect email format",
	"网站格式错误 (不是 http(s):// 开头)": "Website URL format error",
	"验证码未输入": "CAPTCHA cannot be empty",
	"验证码格式错误": "Incorrect CAPTCHA format",
	"评论格式错误": "Comment format error",
	"发送中": "Sending",
	"正在发送": "Sending",
	"评论正在发送中...": "Comment is sending...",
	"发送": "Send",
	"评论发送失败": "Comment failed",
	"发送成功": "Success",
	"您的评论已发送": "Your comment has been sent",
	"评论": "Comments",
	"未知原因": "Unknown Error",
	"评论内容不能为空": "Comment content cannot be empty",
	"编辑中": "Editing",
	"正在编辑": "Editing",
	"评论正在编辑中...": "Comment is editing",
	"编辑": "Edit",
	"评论编辑失败": "Comment editing failed",
	"已编辑": "Edited",
	"编辑成功": "Success",
	"您的评论已编辑": "Your comment has been edited",
	"评论 #": "Comment #",
	"的编辑记录": "- Edit History",
	"加载失败": "Failed to load",
	"展开": "Show",
	"没有更多了": "No more comments",
	"找不到该 Repo": "Can't find the repository",
	"获取 Repo 信息失败": "Failed to get repository information",
	"点赞失败": "Vote failed",
	"Hitokoto 获取失败": "Failed to get Hitokoto",
	"复制成功": "Copied",
	"代码已复制到剪贴板": "Code has been copied to the clipboard",
	"复制失败": "Failed",
	"请手动复制代码": "Please copy the code manually",
	"刚刚": "Now",
	"分钟前": "minutes ago",
	"小时前": "hours ago",
	"昨天": "Yesterday",
	"前天": "The day before yesterday",
	"天前": "days ago",
	"隐藏行号": "Hide Line Numbers",
	"显示行号": "Show Line Numbers",
	"开启折行": "Enable Break Line",
	"关闭折行": "Disable Break Line",
	"复制": "Copy",
	"全屏": "Fullscreen",
	"退出全屏": "Exit Fullscreen"
}, translation.ru_RU = {
	"确定": "ОК",
	"清除": "Очистить",
	"恢复博客默认": "Восстановить по умолчанию",
	"评论内容不能为空": "Содержимое комментария не может быть пустым",
	"昵称不能为空": "Имя не может быть пустым",
	"邮箱或 QQ 号格式错误": "Неверный формат электронной почты или QQ",
	"邮箱格式错误": "Неправильный формат электронной почты",
	"网站格式错误 (不是 http(s):// 开头)": "Сайт ошибка формата URL-адреса ",
	"验证码未输入": "Вы не решили капчу",
	"验证码格式错误": "Ошибка проверки капчи",
	"评论格式错误": "Неправильный формат комментария",
	"发送中": "Отправка",
	"正在发送": "Отправка",
	"评论正在发送中...": "Комментарий отправляется...",
	"发送": "Отправить",
	"评论发送失败": "Не удалось отправить комментарий",
	"发送成功": "Комментарий отправлен",
	"您的评论已发送": "Ваш комментарий был отправлен",
	"评论": "Комментарии",
	"未知原因": "Неизвестная ошибка",
	"评论内容不能为空": "Содержимое комментария не может быть пустым",
	"编辑中": "Редактируется",
	"正在编辑": "Редактируется",
	"评论正在编辑中...": "Комментарий редактируется",
	"编辑": "Редактировать",
	"评论编辑失败": "Не удалось отредактировать комментарий",
	"已编辑": "Изменено",
	"编辑成功": "Успешно",
	"您的评论已编辑": "Ваш комментарий был изменен",
	"评论 #": "Комментарий #",
	"的编辑记录": "- История изменений",
	"加载失败": "Ошибка загрузки",
	"展开": "Показать",
	"没有更多了": "Комментариев больше нет",
	"找不到该 Repo": "Невозможно найти репозиторий",
	"获取 Repo 信息失败": "Неудалось получить информацию репозитория",
	"点赞失败": "Ошибка голосования",
	"Hitokoto 获取失败": "Проблемы с вызовом Hitokoto",
	"复制成功": "Скопировано",
	"代码已复制到剪贴板": "Код скопирован в буфер обмена",
	"复制失败": "Неудалось",
	"请手动复制代码": "Скопируйте код вручную",
	"刚刚": "Сейчас",
	"分钟前": "минут назад",
	"小时前": "часов назад",
	"昨天": "Вчера",
	"前天": "Позавчера",
	"天前": "дней назад",
	"隐藏行号": "Скрыть номера строк",
	"显示行号": "Показать номера строк",
	"开启折行": "Включить перенос строк",
	"关闭折行": "Выключить перенос строк",
	"复制": "Скопировать",
	"全屏": "Полноэкранный режим",
	"退出全屏": "Выход из полноэкранного режима"
}, translation.zh_TW = {
	"确定": "確定",
	"清除": "清除",
	"恢复博客默认": "恢復博客默認",
	"评论内容不能为空": "評論內容不能為空",
	"昵称不能为空": "昵稱不能為空",
	"邮箱或 QQ 号格式错误": "郵箱或 QQ 號格式錯誤",
	"邮箱格式错误": "郵箱格式錯誤",
	"网站格式错误 (不是 http(s):// 开头)": "網站格式錯誤 (不是 http(s):// 開頭)",
	"验证码未输入": "驗證碼未輸入",
	"验证码格式错误": "驗證碼格式錯誤",
	"评论格式错误": "評論格式錯誤",
	"发送中": "發送中",
	"正在发送": "正在發送",
	"评论正在发送中...": "評論正在發送中...",
	"发送": "發送",
	"评论发送失败": "評論發送失敗",
	"发送成功": "發送成功",
	"您的评论已发送": "您的評論已發送",
	"评论": "評論",
	"未知原因": "未知原因",
	"评论内容不能为空": "評論內容不能為空",
	"编辑中": "編輯中",
	"正在编辑": "正在編輯",
	"评论正在编辑中...": "評論正在編輯中...",
	"编辑": "編輯",
	"评论编辑失败": "評論編輯失敗",
	"已编辑": "已編輯",
	"编辑成功": "編輯成功",
	"您的评论已编辑": "您的評論已編輯",
	"评论 #": "評論 #",
	"的编辑记录": "的編輯記錄",
	"加载失败": "加載失敗",
	"展开": "展開",
	"没有更多了": "沒有更多了",
	"找不到该 Repo": "找不到該 Repo",
	"获取 Repo 信息失败": "獲取 Repo 信息失敗",
	"点赞失败": "點贊失敗",
	"Hitokoto 获取失败": "Hitokoto 獲取失敗",
	"复制成功": "復制成功",
	"代码已复制到剪贴板": "代碼已復制到剪貼板",
	"复制失败": "復制失敗",
	"请手动复制代码": "請手動復制代碼",
	"刚刚": "剛剛",
	"分钟前": "分鐘前",
	"小时前": "小時前",
	"昨天": "昨天",
	"前天": "前天",
	"天前": "天前",
	"隐藏行号": "隱藏行號",
	"显示行号": "顯示行號",
	"开启折行": "開啟折行",
	"关闭折行": "關閉折行",
	"复制": "復制",
	"全屏": "全屏",
	"退出全屏": "退出全屏"
}, function () {
	let t, e, o = document.getElementById("navbar-main"), n = $("#banner_container"), a = $("#content");

	function s() {
		let n = document.documentElement.scrollTop || document.body.scrollTop;
		if (n < t) return o.style.setProperty("background-color", "rgba(var(--toolbar-color), 0)", "important"), o.style.setProperty("box-shadow", "none"), void o.classList.add("navbar-ontop");
		if (n > e) return o.style.setProperty("background-color", "rgba(var(--toolbar-color), 0.85)", "important"), o.style.setProperty("box-shadow", ""), void o.classList.remove("navbar-ontop");
		let a = (n - t) / (e - t) * .85;
		o.style.setProperty("background-color", "rgba(var(--toolbar-color), " + a, "important"), o.style.setProperty("box-shadow", ""), o.classList.remove("navbar-ontop")
	}

	t = n.offset().top - 75, e = a.offset().top - 75, $(window).resize(function () {
		t = n.offset().top - 75, e = a.offset().top - 75
	}), s(), document.addEventListener("scroll", s, {passive: !0})
}(), $(document).on("click", "#navbar_search_input_container", function () {
	$(this).addClass("open"), $("#navbar_search_input").focus()
}), $(document).on("blur", "#navbar_search_input_container", function () {
	$(this).removeClass("open")
}), $(document).on("keydown", "#navbar_search_input_container #navbar_search_input ", function (t) {
	if (13 != t.keyCode) return;
	let e = $(this).val();
	"" != e ? ($(document).scrollTop(), window.location.href = "/blog/search?con=" + encodeURI(e)) : $("#navbar_search_input_container").blur()
}), $(document).on("keydown", "#navbar_search_input_mobile", function (t) {
	if (13 != t.keyCode) return;
	let e = $(this).val();
	$("#navbar_global .collapse-close button").click(), "" != e && ($(document).scrollTop(), window.location.href = "/blog/search?con=" + encodeURI(e))
}), $(document).on("click", "#leftbar_search_container", function () {
	return $(".leftbar-search-button").addClass("open"), $("#leftbar_search_input").removeAttr("readonly").focus(), $("#leftbar_search_input").focus(), $("#leftbar_search_input").select(), !1
}), $(document).on("blur", "#leftbar_search_container", function () {
	$(".leftbar-search-button").removeClass("open"), $("#leftbar_search_input").attr("readonly", "readonly")
}), $(document).on("keydown", "#leftbar_search_input", function (t) {
	if (13 != t.keyCode) return;
	let e = $(this).val();
	"" != e ? ($("html").removeClass("leftbar-opened"), window.location.href = "/blog/search?con=" + encodeURI(e)) : $("#leftbar_search_container").blur()
}), function () {
	$("#leftbar_part1"), $("#leftbar_part2");
	let t = document.getElementById("leftbar_part1"), e = document.getElementById("leftbar_part2"),
		o = $("#leftbar_part1").offset().top, n = $("#leftbar_part1").outerHeight();

	function a() {
		let t = document.documentElement.scrollTop || document.body.scrollTop;
		o + n + 10 - t <= 90 ? e.classList.add("sticky") : e.classList.remove("sticky"), o + n + 10 - t <= 20 ? document.body.classList.add("leftbar-can-headroom") : document.body.classList.remove("leftbar-can-headroom")
	}

	a(), document.addEventListener("scroll", a, {passive: !0}), $(window).resize(function () {
		o = $("#leftbar_part1").offset().top, n = $("#leftbar_part1").outerHeight(), a()
	}), new MutationObserver(function () {
		o = $("#leftbar_part1").offset().top, n = $("#leftbar_part1").outerHeight(), a()
	}).observe(t, {attributes: !0, childList: !0, subtree: !0})
}(), argonConfig.headroom) var headroom = new Headroom(document.querySelector("body"), {
	tolerance: {up: 0, down: 0},
	offset: 0,
	classes: {
		initial: "with-headroom",
		pinned: "headroom---pinned",
		unpinned: "headroom---unpinned",
		top: "headroom---top",
		notTop: "headroom---not-top",
		bottom: "headroom---bottom",
		notBottom: "headroom---not-bottom",
		frozen: "headroom---frozen"
	}
}).init();

function lazyloadStickers() {
	$(".emotion-keyboard .emotion-group:not(d-none) .emotion-item > img.lazyload").lazyload({
		threshold: 500,
		effect: "fadeIn"
	}).removeClass("lazyload"), $("html").trigger("scroll")
}

function inputInsertText(t, e) {
	if ($(e).focus(), !document.execCommand("insertText", !1, t)) if ("function" == typeof e.setRangeText) {
		const o = e.selectionStart;
		e.setRangeText(t), e.selectionStart = e.selectionEnd = o + e.length;
		const n = document.createEvent("UIEvent");
		n.initEvent("input", !0, !1), e.dispatchEvent(n)
	} else {
		let o = $(e).val(), n = e.selectionStart, a = e.selectionEnd;
		$(e).val(o.substring(0, n) + t + o.substring(a)), e.selectionStart = n + t.length, e.selectionEnd = n + t.length
	}
	$(e).focus()
}

function showCommentEditHistory(t) {
	let e = parseInt((new Date).getTime());
	$("#comment_edit_history").data("request-id", e), $("#comment_edit_history .modal-title").html(__("评论 #") + t + " " + __("的编辑记录")), $("#comment_edit_history .modal-body").html("<div class='comment-history-loading'><span class='spinner-border text-primary'></span><span style='display: inline-block;transform: translateY(-4px);margin-left: 15px;font-size: 18px;'>加载中</span></div>"), $("#comment_edit_history").modal(null), $.ajax({
		type: "POST",
		url: argonConfig.wp_path + "wp-admin/admin-ajax.php",
		dataType: "json",
		data: {action: "get_comment_edit_history", id: t},
		success: function (t) {
			$("#comment_edit_history").data("request-id") == e && ($("#comment_edit_history .modal-body").hide(), $("#comment_edit_history .modal-body").html(t.history), $("#comment_edit_history .modal-body").fadeIn(300))
		},
		error: function (t) {
			$("#comment_edit_history").data("request-id") == e && ($("#comment_edit_history .modal-body").hide(), $("#comment_edit_history .modal-body").html(__("加载失败")), $("#comment_edit_history .modal-body").fadeIn(300))
		}
	})
}

function foldLongComments() {
	0 != argonConfig.fold_long_comments && $(".comment-item-inner").each(function () {
		$(this).hasClass("comment-unfolded") || this.clientHeight > 800 && ($(this).addClass("comment-folded"), $(this).append("<div class='show-full-comment'><i class='fa fa-angle-down'></i> " + __("展开") + "</div>"))
	})
}

function gotoHash(t, e) {
	0 != t.length && 0 != $(t).length && (null == e && (e = 200), $("body,html").animate({scrollTop: $(t).offset().top - 80}, e))
}

function getHash(t) {
	return t.substring(t.indexOf("#"))
}

function showPostOutdateToast() {
	$("#primary #post_outdate_toast").length > 0 && (iziToast.show({
		title: "",
		message: $("#primary #post_outdate_toast").data("text"),
		class: "shadow-sm",
		position: "topRight",
		backgroundColor: "var(--themecolor)",
		titleColor: "#ffffff",
		messageColor: "#ffffff",
		iconColor: "#ffffff",
		progressBarColor: "#ffffff",
		icon: "fa fa-info",
		close: !1,
		timeout: 8e3
	}), $("#primary #post_outdate_toast").remove())
}

function zoomifyInit() {
	0 != argonConfig.zoomify && $("article img").zoomify(argonConfig.zoomify)
}

function lazyloadInit() {
	0 != argonConfig.lazyload && ("none" == argonConfig.lazyload.effect && delete argonConfig.lazyload.effect, $("article img.lazyload:not(.lazyload-loaded) , .post-thumbnail.lazyload:not(.lazyload-loaded) , .related-post-thumbnail.lazyload:not(.lazyload-loaded)").lazyload(Object.assign(argonConfig.lazyload, {
		load: function () {
			$(this).addClass("lazyload-loaded")
		}
	})), $(".comment-item-text .comment-sticker.lazyload").lazyload(Object.assign(argonConfig.lazyload, {
		load: function () {
			$(this).removeClass("lazyload")
		}
	})))
}

function panguInit() {
	argonConfig.pangu.indexOf("article") >= 0 && pangu.spacingElementById("post_content"), argonConfig.pangu.indexOf("comment") >= 0 && pangu.spacingElementById("comments"), argonConfig.pangu.indexOf("shuoshuo") >= 0 && pangu.spacingElementByClassName("shuoshuo-container")
}

function clampInit() {
	$(".clamp").each(function (t, e) {
		$clamp(e, {clamp: e.getAttribute("clamp-line")})
	})
}

function pv() {
	var t, e = $("#articleId").html();
	t = null != e && "" != e ? e : document.title;
	var o = {
		os: navigator.platform,
		pageId: t,
		referrer: document.referer,
		url: window.location.href,
		timeZone: (new Date).getTimezoneOffset() / 60
	};
	$.ajax({
		type: "POST", url: "/page/view", data: o, success: function (t) {
			console.log("pv：nice to meet you")
		}
	})
}

function getGithubInfoCardContent() {
	$(".github-info-card").each(function () {
		!function (t) {
			if ("backend" == t.attr("data-getdata")) return $(".github-info-card-description", t).html(t.attr("data-description")), $(".github-info-card-stars", t).html(t.attr("data-stars")), void $(".github-info-card-forks", t).html(t.attr("data-forks"));
			$(".github-info-card-description", t).html("Loading..."), $(".github-info-card-stars", t).html("-"), $(".github-info-card-forks", t).html("-"), author = t.attr("data-author"), project = t.attr("data-project"), $.ajax({
				url: "https://api.github.com/repos/" + author + "/" + project,
				type: "GET",
				dataType: "json",
				success: function (e) {
					description = e.description, "" != e.homepage && null != e.homepage && (description += " <a href='" + e.homepage + "' target='_blank' no-pjax>" + e.homepage + "</a>"), $(".github-info-card-description", t).html(description), $(".github-info-card-stars", t).html(e.stargazers_count), $(".github-info-card-forks", t).html(e.forks_count)
				},
				error: function (e) {
					404 == e.status ? $(".github-info-card-description", t).html(__("找不到该 Repo")) : $(".github-info-card-description", t).html(__("获取 Repo 信息失败"))
				}
			})
		}($(this))
	})
}

function rgb2hsl(t, e, o) {
	let n, a, s = t / 255, i = e / 255, r = o / 255, l = Math.min(s, i, r), c = Math.max(s, i, r), m = c - l,
		d = (c + l) / 2;
	return 0 == m ? (n = 0, a = 0) : (a = d < .5 ? m / (c + l) : m / (2 - c - l), del_R = ((c - s) / 6 + m / 2) / m, del_G = ((c - i) / 6 + m / 2) / m, del_B = ((c - r) / 6 + m / 2) / m, s == c ? n = del_B - del_G : i == c ? n = 1 / 3 + del_R - del_B : r == c && (n = 2 / 3 + del_G - del_R), n < 0 && (n += 1), n > 1 && (n -= 1)), {
		h: n,
		s: a,
		l: d
	}
}

function Hue_2_RGB(t, e, o) {
	return o < 0 && (o += 1), o > 1 && (o -= 1), 6 * o < 1 ? t + 6 * (e - t) * o : 2 * o < 1 ? e : 3 * o < 2 ? t + (e - t) * (2 / 3 - o) * 6 : t
}

function hsl2rgb(t, e, o) {
	let n, a, s, i, r;
	return 0 == e ? (n = o, a = o, s = o) : (n = Hue_2_RGB(i = 2 * o - (r = o < .5 ? o * (1 + e) : o + e - e * o), r, t + 1 / 3), a = Hue_2_RGB(i, r, t), s = Hue_2_RGB(i, r, t - 1 / 3)), {
		R: Math.round(255 * n),
		G: Math.round(255 * a),
		B: Math.round(255 * s),
		r: n,
		g: a,
		b: s
	}
}

function rgb2hex(t, e, o) {
	let n, a, s, i = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
	for (n = "", a = "", s = ""; n.length < 2;) n = i[t % 16] + n, t = Math.floor(t / 16);
	for (; a.length < 2;) a = i[e % 16] + a, e = Math.floor(e / 16);
	for (; s.length < 2;) s = i[o % 16] + s, o = Math.floor(o / 16);
	return "#" + n + a + s
}

function hex2rgb(t) {
	let e = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, A: 10, B: 11, C: 12, D: 13, E: 14, F: 15};
	return {
		R: 16 * e[t.substr(1, 1)] + e[t.substr(2, 1)],
		G: 16 * e[t.substr(3, 1)] + e[t.substr(4, 1)],
		B: 16 * e[t.substr(5, 1)] + e[t.substr(6, 1)],
		r: (16 * e[t.substr(1, 1)] + e[t.substr(2, 1)]) / 255,
		g: (16 * e[t.substr(3, 1)] + e[t.substr(4, 1)]) / 255,
		b: (16 * e[t.substr(5, 1)] + e[t.substr(6, 1)]) / 255
	}
}

function rgb2gray(t, e, o) {
	return Math.round(.299 * t + .587 * e + .114 * o)
}

function hex2gray(t) {
	let e = hex2rgb(t);
	return hex2gray(e.R, e.G, e.B)
}

function rgb2str(t) {
	return t.R + "," + t.G + "," + t.B
}

function hex2str(t) {
	return rgb2str(hex2rgb(t))
}

if (function () {
	let t = $("#float_action_buttons"), e = $("#fabtn_back_to_top"), o = $("#fabtn_toggle_sides"),
		n = $("#fabtn_toggle_darkmode"), a = $("#blog_setting_toggle_darkmode_and_amoledarkmode"),
		s = $("#fabtn_toggle_blog_settings_popup"), i = $("#fabtn_go_to_comment"), r = $("#fabtn_reading_progress_bar"),
		l = $("#fabtn_reading_progress_details"), c = !1;

	function m(t) {
		null != t && "" != t || (t = "off"), $("html").hasClass("filter-" + t) || ($("html").removeClass("filter-sunset filter-darkness filter-grayscale"), "off" != t && $("html").addClass("filter-" + t)), $("#blog_setting_filters .blog-setting-filter-btn").removeClass("active"), $("#blog_setting_filters .blog-setting-filter-btn[filter-name='" + t + "']").addClass("active"), localStorage.Argon_Filter = t
	}

	function d() {
		let t = $(window).scrollTop() / Math.max($(document).height() - $(window).height(), .01);
		l.html((100 * t).toFixed(0) + "%"), r.css("width", (100 * t).toFixed(0) + "%"), $(window).scrollTop() >= 400 || t >= .5 ? e.removeClass("fabtn-hidden") : e.addClass("fabtn-hidden")
	}

	e.on("click", function () {
		c || (c = !0, setTimeout(function () {
			c = !1
		}, 600), $("body,html").animate({scrollTop: 0}, 600))
	}), n.on("click", function () {
		toggleDarkmode()
	}), a.on("click", function () {
		toggleAmoledDarkMode()
	}), $("#post_comment").length > 0 ? $("#fabtn_go_to_comment").removeClass("d-none") : $("#fabtn_go_to_comment").addClass("d-none"), i.on("click", function () {
		gotoHash("#post_comment", 600), $("#post_comment_content").focus()
	}), "left" == localStorage.Argon_fabs_Floating_Status && t.addClass("fabtns-float-left"), o.on("click", function () {
		t.addClass("fabtns-unloaded"), setTimeout(function () {
			t.toggleClass("fabtns-float-left"), t.hasClass("fabtns-float-left") ? localStorage.Argon_fabs_Floating_Status = "left" : localStorage.Argon_fabs_Floating_Status = "right", t.removeClass("fabtns-unloaded")
		}, 300)
	}), s.on("click", function () {
		$("#float_action_buttons").toggleClass("blog_settings_opened")
	}), $("#close_blog_settings").on("click", function () {
		$("#float_action_buttons").removeClass("blog_settings_opened")
	}), $("#blog_setting_darkmode_switch .custom-toggle-slider").on("click", function () {
		toggleDarkmode()
	}), $("#blog_setting_font_sans_serif").on("click", function () {
		$("html").removeClass("use-serif"), localStorage.Argon_Use_Serif = "false"
	}), $("#blog_setting_font_serif").on("click", function () {
		$("html").addClass("use-serif"), localStorage.Argon_Use_Serif = "true"
	}), "true" == localStorage.Argon_Use_Serif ? $("html").addClass("use-serif") : "false" == localStorage.Argon_Use_Serif && $("html").removeClass("use-serif"), $("#blog_setting_shadow_small").on("click", function () {
		$("html").removeClass("use-big-shadow"), localStorage.Argon_Use_Big_Shadow = "false"
	}), $("#blog_setting_shadow_big").on("click", function () {
		$("html").addClass("use-big-shadow"), localStorage.Argon_Use_Big_Shadow = "true"
	}), "true" == localStorage.Argon_Use_Big_Shadow ? $("html").addClass("use-big-shadow") : "false" == localStorage.Argon_Use_Big_Shadow && $("html").removeClass("use-big-shadow"), m(localStorage.Argon_Filter), $(".blog-setting-filter-btn").on("click", function () {
		m(this.getAttribute("filter-name"))
	}), d(), $(window).scroll(function () {
		d()
	}), t.removeClass("fabtns-unloaded")
}(), function () {
	function t(t, e) {
		document.documentElement.style.setProperty("--card-radius", t + "px"), e && setCookie("argon_card_radius", t, 365)
	}

	let e = document.getElementById("blog_setting_card_radius");
	noUiSlider.create(e, {
		start: [$("meta[name='theme-card-radius']").attr("content")],
		step: .5,
		connect: [!0, !1],
		range: {min: [0], max: [30]}
	}), e.noUiSlider.on("update", function (e) {
		t(e[0], !1)
	}), e.noUiSlider.on("set", function (e) {
		t(e[0], !0)
	}), $(document).on("click", "#blog_setting_card_radius_to_default", function () {
		e.noUiSlider.set($("meta[name='theme-card-radius-origin']").attr("content")), t($("meta[name='theme-card-radius-origin']").attr("content"), !1), setCookie("argon_card_radius", $("meta[name='theme-card-radius-origin']").attr("content"), 0)
	})
}(), function () {
	function t() {
		replying = !1, replyID = 0, $("#post_comment_reply_info").slideUp(300), $("#post_comment").removeClass("post-comment-force-privatemode-on post-comment-force-privatemode-off")
	}

	function e(t) {
		editing = !1, editID = 0, $("#post_comment").removeClass("post-comment-force-privatemode-on post-comment-force-privatemode-off"), 1 == t && $("#post_comment_content").val(""), $("#post_comment_content").trigger("change"), $("#post_comment").removeClass("editing")
	}

	replying = !1, replyID = 0, $(document).on("click", ".comment-reply", function () {
		!function (t) {
			e(!1), replying = !0, replyID = t, $("#post_comment_reply_name").html($("#comment-" + t + " .comment-item-title > .comment-name")[0].innerHTML);
			let o = $("#comment-" + t + " .comment-item-text")[0].innerHTML;
			"" != $("#comment-" + t + " .comment-item-source")[0].innerHTML && (o = $("#comment-" + t + " .comment-item-source")[0].innerHTML.replace(/\n/g, "</br>")), $("#post_comment_reply_preview").html(o), $("#comment-" + t + " .comment-item-title .badge-private-comment").length > 0 ? $("#post_comment").addClass("post-comment-force-privatemode-on") : $("#post_comment").addClass("post-comment-force-privatemode-off"), $("body,html").animate({scrollTop: $("#post_comment").offset().top - 100}, 300), $("#post_comment_reply_info").slideDown(600), setTimeout(function () {
				$("#post_comment_content").focus()
			}, 300)
		}(this.getAttribute("data-id"))
	}), $(document).on("click", "#post_comment_reply_cancel", function () {
		t()
	}), editing = !1, editID = 0, $(document).on("click", ".comment-edit", function () {
		var e;
		e = this.getAttribute("data-id"), t(), editing = !0, editID = e, $("#post_comment").addClass("editing"), $("#post_comment_content").val($("#comment-" + editID + " .comment-item-source").text()), $("#post_comment_content").trigger("change"), 1 == $("#comment-" + editID).data("use-markdown") && null != document.getElementById("comment_post_use_markdown") ? document.getElementById("comment_post_use_markdown").checked = !0 : document.getElementById("comment_post_use_markdown").checked = !1, $("#comment-" + e + " .comment-item-title .badge-private-comment").length > 0 ? $("#post_comment").addClass("post-comment-force-privatemode-on") : $("#post_comment").addClass("post-comment-force-privatemode-off"), $("body,html").animate({scrollTop: $("#post_comment").offset().top - 100}, 300), $("#post_comment_content").focus()
	}), $(document).on("click", "#post_comment_edit_cancel", function () {
		$("body,html").animate({scrollTop: $("#comment-" + editID).offset().top - 100}, 300), e(!0)
	}), $(document).on("click", "#post_comment_toggle_extra_input", function () {
		$("#post_comment").toggleClass("show-extra-input"), $("#post_comment").hasClass("show-extra-input") ? $("#post_comment_extra_input").slideDown(300) : $("#post_comment_extra_input").slideUp(300)
	}), $(document).on("change input keydown keyup propertychange", "#post_comment_content", function () {
		$("#post_comment_content_hidden")[0].innerText = $("#post_comment_content").val() + "\n", $("#post_comment_content").css("height", $("#post_comment_content_hidden").outerHeight())
	}), $(document).on("focus", "#post_comment_link", function () {
		$(".post-comment-link-container").addClass("active")
	}), $(document).on("blur", "#post_comment_link", function () {
		$(".post-comment-link-container").removeClass("active")
	}), $(document).on("focus", "#post_comment_captcha", function () {
		$(".post-comment-captcha-container").addClass("active")
	}), $(document).on("blur", "#post_comment_captcha", function () {
		$(".post-comment-captcha-container").removeClass("active")
	}), $(document).on("click", "#post_comment_send", function () {
		$("#post_comment").hasClass("editing") ? (commentContent = $("#post_comment_content").val(), isError = !1, errorMsg = "", commentContent.match(/^\s*$/) && (isError = !0, errorMsg += __("评论内容不能为空") + "</br>"), isError ? iziToast.show({
			title: __("评论格式错误"),
			message: errorMsg,
			class: "shadow-sm",
			position: "topRight",
			backgroundColor: "#f5365c",
			titleColor: "#ffffff",
			messageColor: "#ffffff",
			iconColor: "#ffffff",
			progressBarColor: "#ffffff",
			icon: "fa fa-close",
			timeout: 5e3
		}) : ($("#post_comment_content").attr("disabled", "disabled"), $("#post_comment_send").attr("disabled", "disabled"), $("#post_comment_edit_cancel").attr("disabled", "disabled"), $("#post_comment_send .btn-inner--icon.hide-on-comment-not-editing").html("<i class='fa fa-spinner fa-spin'></i>"), $("#post_comment_send .btn-inner--text.hide-on-comment-not-editing").html(__("编辑中")), iziToast.show({
			title: __("正在编辑"),
			message: __("评论正在编辑中..."),
			class: "shadow-sm iziToast-noprogressbar",
			position: "topRight",
			backgroundColor: "var(--themecolor)",
			titleColor: "#ffffff",
			messageColor: "#ffffff",
			iconColor: "#ffffff",
			progressBarColor: "#ffffff",
			icon: "fa fa-spinner fa-spin",
			close: !1,
			timeout: 999999999
		}), $.ajax({
			type: "POST",
			url: argonConfig.wp_path + "wp-admin/admin-ajax.php",
			dataType: "json",
			data: {action: "user_edit_comment", comment: commentContent, id: editID},
			success: function (t) {
				if ($("#post_comment_content").removeAttr("disabled"), $("#post_comment_send").removeAttr("disabled"), $("#post_comment_edit_cancel").removeAttr("disabled"), $("#post_comment_send .btn-inner--icon.hide-on-comment-not-editing").html("<i class='fa fa-pencil'></i>"), $("#post_comment_send .btn-inner--text.hide-on-comment-not-editing").html(__("编辑")), "failed" == t.status) return iziToast.destroy(), void iziToast.show({
					title: __("评论编辑失败"),
					message: t.msg,
					class: "shadow-sm",
					position: "topRight",
					backgroundColor: "#f5365c",
					titleColor: "#ffffff",
					messageColor: "#ffffff",
					iconColor: "#ffffff",
					progressBarColor: "#ffffff",
					icon: "fa fa-close",
					timeout: 5e3
				});
				t.new_comment = t.new_comment.replace(/<img class='comment-sticker lazyload'(.*?)\/>/g, "").replace(/<(\/).noscript>/g, ""), $("#comment-" + editID + " .comment-item-text").html(t.new_comment), $("#comment-" + editID + " .comment-item-source").html(t.new_comment_source), 0 == $("#comment-" + editID + " .comment-info .comment-edited").length && $("#comment-" + editID + " .comment-info").prepend("<div class='comment-edited'><i class='fa fa-pencil' aria-hidden='true'></i>" + __("已编辑") + "</div>"), t.can_visit_edit_history && $("#comment-" + editID + " .comment-info .comment-edited").addClass("comment-edithistory-accessible"), iziToast.destroy(), iziToast.show({
					title: __("编辑成功"),
					message: __("您的评论已编辑"),
					class: "shadow-sm",
					position: "topRight",
					backgroundColor: "#2dce89",
					titleColor: "#ffffff",
					messageColor: "#ffffff",
					iconColor: "#ffffff",
					progressBarColor: "#ffffff",
					icon: "fa fa-check",
					timeout: 5e3
				}), $("body,html").animate({scrollTop: $("#comment-" + editID).offset().top - 100}, 300), editing = !1, editID = 0, $("#post_comment_content").val(""), $("#post_comment").removeClass("editing post-comment-force-privatemode-on post-comment-force-privatemode-off"), $("#post_comment_content").trigger("change")
			},
			error: function (t) {
				if ($("#post_comment_content").removeAttr("disabled"), $("#post_comment_send").removeAttr("disabled"), $("#post_comment_edit_cancel").removeAttr("disabled"), $("#post_comment_send .btn-inner--icon.hide-on-comment-not-editing").html("<i class='fa fa-pencil'></i>"), $("#post_comment_send .btn-inner--text.hide-on-comment-not-editing").html(__("编辑")), 4 != t.readyState || 0 == t.status) return iziToast.destroy(), void iziToast.show({
					title: __("评论编辑失败"),
					message: __("未知原因"),
					class: "shadow-sm",
					position: "topRight",
					backgroundColor: "#f5365c",
					titleColor: "#ffffff",
					messageColor: "#ffffff",
					iconColor: "#ffffff",
					progressBarColor: "#ffffff",
					icon: "fa fa-close",
					timeout: 5e3
				})
			}
		}))) : function () {
			commentContent = $("#post_comment_content").val(), commentName = $("#post_comment_name").val(), commentEmail = $("#post_comment_email").val(), commentLink = $("#post_comment_link").val(), $("#comment_post_use_markdown").length > 0 ? useMarkdown = $("#comment_post_use_markdown")[0].checked : useMarkdown = !1, $("#comment_post_privatemode").length > 0 ? privateMode = $("#comment_post_privatemode")[0].checked : privateMode = !1, $("#comment_post_mailnotice").length > 0 ? mailNotice = $("#comment_post_mailnotice")[0].checked : mailNotice = !1, isError = !1, errorMsg = "";
			var t = $("#articleId").html();
			commentContent.match(/^\s*$/) && (isError = !0, errorMsg += __("评论内容不能为空") + "</br>"), $("#post_comment").hasClass("no-need-name-email") ? null != document.getElementById("comment_post_mailnotice") && 1 == document.getElementById("comment_post_mailnotice").checked && ($("#post_comment").hasClass("enable-qq-avatar") ? /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(commentEmail) || /^[1-9][0-9]{4,10}$/.test(commentEmail) || (isError = !0, errorMsg += __("邮箱或 QQ 号格式错误") + "</br>") : /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(commentEmail) || (isError = !0, errorMsg += __("邮箱格式错误") + "</br>")) : (commentName.match(/^\s*$/) && (isError = !0, errorMsg += __("昵称不能为空") + "</br>"), $("#post_comment").hasClass("enable-qq-avatar") ? /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(commentEmail) || /^[1-9][0-9]{4,10}$/.test(commentEmail) || (isError = !0, errorMsg += __("邮箱或 QQ 号格式错误") + "</br>") : /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(commentEmail) || (isError = !0, errorMsg += __("邮箱格式错误") + "</br>")), "" == commentLink || /https?:\/\//.test(commentLink) || (isError = !0, errorMsg += __("网站格式错误 (不是 http(s):// 开头)") + "</br>"), isError ? iziToast.show({
				title: __("评论格式错误"),
				message: errorMsg,
				class: "shadow-sm",
				position: "topRight",
				backgroundColor: "#f5365c",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-close",
				timeout: 5e3
			}) : ($("#post_comment").addClass("sending"), $("#post_comment_content").attr("disabled", "disabled"), $("#post_comment_name").attr("disabled", "disabled"), $("#post_comment_email").attr("disabled", "disabled"), $("#post_comment_link").attr("disabled", "disabled"), $("#post_comment_send").attr("disabled", "disabled"), $("#post_comment_reply_cancel").attr("disabled", "disabled"), $("#post_comment_send .btn-inner--icon.hide-on-comment-editing").html("<i class='fa fa-spinner fa-spin'></i>"), $("#post_comment_send .btn-inner--text.hide-on-comment-editing").html(__("发送中")), iziToast.show({
				title: __("正在发送"),
				message: __("评论正在发送中..."),
				class: "shadow-sm iziToast-noprogressbar",
				position: "topRight",
				backgroundColor: "var(--themecolor)",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-spinner fa-spin",
				close: !1,
				timeout: 999999999
			}), console.log(replyID), $.ajax({
				type: "POST",
				url: "/blog/comments/save",
				dataType: "json",
				data: {
					pid: replyID,
					content: commentContent,
					userName: commentName,
					email: commentEmail,
					url: commentLink,
					tid: t
				},
				success: function (t) {
					if ($("#post_comment").removeClass("sending"), $("#post_comment_content").removeAttr("disabled"), $("#post_comment_name").removeAttr("disabled"), $("#post_comment_email").removeAttr("disabled"), $("#post_comment_link").removeAttr("disabled"), $("#post_comment_send").removeAttr("disabled"), $("#post_comment_reply_cancel").removeAttr("disabled"), $("#post_comment_send .btn-inner--icon.hide-on-comment-editing").html("<i class='fa fa-send'></i>"), $("#post_comment_send .btn-inner--text.hide-on-comment-editing").html(__("发送")), $("#post_comment").removeClass("show-extra-input post-comment-force-privatemode-on post-comment-force-privatemode-off"), t.isAdmin || $("#post_comment_captcha").removeAttr("disabled"), 0 != t.code && "0" != t.code) return iziToast.destroy(), void iziToast.show({
						title: __("发送失败"),
						message: t.msg,
						class: "shadow-sm",
						position: "topRight",
						backgroundColor: "#f5365c",
						titleColor: "#ffffff",
						messageColor: "#ffffff",
						iconColor: "#ffffff",
						progressBarColor: "#ffffff",
						icon: "fa fa-close",
						timeout: 5e3
					});
					iziToast.destroy(), iziToast.show({
						title: __("发送成功"),
						message: __("您的评论已发送"),
						class: "shadow-sm",
						position: "topRight",
						backgroundColor: "#2dce89",
						titleColor: "#ffffff",
						messageColor: "#ffffff",
						iconColor: "#ffffff",
						progressBarColor: "#ffffff",
						icon: "fa fa-check",
						timeout: 5e3
					}), location.reload()
				},
				error: function (t) {
					$("#post_comment").removeClass("sending"), $("#post_comment_content").removeAttr("disabled"), $("#post_comment_name").removeAttr("disabled"), $("#post_comment_email").removeAttr("disabled"), $("#post_comment_link").removeAttr("disabled"), $("#post_comment_send").removeAttr("disabled"), $("#post_comment_reply_cancel").removeAttr("disabled"), $("#post_comment_send .btn-inner--icon.hide-on-comment-editing").html("<i class='fa fa-send'></i>"), $("#post_comment_send .btn-inner--text.hide-on-comment-editing").html(__("发送")), $("#post_comment").removeClass("show-extra-input post-comment-force-privatemode-on post-comment-force-privatemode-off"), t.isAdmin || $("#post_comment_captcha").removeAttr("disabled"), iziToast.destroy(), iziToast.show({
						title: __("评论发送失败"),
						message: __("未知原因"),
						class: "shadow-sm",
						position: "topRight",
						backgroundColor: "#f5365c",
						titleColor: "#ffffff",
						messageColor: "#ffffff",
						iconColor: "#ffffff",
						progressBarColor: "#ffffff",
						icon: "fa fa-close",
						timeout: 5e3
					})
				}
			}))
		}()
	})
}(), $(document).on("click", "#comment_emotion_btn", function () {
	$("#comment_emotion_btn").toggleClass("comment-emotion-keyboard-open"), lazyloadStickers()
}), $(document).on("click", ".emotion-keyboard .emotion-group-name", function () {
	$(".emotion-keyboard .emotion-group-name.active").removeClass("active"), $(this).addClass("active"), $(".emotion-keyboard .emotion-group:not(d-none)").addClass("d-none"), $(".emotion-keyboard .emotion-group[index='" + $(this).attr("index") + "']").removeClass("d-none"), lazyloadStickers()
}), $(document).on("click", ".emotion-keyboard .emotion-item", function () {
	$("#comment_emotion_btn").removeClass("comment-emotion-keyboard-open"), $(this).hasClass("emotion-item-sticker") ? inputInsertText(" :" + $(this).attr("code") + ": ", document.getElementById("post_comment_content")) : inputInsertText($(this).attr("text"), document.getElementById("post_comment_content"))
}), $(document).on("dragstart", ".emotion-keyboard .emotion-item > img, .comment-sticker", function (t) {
	t.preventDefault()
}), document.addEventListener("click", t => {
	null != document.getElementById("comment_emotion_btn") && ("comment_emotion_btn" == t.target.id || "emotion_keyboard" == t.target.id || document.getElementById("comment_emotion_btn").contains(t.target) || document.getElementById("emotion_keyboard").contains(t.target) || $("#comment_emotion_btn").removeClass("comment-emotion-keyboard-open"))
}), $(document).on("click", ".comment-edited.comment-edithistory-accessible", function () {
	showCommentEditHistory($(this).parent().parent().parent().parent().data("id"))
}), foldLongComments(), $(document).on("click", ".show-full-comment", function () {
	$(this).parent().removeClass("comment-folded").addClass("comment-unfolded")
}), document.addEventListener("error", function (t) {
	let e = $(t.target);
	if (!e.hasClass("avatar")) return;
	if (!e.parent().hasClass("comment-item-avatar")) return;
	let o = e.attr("src").match(/([a-f\d]{32}|[A-F\d]{32})/)[0], n = 0;
	for (i in o) n = (233 * n + o.charCodeAt(i)) % 16;
	let a = $(".comment-name", e.parent().parent()).text().trim()[0];
	e.parent().html('<div class="avatar avatar-40 photo comment-text-avatar" style="background-color: ' + ["#e25f50", "#f25e90", "#bc67cb", "#9672cf", "#7984ce", "#5c96fa", "#7bdeeb", "#45d0e2", "#48b7ad", "#52bc89", "#9ace5f", "#d4e34a", "#f9d715", "#fac400", "#ffaa00", "#ff8b61", "#c2c2c2", "#8ea3af", "#a1877d", "#a3a3a3", "#b0b6e3", "#b49cde", "#c2c2c2", "#7bdeeb", "#bcaaa4", "#aed77f"][n] + ';">' + a + "</div>")
}, !0), $(document).on("submit", ".post-password-form", function () {
	return $("input[type='submit']", this).attr("disabled", "disabled"), $(this).attr("action"), $.pjax.form(this, {
		push: !1,
		replace: !1
	}), !1
}), $(document).on("click", "#comments_navigation .page-item > div", function () {
	$("#comments").addClass("comments-loading"), NProgress.set(.618), url = $(this).attr("href"), $.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		success: function (t) {
			NProgress.done(), $vdom = $(t), $("#comments").html($("#comments", $vdom).html()), $("#comments").removeClass("comments-loading"), $("body,html").animate({scrollTop: $("#comments").offset().top - 100}, 300), foldLongComments(), calcHumanTimesOnPage(), panguInit(), $(".comment-item-text .comment-sticker.lazyload").lazyload(argonConfig.lazyload).removeClass("lazyload")
		},
		error: function () {
			window.location.href = url
		}
	})
}), $(document).on("click", "#comments_more", function () {
	$("#comments_more").attr("disabled", "disabled"), NProgress.set(.618), url = $(this).attr("href"), $.ajax({
		type: "POST",
		url: url,
		data: {no_post_view: "true"},
		dataType: "html",
		success: function (t) {
			NProgress.done(), $vdom = $(t), $("#comments > .card-body > ol.comment-list").append($("#comments > .card-body > ol.comment-list", $vdom).html()), 0 == $("#comments_more", $vdom).length ? ($("#comments_more").remove(), $(".comments-navigation-more").html("<div class='comments-navigation-nomore'>" + __("没有更多了") + "</div>")) : ($("#comments_more").attr("href", $("#comments_more", $vdom).attr("href")), $("#comments_more").removeAttr("disabled")), foldLongComments(), calcHumanTimesOnPage(), panguInit(), $(".comment-item-text .comment-sticker.lazyload").lazyload(argonConfig.lazyload).removeClass("lazyload")
		},
		error: function () {
			window.location.href = url
		}
	})
}), $(window).on("hashchange", function () {
	hash = window.location.hash, gotoHash(hash)
}), $(window).trigger("hashchange"), showPostOutdateToast(), zoomifyInit(), lazyloadInit(), panguInit(), clampInit(), $.pjax.defaults.timeout = 1e4, $.pjax.defaults.container = ["#primary", "#leftbar_part1_menu", "#leftbar_part2_inner", ".page-information-card-container", "#wpadminbar"], $.pjax.defaults.fragment = ["#primary", "#leftbar_part1_menu", "#leftbar_part2_inner", ".page-information-card-container", "#wpadminbar"], $(document).pjax("a[href]:not([no-pjax]):not(.no-pjax):not([target='_blank']):not([download])").on("pjax:click", function (t, e, o) {
	1 != argonConfig.disable_pjax ? (NProgress.remove(), NProgress.start()) : t.preventDefault()
}).on("pjax:afterGetContainers", function (t, e, o) {
	if (o.is("#main article.post-preview a.post-title")) {
		$(".card-div").remove();
		let t = $(o.parents("article.post-preview")[0]);
		t.append("<div class='loading-css-animation'><div class='loading-dot loading-dot-1' ></div><div class='loading-dot loading-dot-2' ></div><div class='loading-dot loading-dot-3' ></div><div class='loading-dot loading-dot-4' ></div><div class='loading-dot loading-dot-5' ></div><div class='loading-dot loading-dot-6' ></div><div class='loading-dot loading-dot-7' ></div><div class='loading-dot loading-dot-8' ></div></div></div>"), t.addClass("post-pjax-loading"), $("#main").addClass("post-list-pjax-loading");
		let e = $(t).offset().top - $("#main").offset().top;
		t.css("transform", "translateY(-" + e + "px)"), $("body,html").animate({scrollTop: 0}, 450)
	}
}).on("pjax:send", function () {
	NProgress.set(.618)
}).on("pjax:beforeReplace", function (t, e) {
	$("#post_comment", e[0]).length > 0 ? $("#fabtn_go_to_comment").removeClass("d-none") : $("#fabtn_go_to_comment").addClass("d-none")
}).on("pjax:complete", function () {
	pv(), -1 != window.location.href.search("article") && $.getScript("/blog/argon/js/article.js"), NProgress.inc();
	try {
		null != MathJax && MathJax.typeset()
	} catch (t) {
	}
	try {
		$("script#mathjax_v2_script", $vdom).length > 0 && MathJax.Hub.Typeset()
	} catch (t) {
	}
	try {
		null != renderMathInElement && renderMathInElement(document.body, {
			delimiters: [{
				left: "$$",
				right: "$$",
				display: !0
			}, {left: "$", right: "$", display: !1}, {left: "\\(", right: "\\)", display: !1}]
		})
	} catch (t) {
	}
	if (lazyloadInit(), zoomifyInit(), highlightJsRender(), panguInit(), clampInit(), getGithubInfoCardContent(), showPostOutdateToast(), foldLongComments(), "function" == typeof window.pjaxLoaded) try {
		window.pjaxLoaded()
	} catch (t) {
		console.error(t)
	}
	NProgress.done()
}).on("pjax:end", function () {
	lazyloadInit()
}), $(document).on("click", "#blog_tags .tag", function () {
	$("#blog_tags button.close").trigger("click")
}), $(document).on("click", "#blog_categories .tag", function () {
	$("#blog_categories button.close").trigger("click")
}), $(document).on("click", "#fabtn_open_sidebar,#open_sidebar", function () {
	$("html").addClass("leftbar-opened")
}), $(document).on("click", "#sidebar_mask", function () {
	$("html").removeClass("leftbar-opened")
}), $(document).on("click", "#leftbar a[href]:not([no-pjax]):not([href^='#'])", function () {
	$("html").removeClass("leftbar-opened")
}), $(document).on("click", "#navbar_global.show .navbar-nav a[href]:not([no-pjax]):not([href^='#'])", function () {
	$("#navbar_global .navbar-toggler").click()
}), $(document).on("click", "#navbar_global.show #navbar_search_btn_mobile", function () {
	$("#navbar_global .navbar-toggler").click()
}), $(document).on("click", ".collapse-block .collapse-block-title", function () {
	let t = ".collapse-block[collapse-id='" + this.getAttribute("collapse-id") + "']";
	$(t).toggleClass("collapsed"), $(t).hasClass("collapsed") ? $(t + " .collapse-block-body").stop(!0, !1).slideUp(200) : $(t + " .collapse-block-body").stop(!0, !1).slideDown(200), $("html").trigger("scroll")
}), getGithubInfoCardContent(), $(document).on("click", ".shuoshuo-upvote", function () {
	$this = $(this), ID = $this.attr("data-id"), $this.addClass("shuoshuo-upvoting"), $.ajax({
		url: argonConfig.wp_path + "wp-admin/admin-ajax.php",
		type: "POST",
		dataType: "json",
		data: {action: "upvote_shuoshuo", shuoshuo_id: ID},
		success: function (t) {
			$this.removeClass("shuoshuo-upvoting"), "success" == t.status ? ($(".shuoshuo-upvote-num", $this).html(t.total_upvote), $("i.fa-thumbs-o-up", $this).addClass("fa-thumbs-up").removeClass("fa-thumbs-o-up"), $this.addClass("upvoted"), $this.addClass("shuoshuo-upvoted-animation"), iziToast.show({
				title: t.msg,
				class: "shadow-sm",
				position: "topRight",
				backgroundColor: "#2dce89",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-check",
				timeout: 5e3
			})) : ($(".shuoshuo-upvote-num", $this).html(t.total_upvote), iziToast.show({
				title: t.msg,
				class: "shadow-sm",
				position: "topRight",
				backgroundColor: "#f5365c",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-close",
				timeout: 5e3
			}))
		},
		error: function (t) {
			$this.removeClass("shuoshuo-upvoting"), iziToast.show({
				title: __("点赞失败"),
				class: "shadow-sm",
				position: "topRight",
				backgroundColor: "#f5365c",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-close",
				timeout: 5e3
			})
		}
	})
}), "true" == $("meta[name='argon-enable-custom-theme-color']").attr("content")) {
	let t = new Pickr({
		el: "#theme-color-picker",
		container: "body",
		theme: "monolith",
		closeOnScroll: !1,
		appClass: "theme-color-picker-box",
		useAsButton: !1,
		padding: 8,
		inline: !1,
		autoReposition: !0,
		sliders: "h",
		disabled: !1,
		lockOpacity: !0,
		outputPrecision: 0,
		comparison: !1,
		default: $("meta[name='theme-color']").attr("content"),
		swatches: ["#5e72e4", "#fa7298", "#009688", "#607d8b", "#2196f3", "#3f51b5", "#ff9700", "#109d58", "#dc4437", "#673bb7", "#212121", "#795547"],
		defaultRepresentation: "HEX",
		showAlways: !1,
		closeWithKey: "Escape",
		position: "top-start",
		adjustableNumbers: !1,
		components: {
			palette: !0,
			preview: !0,
			opacity: !1,
			hue: !0,
			interaction: {hex: !0, rgba: !0, hsla: !1, hsva: !1, cmyk: !1, input: !0, clear: !1, cancel: !0, save: !0}
		},
		strings: {save: __("确定"), clear: __("清除"), cancel: __("恢复博客默认")}
	});
	t.on("change", t => {
		updateThemeColor(pickrObjectToHEX(t), !0)
	}), t.on("save", (e, o) => {
		updateThemeColor(pickrObjectToHEX(o._color), !0), t.hide()
	}), t.on("cancel", e => {
		t.hide(), t.setColor($("meta[name='theme-color-origin']").attr("content").toUpperCase()), updateThemeColor($("meta[name='theme-color-origin']").attr("content").toUpperCase(), !1), setCookie("argon_custom_theme_color", "", 0)
	})
}

function pickrObjectToHEX(t) {
	let e = t.toHEXA();
	return ("#" + e[0] + e[1] + e[2]).toUpperCase()
}

function updateThemeColor(t, e) {
	let o = t, n = hex2str(o), a = hex2rgb(o), s = rgb2hsl(a.R, a.G, a.B),
		i = hsl2rgb(s.h, s.s, Math.max(s.l - .025, 0)), r = rgb2hex(i.R, i.G, i.B),
		l = hsl2rgb(s.h, s.s, Math.max(s.l - .05, 0)), c = rgb2hex(l.R, l.G, l.B),
		m = hsl2rgb(s.h, s.s, Math.max(s.l - .1, 0)), d = rgb2hex(m.R, m.G, m.B),
		f = hsl2rgb(s.h, s.s, Math.max(s.l - .15, 0)), p = rgb2hex(f.R, f.G, f.B),
		u = hsl2rgb(s.h, s.s, Math.min(s.l + .1, 1)), g = rgb2hex(u.R, u.G, u.B);
	document.documentElement.style.setProperty("--themecolor", o), document.documentElement.style.setProperty("--themecolor-dark0", r), document.documentElement.style.setProperty("--themecolor-dark", c), document.documentElement.style.setProperty("--themecolor-dark2", d), document.documentElement.style.setProperty("--themecolor-dark3", p), document.documentElement.style.setProperty("--themecolor-light", g), document.documentElement.style.setProperty("--themecolor-rgbstr", n), rgb2gray(a.R, a.G, a.B) < 50 ? $("html").addClass("themecolor-toodark") : $("html").removeClass("themecolor-toodark"), $("meta[name='theme-color']").attr("content", o), $("meta[name='theme-color-rgb']").attr("content", n), e && setCookie("argon_custom_theme_color", o, 365)
}

function typeEffect(t, e, o, n) {
	t.classList.add("typing-effect"), o > e.length ? setTimeout(function () {
		t.classList.remove("typing-effect")
	}, 1e3 - n * o % 1e3 - 50) : (t.innerText = e.substring(0, o), setTimeout(function () {
		typeEffect(t, e, o + 1, n)
	}, n))
}

function randomString(t) {
	t = t || 32;
	let e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", o = "";
	for (let n = 0; n < t; n++) o += e.charAt(Math.floor(Math.random() * e.length));
	return o
}

!function () {
	let t = 0, e = null;
	$(document).on("click", ".comment-item-text .comment-image", function () {
		$(".comment-image-preview", this).attr("data-easing", "cubic-bezier(0.4, 0, 0, 1)"), $(".comment-image-preview", this).attr("data-duration", "500"), $(this).hasClass("comment-image-preview-zoomed") ? (clearInterval(t), e = null, $(this).removeClass("comment-image-preview-zoomed"), $(".comment-image-preview", this).zoomify("zoomOut")) : (e = this, $(this).addClass("comment-image-preview-zoomed"), $(this).hasClass("loaded") || $(".comment-image-preview", this).attr("src", $(this).attr("data-src")), $(".comment-image-preview", this).zoomify("zoomIn"), $(this).hasClass("loaded") || (t = setInterval(function () {
			0 != e.width && ($("html").trigger("scroll"), $(e).addClass("loaded"), clearInterval(t), e = null)
		}, 50)))
	})
}(), $bannerTitle = $(".banner-title"), null != $bannerTitle.data("text") && typeEffect($(".banner-title-inner")[0], $bannerTitle.data("text"), 0, $bannerTitle.data("interval")), $(".hitokoto").length > 0 && $.ajax({
	type: "GET",
	url: "https://v1.hitokoto.cn",
	success: function (t) {
		$(".hitokoto").text(t.hitokoto)
	},
	error: function (t) {
		$(".hitokoto").text(__("Hitokoto 获取失败"))
	}
});
var codeOfBlocks = {};

function getCodeFromBlock(t) {
	if (null != codeOfBlocks[t.id]) return codeOfBlocks[t.id];
	let e = $(".hljs-ln-code", t), o = "";
	for (let t = 0; t < e.length - 1; t++) o += e[t].innerText, o += "\n";
	return o += e[e.length - 1].innerText, codeOfBlocks[t.id] = o, o
}

function highlightJsRender() {
	"undefined" != typeof hljs && "undefined" != typeof argonEnableCodeHighlight && argonEnableCodeHighlight && ($("article pre.code").each(function (t, e) {
		$(e).hasClass("no-hljs") || $(e).html("<code>" + $(e).html() + "</code>")
	}), $("article pre > code").each(function (t, e) {
		if ($(e).hasClass("no-hljs")) return;
		$(e).parent().attr("id", randomString()), hljs.highlightBlock(e), hljs.lineNumbersBlock(e, {singleLine: !0}), $(e).parent().addClass("hljs-codeblock"), $(e).attr("hljs-codeblock-inner", "");
		let o = "copy_btn_" + randomString();
		$(e).parent().append('<div class="hljs-control hljs-title">\n\t\t\t\t<div class="hljs-control-btn hljs-control-toggle-linenumber" tooltip-hide-linenumber="' + __("隐藏行号") + '" tooltip-show-linenumber="' + __("显示行号") + '">\n\t\t\t\t\t<i class="fa fa-list"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class="hljs-control-btn hljs-control-toggle-break-line" tooltip-enable-breakline="' + __("开启折行") + '" tooltip-disable-breakline="' + __("关闭折行") + '">\n\t\t\t\t\t<i class="fa fa-align-left"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class="hljs-control-btn hljs-control-copy" id=' + o + ' tooltip="' + __("复制") + '">\n\t\t\t\t\t<i class="fa fa-clipboard"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class="hljs-control-btn hljs-control-fullscreen" tooltip-fullscreen="' + __("全屏") + '" tooltip-exit-fullscreen="' + __("退出全屏") + '">\n\t\t\t\t\t<i class="fa fa-arrows-alt"></i>\n\t\t\t\t</div>\n\t\t\t</div>');
		let n = new ClipboardJS("#" + o, {
			text: function (t) {
				return getCodeFromBlock($(e).parent()[0])
			}
		});
		n.on("success", function (t) {
			iziToast.show({
				title: __("复制成功"),
				message: __("代码已复制到剪贴板"),
				class: "shadow",
				position: "topRight",
				backgroundColor: "#2dce89",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-check",
				timeout: 5e3
			})
		}), n.on("error", function (t) {
			iziToast.show({
				title: __("复制失败"),
				message: __("请手动复制代码"),
				class: "shadow",
				position: "topRight",
				backgroundColor: "#f5365c",
				titleColor: "#ffffff",
				messageColor: "#ffffff",
				iconColor: "#ffffff",
				progressBarColor: "#ffffff",
				icon: "fa fa-close",
				timeout: 5e3
			})
		})
	}))
}

function addPreZero(t, e) {
	for (var o = t.toString().length; o < e;) t = "0" + t, o++;
	return t
}

function humanTimeDiff(t) {
	let e = new Date, o = e - (t = new Date(t));
	if (o < 0 && (o = 0), o < 6e4) return __("刚刚");
	if (o < 36e5) return parseInt(o / 6e4) + " " + __("分钟前");
	if (o < 864e5) return parseInt(o / 36e5) + " " + __("小时前");
	let n = new Date(e - 864e5);
	if (n.setHours(0), n.setMinutes(0), n.setSeconds(0), n.setMilliseconds(0), t > n) return __("昨天") + " " + t.getHours() + ":" + addPreZero(t.getMinutes(), 2);
	let a = new Date(e - 1728e5);
	if (a.setHours(0), a.setMinutes(0), a.setSeconds(0), a.setMilliseconds(0), t > a && 0 == argonConfig.language.indexOf("zh")) return __("前天") + " " + t.getHours() + ":" + addPreZero(t.getMinutes(), 2);
	if (o < 2592e6) return parseInt(o / 864e5) + " " + __("天前");
	let s = new Date(e);
	return s.setMonth(0), s.setDate(1), s.setHours(0), s.setMinutes(0), s.setSeconds(0), s.setMilliseconds(0), t > s ? "YMD" == argonConfig.dateFormat || "MDY" == argonConfig.dateFormat ? t.getMonth() + 1 + "-" + t.getDate() : t.getDate() + "-" + (t.getMonth() + 1) : "YMD" == argonConfig.dateFormat ? t.getFullYear() + "-" + (t.getMonth() + 1) + "-" + t.getDate() : "MDY" == argonConfig.dateFormat ? t.getDate() + "-" + (t.getMonth() + 1) + "-" + t.getFullYear() : "DMY" == argonConfig.dateFormat ? t.getDate() + "-" + (t.getMonth() + 1) + "-" + t.getFullYear() : void 0
}

$(document).ready(function () {
	highlightJsRender()
}), $(document).on("click", ".hljs-control-fullscreen", function () {
	let t = $(this).parent().parent();
	t.toggleClass("hljs-codeblock-fullscreen"), t.hasClass("hljs-codeblock-fullscreen") ? $("html").addClass("noscroll codeblock-fullscreen") : $("html").removeClass("noscroll codeblock-fullscreen")
}), $(document).on("click", ".hljs-control-toggle-break-line", function () {
	$(this).parent().parent().toggleClass("hljs-break-line")
}), $(document).on("click", ".hljs-control-toggle-linenumber", function () {
	$(this).parent().parent().toggleClass("hljs-hide-linenumber")
}), console.log("%cBuild: %c%cBy  清欢", "color: rgba(255,255,255,.6); background: #5e72e4; font-size: 15px;border-radius:5px 0 0 5px;padding:10px 0 10px 20px;", "color: rgba(255,255,255,1); background: #5e72e4; font-size: 15px;border-radius:0;padding:10px 15px 10px 0px;", "color: #fff; background: #92A1F4; font-size: 15px;border-radius:0 5px 5px 0;padding:10px 20px 10px 15px;");