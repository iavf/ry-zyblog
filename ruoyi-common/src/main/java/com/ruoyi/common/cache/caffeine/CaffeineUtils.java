//package com.ruoyi.common.cache.caffeine;
//
//import com.ruoyi.common.utils.spring.SpringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.cache.Cache;
//import org.springframework.cache.support.SimpleCacheManager;
//
//import java.util.Collection;
//
//public class CaffeineUtils {
//    private static Logger logger = LoggerFactory.getLogger(CaffeineUtils.class);
//    private static SimpleCacheManager caffeineCacheManager = SpringUtils.getBean("caffeineCacheManager");
//    /**
//     * 清空指定name下的缓存
//     *
//     * @param cacheName
//     */
//
//    public static void clear(String cacheName) {
//        Cache cache = caffeineCacheManager.getCache(cacheName);
//        cache.clear();
//        caffeineCacheManager.getCache(cacheName).clear();
//    }
//
//
//    /**
//     * 清空指定name下指定value的缓存
//     *
//     * @param cacheName
//     * @param key
//     */
//    public static void remove(String cacheName, String key) {
//        getCache(cacheName).evict(key);
//    }
//
//    /**
//     * 清空所有缓存
//     */
//    public static void clear() {
//        Collection<String> cacheNames = caffeineCacheManager.getCacheNames();
//        for (String cachename : cacheNames) {
//            caffeineCacheManager.getCache(cachename).clear();
//            logger.info("---------------------"+cachename+"缓存被清除了");
//        }
////        System.out.println(object.toString());
//    }
//
//
//    /**
//     * 获取缓存值
//     *
//     * @param cacheName 缓存的名称
//     * @param key       缓存的键
//     * @return
//     */
//    public static Object get(String cacheName, String key) {
//        Object object = null;
//        if (getCache(cacheName).get(key) != null) {
//            object = getCache(cacheName).get(key).get();
//
//        }
//        return object ;
//    }
//
//
//    /**
//     * 写入缓存
//     *
//     * @param cacheName
//     * @param key
//     * @param value
//     */
//
//    public static void put(String cacheName, String key, Object value) {
//        getCache(cacheName).put(key, value);
//    }
//
//
//    /**
//     * 获得一个Cache，没有则创建一个。
//     *
//     * @param cacheName
//     * @return
//     */
//    public static Cache getCache(String cacheName) {
//        Cache cache = caffeineCacheManager.getCache(cacheName);
//        if (cache == null) {
//            caffeineCacheManager.initializeCaches();
//        }
//        return cache;
//    }
//
//    public static SimpleCacheManager getCacheManager() {
//        return caffeineCacheManager;
//    }
//
//    public static Object get(String cacheName, String cacheKey, String cacheValue) {
//        Cache cache = caffeineCacheManager.getCache(cacheName);
//        return cache.get(cacheKey);
//    }
//}
