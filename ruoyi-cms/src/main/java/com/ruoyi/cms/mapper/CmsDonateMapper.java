package com.ruoyi.cms.mapper;


import com.ruoyi.cms.domain.CmsDonate;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity generate.CmsDonate
 */
public interface CmsDonateMapper {
    /**
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated
     */
    int insert(CmsDonate record);

    /**
     * @mbg.generated
     */
    int insertSelective(CmsDonate record);

    /**
     * @mbg.generated
     */
    CmsDonate selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(CmsDonate record);

    /**
     * @mbg.generated
     */
    int updateByPrimaryKey(CmsDonate record);

    List<CmsDonate> selectDonateList(CmsDonate cmsDonate);

    @Select("select sum(donate_money) from cms_donate where money_type='+'")
    String  selectAllInCount();

    @Select("select sum(donate_money) from cms_donate where money_type='-'")
    String  selectAllOutCount();

    @Select("select Max(donate_time) from cms_donate ")
    String selectlastTime();

//    @Results({
//            @Result(property = "PickID", column = "PickID"),
//            @Result(property = "date", column = "date"),
//            @Result(property = "code", column = "code")}
//    )
//    @Select("select donate_from_name,donate_money,donate_msg,donate_time,donate_mark,donate_type,money_type  from cms_donate where money_type ='-'")
    List<CmsDonate> selectPayList(CmsDonate cmsDonate);

    List<CmsDonate> selectMoneyList(CmsDonate cmsDonate);
}