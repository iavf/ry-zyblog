package com.ruoyi.cms.service;

import com.ruoyi.cms.domain.CmsDonate;

/**
 * 打赏管理Service接口
 *
 * @author wujiyue
 * @date 2019-10-28
 */
public interface IDonateService {

    /**
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * @mbg.generated
     */
    int insert(CmsDonate record);

    /**
     * @mbg.generated
     */
    int insertSelective(CmsDonate record);

    /**
     * @mbg.generated
     */
    CmsDonate selectByPrimaryKey(Integer id);

    /**
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(CmsDonate record);

    /**
     * @mbg.generated
     */
    int updateByPrimaryKey(CmsDonate record);

}
