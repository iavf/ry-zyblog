var clientType = "";
function browserRedirect() {
  var sUserAgent = navigator.userAgent.toLowerCase();
  var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
  var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
  var bIsMidp = sUserAgent.match(/midp/i) == "midp";
  var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
  var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
  var bIsAndroid = sUserAgent.match(/android/i) == "android";
  var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
  var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
  if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
    clientType = "mobile";
  } else {
    clientType = "pc";
  }
}
browserRedirect();

function contactUs (flag, url) {
  var iframeurlpc = "https://mk.yonyou.com/form/pc.html?id=5e804fa4b001a600016d7794";
  var iframeurlmobile = "https://mk.yonyou.com/form/mb.html?id=5e804fa4b001a600016d7794&jump=1";
  // flag 0留咨；1下载 2:yonsuite
  if (flag == 1 && window.localStorage && localStorage.getItem('yy_form_flag')) {
    window.open(url ? url : pdfUrl)
  } else if (flag == 2) {
    if (clientType == "pc") {
      iframe.src = "https://mk.yonyou.com/form/pc.html?id=5e785c80b001a600016d76f8";
      updateIframeHeight();
      $('#index-tost-box').slideDown();
    } else {
      window.open("https://mk.yonyou.com/form/mb.html?id=5e785c80b001a600016d76f8&returnUrl=https://www.yonyou.com");
    }
  } else {
    if (clientType == "pc") {
      if (flag == 1) {
        iframe.src = iframeurlpc + '&returnUrl=' +  (url ? url : pdfUrl)
      } else {
        iframe.src = iframeurlpc
      }
      updateIframeHeight();
      $('#index-tost-box').slideDown();
    } else {
      window.open(iframeurlmobile + "&returnUrl=https://www.yonyou.com");
    }
  }
};

var iframe = document.getElementById('mainIframe');
var iframe_cw = iframe.contentWindow;
function iFrameHeight(val) {
  var ifm= document.getElementById("mainIframe");
  ifm.style.height = 'auto';
  ifm.style.height = val + (iframe.src.indexOf('mb.html') > 0 ? 290 : 0) + 'px';
  ifm.style.width =  '100%';
};

function updateIframeHeight() {
  iframe_cw.postMessage({height: 0}, "*");
};

window.addEventListener('message', function (event) {
  if (event.data.height) {
    iFrameHeight(event.data.height)
  }
  if (event.data.isSubmit) {
    localStorage.setItem('yy_form_flag', '1')
  }
}, false);