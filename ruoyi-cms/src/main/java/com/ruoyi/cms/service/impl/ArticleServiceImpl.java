package com.ruoyi.cms.service.impl;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.*;
import com.google.common.collect.Lists;
import com.ruoyi.cms.domain.*;
import com.ruoyi.cms.mapper.ArticleMapper;
import com.ruoyi.cms.mapper.TagsMapper;
import com.ruoyi.cms.service.IArticleService;
import com.ruoyi.cms.service.ICategoryService;
import com.ruoyi.cms.service.ICommentService;
import com.ruoyi.common.cache.bean.CacheConstant;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.ehcache.util.EhCacheUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static cn.hutool.log.level.Level.INFO;

/**
 * 文章管理Service业务层处理
 *
 * @author wujiyue
 * @date 2019-10-28
 */
@Service
public class ArticleServiceImpl implements IArticleService {

    @CreateCache(expire = 60 * 60 * 6, name = CacheConstant.CATEGORY_CACHENAME, cacheType = CacheType.REMOTE)
    private Cache<String, Object> categoryCache;

    @CreateCache(expire = 60 * 60 * 6, name = CacheConstant.TAG_CACHE_KEY, cacheType = CacheType.REMOTE)
    private Cache<String, Object> tagCache;

    @CreateCache(expire = 60 * 60 * 6, name = CacheConstant.ALL_ARTICLE_SIMPLE_INFO_JSON)
    private Cache<String, Object> selectSimpleArticlesSelectJson;

    @CreateCache(name = CacheConstant.ARTICLE_CACHE_NAME)
    private Cache<String, Object> articleCache;


    @CreateCache(name = CacheConstant.ALL_ARTICLE_SIMPLE_INFO)
    private Cache<String, Object> simpleArticlesCache;

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private TagsMapper tagsMapper;
    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private ICommentService commentService;
    private static final Log log = LogFactory.get();


    /**
     * 在文章列表中根据文章阅读数排序
     */
    public List<Article> sortAtricleListByHit(List<Article> articlList) {
        articlList = articlList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Article::getId))), ArrayList::new));
        return articlList.stream().sorted(Comparator.comparing(Article::getHit).reversed()).limit(10).collect(Collectors.toList());
    }

    /**
     * 查询文章管理
     *
     * @param id 文章管理ID
     * @return 文章管理
     */
    @Override
    @Cached(expire = 3600, name = CacheConstant.ARTICLE_CACHE_NAME
            ,key="#articleId", cacheType = CacheType.REMOTE)
    public Article selectArticleById(String articleId) {

        Article article = articleMapper.selectArticleById(articleId);
        //文章内容
        Map<String, Object> m = articleMapper.getArticleContent(articleId);
        if (m != null) {
            article.setContent(String.valueOf(m.get("content")));
            article.setContent_markdown_source(String.valueOf(m.get("content_markdown_source")));
            if (m.containsKey("resource_content")) {
                article.setResourceContent(String.valueOf(m.get("resource_content")));
            }
            selectCategory(article);
            selectTags(article);

            //评论列表
            List<Comment> commentList = commentService.selectRootCommentByArticleId(articleId);
            article.setCommentList(commentList);

            //查找相同分类文章中最热门的
            List<Article> hotArticleListByCategoryId = sortAtricleListByHit(articleMapper.selectHotArticleListByCategoryId(article.getCategoryId()));
            article.setSameCategoryIdArticlesByHit(hotArticleListByCategoryId);

            //查找相同标签文章中最热门的。
            List<HashMap> hotArticleListByTagIds = new ArrayList<>();
            List<Tags> tagList = article.getTagList();
            if (CollectionUtils.isNotEmpty(tagList)) {
                for (int i = 0; i < tagList.size(); i++) {
                    HashMap objectObjectHashMap = new HashMap<>();
                    List<Article> hotArticleListByTagId = articleMapper.selectHotArticleListByTagId(tagList.get(i).getTagId());
                    objectObjectHashMap.put("name", tagList.get(i).getTagName());
                    objectObjectHashMap.put("id", tagList.get(i).getTagId());
                    objectObjectHashMap.put("list", hotArticleListByTagId);
                    hotArticleListByTagIds.add(objectObjectHashMap);
                }
            }
            article.setSameTagsArticlesByHit(hotArticleListByTagIds);

            //查询上级文章
            ArrayList<Article> upArticlesList = new ArrayList<>();

            String upArticles = article.getUpArticles();
            if (StringUtils.isNotEmpty(upArticles)) {
                if (upArticles.endsWith(",")) {
                    upArticles = upArticles.substring(0, upArticles.length() - 1);
                }
                String[] arr = Convert.toStrArray(upArticles);
                List<String> list = Lists.newArrayList(arr);
                list.forEach(t -> {
                    upArticlesList.add(articleMapper.selectArticleById(t));
                });
            }
            article.setUpArticlesList(upArticlesList);
            log.info("DB查询文章id:"+articleId+"_title:"+article.getTitle());
            return article;
        } else {
            return article;
        }

    }

    /**
     * 查询文章管理列表
     *
     * @param article 文章管理
     * @return 文章管理
     */
    @Override
    public List<Article> selectArticleList(Article article) {
        List<Article> articles = articleMapper.selectArticleListSimple(article);
        selectTags(articles);
        selectCategory(articles);
        return articles;
    }

    public int getSeriesId() {
        List<String> ids = articleMapper.selectAtricleAllId();
        int i = 1;
        while (ids.contains(String.valueOf(i))) {
            i++;
        }
        return i;
    }

    /**
     * 新增文章管理
     *
     * @param article 文章管理
     * @return 结果
     */
    @Override
    @Transactional
//    @Caching(evict = {
//            @CacheEvict(cacheNames = CacheConstant.BLOGINFO_CACHE_NAME, key = CacheConstant.YH + CacheConstant.ALL_ARTICLE_SIMPLE_INFO + CacheConstant.YH),
//            @CacheEvict(cacheNames = CacheConstant.BLOGINFO_CACHE_NAME, key = CacheConstant.YH + CacheConstant.SELECT_RECENT_ARTICLE_LIST_BY_CREATE_TIME_KEY + CacheConstant.YH)
//    })
    public int insertArticle(Article article) {
        if (selectSimpleArticlesSelectJson.remove(CacheConstant.ALL_ARTICLE_SIMPLE_INFO_JSON)){
            log.info("selectSimpleArticlesSelectJson删除了");
        }
        if (simpleArticlesCache.remove(CacheConstant.ALL_ARTICLE_SIMPLE_INFO)){
            log.info("simpleArticlesCache删除key"+CacheConstant.ALL_ARTICLE_SIMPLE_INFO);
        }
        if (selectSimpleArticlesSelectJson.remove(CacheConstant.ALL_ARTICLE_SIMPLE_INFO_JSON)){
            log.info("selectSimpleArticlesSelectJson删除了");
        }
        article.setId(String.valueOf(getSeriesId()));
        article.setCreateTime(DateUtils.getNowDate());
        article.setUpdateTime(DateUtils.getNowDate());
        SysUser user = ShiroUtils.getSysUser();
        article.setYhid(user.getUserId().toString());
        article.setDeleted(0);
        String tags = article.getTags();
        if (StringUtils.isNotEmpty(tags)) {
            if (!tags.endsWith(",")) {
                tags += ",";
                article.setTags(tags);
            }
        }
        article.setAuthor(user.getUserName());

        if (article.getCommentFlag() == null) {
            article.setCommentFlag("0");
        }
        if ("on".equals(article.getCommentFlag())) {
            article.setCommentFlag("1");
        }
        if ("off".equals(article.getCommentFlag())) {
            article.setCommentFlag("0");
        }
        int n = 0;
        n = articleMapper.insertArticle(article);
        n += articleMapper.insertArticleContent(article);
        return n;
    }

    /**
     * 修改文章管理
     *
     * @param article 文章管理
     * @return 结果
     */
    @Override
    @Transactional
    public int updateArticle(Article article) {
        if (articleCache.remove(article.getId())){
            log.info("articleCache删除key"+article.getId());
        }
        if (simpleArticlesCache.remove(CacheConstant.ALL_ARTICLE_SIMPLE_INFO)){
            log.info("simpleArticlesCache删除key"+CacheConstant.ALL_ARTICLE_SIMPLE_INFO);
        }
        if (selectSimpleArticlesSelectJson.remove(CacheConstant.ALL_ARTICLE_SIMPLE_INFO_JSON)){
            log.info("selectSimpleArticlesSelectJson删除了");
        }
        article.setUpdateTime(DateUtils.getNowDate());
        String tags = article.getTags();
        if (StringUtils.isNotEmpty(tags)) {
            if (!tags.endsWith(",")) {
                tags += ",";
                article.setTags(tags);
            }
        }

        String upArticles = article.getUpArticles();
        if (StringUtils.isNotEmpty(upArticles)) {
            if (!upArticles.endsWith(",")) {
                upArticles += ",";
                article.setUpArticles(upArticles);
            }
        }
        if (article.getCommentFlag() == null) {
            article.setCommentFlag("0");
        }
        if ("on".equals(article.getCommentFlag())) {
            article.setCommentFlag("1");
        }
        if ("off".equals(article.getCommentFlag())) {
            article.setCommentFlag("0");
        }
//        log.info("-----更新文章内容"+article.toString());
        int n = articleMapper.updateArticle(article);
        n += articleMapper.updateArticleContent(article);
        return n;
    }

    /**
     * 删除文章管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteArticleByIds(String ids) {
        articleMapper.deleteArticleContentByIds(Convert.toStrArray(ids));
        return articleMapper.deleteArticleByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文章管理信息
     *
     * @param articleId 文章管理ID
     * @return 结果
     */
    @Override
    //@CacheEvict(cacheNames = {CacheConstant.BLOGINFO_CACHE_NAME, "article"}, key = "#articleId")
    public int deleteArticleById(String articleId) {
        articleMapper.deleteArticleContentById(articleId);
        return articleMapper.deleteArticleById(articleId);
    }

    @Override
    public List<Article> selectArticlesByArticleRegionType(ArticleRegionType articleRegionType) {
        List<Article> list = this.selectArticlesByArticleRegionTypeInDb(articleRegionType);
        EhCacheUtils.putSysInfo("ArticleRegionType_", articleRegionType.getVal(), list);
        selectCategory(list);
        selectTags(list);
        return list;
    }

    @Override
    public List<Article> selectArticlesRegionNotNull(Article article) {
        List<Article> list = articleMapper.selectArticlesRegionNotNull(article);
        selectCategory(list);
        selectTags(list);
        return list;
    }

    @Override
    public List<Article> selectArticlesRegionIsNull(Article article) {
        List<Article> list = articleMapper.selectArticlesRegionIsNull(article);
        selectCategory(list);
        selectTags(list);
        return list;
    }

    @Override
    public int upVote(String id) {
        return articleMapper.upVote(id);
    }

    @Override
    public int articleLook(String id) {
        return articleMapper.articleLook(id);
    }

    private List<Article> selectArticlesByArticleRegionTypeInDb(ArticleRegionType articleRegionType) {
        Article queryForm = new Article();
        queryForm.setArticleRegion(articleRegionType.getVal());
        List<Article> list = this.selectArticleList(queryForm);
        selectCategory(list);
        return list;
    }

    @Override
    public void selectCategory(List<Article> list) {
        list.forEach(a -> {
            String cid = a.getCategoryId();
            Category category = null;
            category = (Category) categoryCache.get(cid);
            if (category == null) {
                //如果Jetcache查不出来，查询数据库
                if (StringUtils.isNotEmpty(cid)){
                    category = categoryService.selectCategoryById(Long.valueOf(cid));
                    //如果数据库查到了，
                    if (category != null) {
                        log.log(INFO, "在数据库中查找类别：" + category.getCategoryName());
                        categoryCache.put(cid, category);
                        a.setCategory(category);
                        return;
                    }
                }
            } else {
                //如果Jetcache查出来了
//                log.info("Jetcache查询类别:" + category.getCategoryName());
                a.setCategory(category);
            }
            if (category != null) {
                a.setCategory(category);
            }
        });
    }

    @Override
    public void selectCategory(Article a) {
        if (a != null) {
            String cid = a.getCategoryId();
            Category category = null;
            category = (Category) categoryCache.get(cid);
//            Category category = (Category) CaffeineUtils.get(CacheConstant.CATEGORY_CACHENAME, CacheConstant.CATEGORY_KEY + cid);
            if (category == null) {
                category = categoryService.selectCategoryById(Long.valueOf(cid));
                if (category != null) {
                    categoryCache.put(cid, category);
//                    CaffeineUtils.put(CacheConstant.CATEGORY_CACHENAME, CacheConstant.CATEGORY_KEY + cid, category);
                }
            }
            if (category != null) {
                a.setCategory(category);
            }
        }


    }

    @Override
    public void selectTags(List<Article> articles) {
        //转换标签名称，这部分可以使用缓存提升性能
        articles.forEach(a -> {
            String tagsStr = a.getTags();
            if (StringUtils.isNotEmpty(tagsStr)) {
                String[] tagsArr = Convert.toStrArray(tagsStr);
                String tagsName = "";
                List<Tags> tags = Lists.newArrayList();
                for (String id : tagsArr) {
                    if (StringUtils.isNotEmpty(id)) {
                        Tags tag = null;
                        tag = (Tags) tagCache.get(id);
                        if (tag == null) {
                            //如果Jetcache没查到用数据库查
                            tag = tagsMapper.selectTagsById(Long.valueOf(id));
                            tagCache.put(id, tag);
                            log.log(INFO, "数据库查询标签：" + tag.getTagName());
                        } else {
//                            log.log(INFO, "Jetcache查询标签：" + tag.getTagName());
                        }
                        tags.add(tag);
                        if (tag != null) {
                            tagsName += tag.getTagName() + ",";
                        }
                    }
                }
                if (tagsName.endsWith(",")) {
                    tagsName = StringUtils.substring(tagsName, 0, tagsName.length() - 1);
                }
                a.setTags_name(tagsName);
                a.setTagList(tags);
            }
        });


    }

    @Override
    public List<Article> selectRecentArticleListByCreateTime(Integer limit) {
        List<Article> articles = articleMapper.selectRecentArticleListByCreateTime(limit);
        selectCategory(articles);
        selectTags(articles);
        return articles;
    }

    @Override
    public List<Article> selectHotArticleListByCategoryId(String categoryId) {
        return articleMapper.selectHotArticleListByCategoryId(categoryId);
    }

    @Override
    public List<Article> selectHotArticleListByTagId(Long tagId) {
        return articleMapper.selectHotArticleListByTagId(tagId);
    }

    //@Cacheable(cacheNames = CacheConstant.BLOGINFO_CACHE_NAME, key = "#root.methodName")
    @Override
    @Cached(expire = 3600,name = CacheConstant.ALL_ARTICLE_SIMPLE_INFO,
            key = CacheConstant.YH+CacheConstant.ALL_ARTICLE_SIMPLE_INFO+CacheConstant.YH,
            cacheType = CacheType.REMOTE)
    public List<Article> selectSimpleArticles() {
        log.info("数据库查询所有文章的简要信息");
        return articleMapper.selectSimpleArticles();
    }

    //@Cacheable(cacheNames = CacheConstant.BLOGINFO_CACHE_NAME, key = "#root.methodName")
    @Override
    @Cached(expire = 3600,name =CacheConstant.ALL_ARTICLE_SIMPLE_INFO_JSON,
            key = CacheConstant.YH+CacheConstant.ALL_ARTICLE_SIMPLE_INFO_JSON+CacheConstant.YH
            ,cacheType = CacheType.REMOTE)
    public JSONArray selectSimpleArticlesSelectJson() {
        JSONArray jsonArray = new JSONArray();
        List<Article> articles = this.selectSimpleArticles();
        for (int i = 0; i < articles.size(); i++) {
            JSONObject json = new JSONObject();
            json.put("name", articles.get(i).getTitle());
            json.put("value", articles.get(i).getTitle());
            jsonArray.add(json);
        }
        return jsonArray;
    }


    @Override
    public List<Article> selectArticleListSimple(Article article) {
        List<Article> articles = articleMapper.selectArticleListSimple(article);
        selectTags(articles);
        selectCategory(articles);
        return articles;
    }

    private void selectTags(Article a) {
        //tagIds
        String tids = a.getTags();
        if (StringUtils.isEmpty(tids)) {
            return;
        }
        String[] arr = Convert.toStrArray(tids);
        List<Tags> tags = Lists.newArrayList();
        //遍历所有标签列表
        for (String tid : arr) {
            if (StringUtils.isEmpty(tid)) {
                continue;
            }
            Tags tag = null;
            tag = (Tags) tagCache.get(tid);
            if (tag == null) {
                tag = tagsMapper.selectTagsById(Long.valueOf(tid));
            }
            tags.add(tag);
        }
        a.setTagList(tags);
    }


    @Override
    public List<Article> selectSelectedAtricleAll(String selectedIds) {


        //查询所有文章，
        List<Article> articles = this.selectSimpleArticles();

        //吧
        if (StringUtils.isNotEmpty(selectedIds)) {
            if (selectedIds.endsWith(",")) {
                selectedIds = selectedIds.substring(0, selectedIds.length() - 1);
            }
            String[] arr = Convert.toStrArray(selectedIds);
            List<String> list = Lists.newArrayList(arr);

            //遍历所有标签，
            updateArticle:
            for (int i = 0; i < list.size(); i++) {

                //找到标签并移除
                String s = list.get(i);

                articles:
                for (int j = 0; j < articles.size(); j++) {
                    Article article = articles.get(j);
                    if (s.equals(article.getId())) {
                        //设置为选中
//                    article.setSelected(true);
                        Article tempArticle = article;
                        articles.remove(j);
                        articles.add(tempArticle);
                        //放到最后
                        continue updateArticle;
                    }
                }

                Article article = articles.get(i);

                //设置为选中
//                    article.setSelected(true);
                Article tempArticle = article;
                articles.remove(article);
                articles.add(tempArticle);
                //放到最后
            }


//            for (int i = 0; i < articles.size(); i++) {
//                Article article = articles.get(i);
//                if (list.contains(article.getId().toString())) {
//                    //设置为选中
////                    article.setSelected(true);
//                    Article tempArticle = article;
//                    articles.remove(i);
//                    articles.add(tempArticle);
//                    //放到最后
//                }
//            }
//            articles.forEach(t -> {
//
//            });
        }
        return articles;
    }

    private List<Article> selectArticlesAll() {
        Article blackArticle = new Article();
        return articleMapper.selectArticleList(blackArticle);
    }

    @Override
    @Cached(expire = 3600, cacheType = CacheType.BOTH)
    public List<Article> getHotArticleByLimit(Integer limit) {
        List<Article> hotArticlesList = articleMapper.getHotArticleByLimit(limit);
        selectCategory(hotArticlesList);
        selectTags(hotArticlesList);
        log.info("从数据库中取getHotArticleByLimit");
        return hotArticlesList;
    }

    @Override
    //@Cacheable(value = "articleHits")
    public String getArticleHits() {

        String articleHits = articleMapper.getArticleHits();
        log.info("db.articleHits:" + articleHits);
        return articleHits;
    }

    @Override
    public String selectArticleResourceContentByArticleId(String articleId) {
        return articleMapper.selectArticleResourceContentByArticleId(articleId);
    }

    /**
     * 根据文章id获取密码
     *
     * @param articleId
     * @return
     */
    @Override
    public String selectArticlePasswordByArticleId(String articleId) {
        return articleMapper.selectArticlePasswordByArticleId(articleId);
    }

    @Override
    @Cached(expire = 3600, cacheType = CacheType.BOTH)
    public List<Article> selectrecentListByUpdatetime(Integer limit) {
        List<Article> articles = articleMapper.selectrecentListByUpdatetime(limit);
        selectCategory(articles);
        selectTags(articles);
        return articles;
    }


}

