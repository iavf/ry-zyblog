package com.ruoyi.framework.aspectj;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
import com.ruoyi.common.utils.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 多数据源处理
 *
 * @author ruoyi
 */
@Aspect
@Order(1)
@Component
public class DataSourceAspect
{
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("@annotation(com.ruoyi.common.annotation.DataSource)"
            + "|| @within(com.ruoyi.common.annotation.DataSource)")
    public void dsPointCut()
    {

    }

    @Around("dsPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable
    {
        DataSource dataSource = getDataSource(point);

        if (StringUtils.isNotNull(dataSource))
        {
            DynamicDataSourceContextHolder.setDataSourceType(dataSource.value().name());
        }

        try
        {
            return point.proceed();
        }
        finally
        {
            // 销毁数据源 在执行方法之后
            DynamicDataSourceContextHolder.clearDataSourceType();
        }
    }

    /**
     * 获取需要切换的数据源
     */
    public DataSource getDataSource(ProceedingJoinPoint point)
    {
        MethodSignature signature = (MethodSignature) point.getSignature();
        // point.getTarget().getClass(); 这个获取的是动态代理的class，上面获取不到自定义注解
        // 先获取方法上的，获取不到啊再从class上找
        DataSource dataSource = AnnotationUtils.findAnnotation(signature.getMethod(), DataSource.class);
        if (Objects.nonNull(dataSource))
        {
            return dataSource;
        }

        return AnnotationUtils.findAnnotation(signature.getDeclaringType(), DataSource.class);
    }
}
//package com.ruoyi.framework.aspectj;
//
//import com.ruoyi.common.config.datasource.DynamicDataSourceContextHolder;
//import com.ruoyi.common.enums.DataSourceType;
//import org.apache.commons.lang3.StringUtils;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//
///**
// * 多数据源处理
// */
//@Aspect
//@Order(1)
//@Component
//public class DataSourceAspect {
//    protected Logger logger = LoggerFactory.getLogger(getClass());
//
//    @Around("execution(* com.ruoyi..*ServiceImpl.*(..))")
//    public Object around(ProceedingJoinPoint point) throws Throwable {
//        // 获取到当前执行的方法名
//        String methodName = point.getSignature().getName();
//        if (isSlave(methodName)) {
//            // 标记为读库,可以自定义选择数据源
//            DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name());
//        } else {
//            // 标记为写库
//            DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.MASTER.name());
//        }
//        try {
//            return point.proceed();
//        } finally {
//            // 销毁数据源 在执行方法之后
//            DynamicDataSourceContextHolder.clearDataSourceType();
//        }
//    }
//
//    /**
//     * 判断是否为读库
//     *
//     * @param methodName
//     * @return
//     */
//    private boolean isSlave(String methodName) {
//        // 方法名以query、find、get开头的方法名走从库
//        return StringUtils.startsWithAny(methodName, new String[]{"query", "find", "get", "select"});
//    }
//}
