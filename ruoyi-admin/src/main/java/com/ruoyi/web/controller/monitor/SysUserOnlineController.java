package com.ruoyi.web.controller.monitor;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.enums.OnlineStatus;
import com.ruoyi.framework.shiro.session.OnlineSession;
import com.ruoyi.framework.shiro.session.OnlineSessionDAO;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUserOnline;
import com.ruoyi.system.service.ISysUserOnlineService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 在线用户监控
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController {
    private String prefix = "monitor/online";

    @Autowired
    private ISysUserOnlineService userOnlineService;

    @Autowired
    private OnlineSessionDAO onlineSessionDAO;

    @RequiresPermissions("monitor:online:view")
    @GetMapping()
    public String online() {
        return prefix + "/online";
    }

    @RequiresPermissions("monitor:online:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysUserOnline userOnline) {
        startPage();
        List<SysUserOnline> list = userOnlineService.selectUserOnlineList(userOnline);
        return getDataTable(list);
    }


    /**
     * 1、删除了 forceLogout 方法
     * 2、将 batchForceLogout 和 forceLogout 的权限逻辑 改成了 OR【按需要设定】
     * 3、@RequestParam("ids[]") ==> @RequestParam("ids")
     * 4、开源拥有者 可以斟酌一下
     *
     * @param ids
     * @return
     */
//    @RequiresPermissions("monitor:online:batchForceLogout")
    @RequiresPermissions(value = { "monitor:online:batchForceLogout", "monitor:online:forceLogout" }, logical = Logical.OR)
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @PostMapping("/batchForceLogout")
    @ResponseBody
//    public AjaxResult batchForceLogout(@RequestParam("ids[]") String[] ids)
//    public AjaxResult batchForceLogout(@RequestParam("ids") String[] ids) {
    public AjaxResult batchForceLogout(String ids){
//        for (String sessionId : ids) {
     for (String sessionId : Convert.toStrArray(ids)){
            SysUserOnline online = userOnlineService.selectOnlineById(sessionId);
            if (online == null) {
                return error("用户已下线");
            }
            OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getSessionId());
            if (onlineSession == null) {
                return error("用户已下线");
            }
            if (sessionId.equals(ShiroUtils.getSessionId())) {
                return error("当前登陆用户无法强退");
            }
            onlineSession.setStatus(OnlineStatus.off_line);
            onlineSessionDAO.update(onlineSession);
            online.setStatus(OnlineStatus.off_line);
            userOnlineService.saveOnline(online);
        }
        return success();
    }

//    @RequiresPermissions("monitor:online:forceLogout")
//    @Log(title = "在线用户", businessType = BusinessType.FORCE)
//    @PostMapping("/forceLogout")
//    @ResponseBody
//    public AjaxResult forceLogout(String sessionId) {
//        SysUserOnline online = userOnlineService.selectOnlineById(sessionId);
//        if (sessionId.equals(ShiroUtils.getSessionId())) {
//            return error("当前登陆用户无法强退");
//        }
//        if (online == null) {
//            return error("用户已下线");
//        }
//        OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getSessionId());
//        if (onlineSession == null) {
//            return error("用户已下线");
//        }
//        onlineSession.setStatus(OnlineStatus.off_line);
//        onlineSessionDAO.update(onlineSession);
//        online.setStatus(OnlineStatus.off_line);
//        userOnlineService.saveOnline(online);
//        return success();
//    }
}
