//package com.ruoyi.common.cache.caffeine;
//
//import com.github.benmanes.caffeine.cache.Caffeine;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.cache.CacheManager;
//import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.cache.caffeine.CaffeineCache;
//import org.springframework.cache.support.SimpleCacheManager;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//
//import java.util.ArrayList;
//import java.util.concurrent.TimeUnit;
//@Configuration
//@EnableCaching
//public class CacheConfig {
//    @Value("spring.cache.cache-names.bloginfo")
//    public static  String DEFAULT_MAXSIZE1 ;
//    public static final int DEFAULT_MAXSIZE = 50000;
//    //1h=60 1m=60s
//    public static final int DEFAULT_TTL = 60;
//
//    /**
//     * 定義cache名稱、超時時長（秒）、最大容量
//     * 每个cache缺省：10秒超时、最多缓存50000条数据，需要修改可以在                构造方法的参数中指定。
//     */
//    public enum Caches{
//        //默认缓存，60s消失，
//        def(60),
//        //文章缓存1h消失，最大100条
//        article(60*60,100),
//        //站点信息，12h，
//        blogInfo(60*60),
//        category(60*60*12),
//        tag(60*60*12),
//        //财务信息
//        money(60*60*12),
//        config(60*60*12),
//        ;
//
//        Caches() {
//        }
//
//        Caches(int ttl) {
//            this.ttl = ttl;
//        }
//
//        Caches(int ttl, int maxSize) {
//            this.ttl = ttl;
//            this.maxSize = maxSize;
//        }
//
//        private int maxSize=DEFAULT_MAXSIZE;    //最大數量
//        private int ttl=DEFAULT_TTL;        //过期时间（秒）
//
//        public int getMaxSize() {
//            return maxSize;
//        }
//        public int getTtl() {
//            return ttl;
//        }
//    }
//
//    /**
//     * 创建基于Caffeine的Cache Manager
//     * @return
//     */
//    @Bean("caffeineCacheManager")
//    @Primary
//    public CacheManager caffeineCacheManager() {
//        SimpleCacheManager cacheManager = new SimpleCacheManager();
//
//        ArrayList<CaffeineCache> caches = new ArrayList<CaffeineCache>();
//        for(Caches c : Caches.values()){
//            caches.add(new CaffeineCache(c.name(),
//                    Caffeine.newBuilder().recordStats()
//                            .expireAfterWrite(c.getTtl(), TimeUnit.SECONDS)
//                            //根据缓存的权重来进行驱逐（权重只是用于确定缓存大小，不会用于决定该缓存是否被驱逐）
//                            //.maximumWeight(c.getMaxSize())
//                            //.weigher((Key key, Graph graph) -> graph.vertices().size())
////                            .weigher()
//                            .maximumSize(c.getMaxSize())
//                            .softValues()
//                            .build())
//            );
//        }
//
//        cacheManager.setCaches(caches);
//        return cacheManager;
//    }
//
//
//
////    @Bean
////    public CacheLoader<Object, Object> cacheLoader() {
////
////        CacheLoader<Object, Object> cacheLoader = new CacheLoader<Object, Object>() {
////
////            @Override
////            public Object load(Object key) throws Exception {
////                return null;
////            }
////
////            // 重写这个方法将oldValue值返回回去，进而刷新缓存
////            @Override
////            public Object reload(Object key, Object oldValue) throws Exception {
////                return oldValue;
////            }
////        };
////        return cacheLoader;
////    }
//
//
//}