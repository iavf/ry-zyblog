var baseUrl = "https://mk.yonyou.com/websitecenter/";
var websiteId = "5e8163d07b07250001357687";

var p = 0, t = 0;

$(window).scroll(function () {
  if ($(window).scrollTop() > 58) {
    $('.barLi-three').show();
    p = $(this).scrollTop();
    if (t <= p) {
      $('#navjs').removeClass('up').addClass('down');
    } else {
      $('#navjs').removeClass('down').addClass('up');
    }
    setTimeout(function () {
      t = p;
    }, 0);
  } else {
    $('.barLi-three').hide();
    $('.fixed_r_nav .li_4').fadeOut();
  }
});

function bubbleSort(arr) {
  if (Array.isArray(arr)) {
    for (var i = arr.length - 1; i > 0; i--) {
      for (var j = 0; j < i; j++) {
        if (arr[j].sort > arr[j + 1].sort) {
          [arr[j], arr[j + 1]] = [arr[j + 1], arr[j]];
        }
      }
    }
    return arr;
  }
}

var index = {
  headers: function () {
    // 顶部菜单栏鼠标移入移出事件
    var qcloud = {};
    $('.menulist-left-ul').each(function () {
      $(this).find('li').eq(0).addClass('active');
    });
    $('[dataNav]').hover(function () {
      var _nav = $(this).attr('dataNav');
      clearTimeout(qcloud[_nav + '_timer']);
      qcloud[_nav + '_timer'] = setTimeout(function () {
        $('[dataNav]').each(function () {
          $(this)[_nav == $(this).attr('dataNav') ? 'addClass' : 'removeClass']('nav-up-selected');
        });
        $('.menulist').find('#' + _nav).stop(true, true).slideDown(300);
        if (_nav == 'product') {
          $('.menulist').find('#' + _nav).find('ul.main').addClass('main-right-ul');
        }
        // 鼠标滑入滑出二级菜单
        $('.menulist-left-ul').off();
        $('.menulist-left-ul').hover(function () {
          $(this).find('li').each(function (ind) {
            $(this).attr('hover_index', ind);
            $('.menulist-left-ul > li').off();
            $('.menulist-left-ul > li').hover(function () {
              $(this).addClass('active').siblings().removeClass('active');
              var _index = $(this).attr('hover_index');
              $('.main-right-ul > li').eq(_index).show().siblings().hide();
            }, function () {
            });
          });
        }, function () {
        });
      }, 100);
    }, function () {
      var _nav = $(this).attr('dataNav');
      clearTimeout(qcloud[_nav + '_timer']);
      qcloud[_nav + '_timer'] = setTimeout(function () {
        // if()
        $('[dataNav]').removeClass('nav-up-selected');
        $('.menulist').find('#' + _nav).stop(true, true).slideUp(300);
      }, 100);
    });
    // 顶部header容器移入移出事件
    $('.yongyou-header').hover(function () {
    }, function () {
      $(this).removeClass('yongyou-header-hover');
    });
  },
  headerSmall: function () {
    var navbarToggle = $('.navbar-toggle');
    var smallLi = $('.small-menu').find('li');
    var mainLi = $('.mainOneli');
    var mainLis = $('.samall-menulists').find('.mainOnelis');
    var samallMenulist = $('.samall-menulist').find('.list');
    var mainA = samallMenulist.find('ul li a');
    $('.faright').append('<i class="fa fa-angle-right"></i>');
    navbarToggle.click(function () {
      var scrollTop = window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop ||
        0;
      sss = scrollTop;
      document.body.style.top = -1 * scrollTop + 'px';
      document.body.style.position = 'fixed';
      /**
       *  侧边栏宽度改为589px,因此将 $('.form-div').animate({right:-500},300) 改为
       *                           $('.form-div').animate({right:-600},300)
       */
      if (!$('.opens').is('.active')) {
        $('.form-div').animate({
          right: -600
        }, 300);
        $('.opens').addClass('active');
      };
      if (!$('.small-menu').is(':hidden')) {
        document.body.style.overflow = '';
        document.body.style.position = null;
        document.body.style.top = null;
        window.scrollTo(0, sss);
      }
      if (!$(this).is('.active')) {
        $('.mask').show();
        $('.div-wrapper').addClass('divWrap');
        $('header').addClass('divWrap');
        $('.small-menu').show(100);
        $(this).addClass('active');
        $('.bg-div').show();
      } else {
        $('.mask').hide();
        $('.div-wrapper').removeClass('divWrap');
        $('header').removeClass('divWrap');
        $('.small-menu').hide(300);
        samallMenulist.animate({
          left: '-300px'
        }, 250);
        $('.samall-menulists').animate({
          left: '-300px'
        }, 250);
        $(this).removeClass('active');
        $('.bg-div').hide();
      }
    });
    $('.mask').click(function () {
      $(this).hide();
      $('.small-menu').hide();
      navbarToggle.removeClass('active');
      $('.yongyou-header').removeClass('divWrap');
      $('.div-wrapper').removeClass('divWrap');
      $('.small-menu').hide(300);
      samallMenulist.animate({
        left: '-300px'
      }, 250);
      $('.samall-menulists').animate({
        left: '-300px'
      }, 250);
      document.body.style.overflow = '';
      document.body.style.position = null;
      document.body.style.top = null;
      window.scrollTo(0, sss);
    });
    smallLi.click(function () {
      var index = $(this).index();
      samallMenulist.eq(index).animate({
        left: '0'
      });
    });
    mainLi.click(function () {
      $(this).parents('.list').removeClass('.nav-up-selected');
      $(this).parents('.list').animate({
        left: '-300px'
      }, 250);
    });
    mainA.click(function () {
      $(this).siblings().animate({
        left: '0'
      });
    });
    mainLis.on('click', function () {
      ($(this).parent()).parent('.samall-menulists').animate({
        left: '-300px'
      }, 250);
    });
  },
  oneLunbo: function () {
    var liSlider = $('.slider1 li');
    liSlider.hover(function () {
      $(this).addClass('active');
    }, function () {
      $(this).removeClass('active');
    });
  },
  top: function () {
    var leftWidth = $('.leftmenu').width();
    
    $('.top').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 1000);
      return false;
    });
    
    $('.form-b').on('click', function () {
      $('.open').addClass('active');
      $('.form-sub').animate({
        right: -600
      }, 300);
      $('.form-div').animate({
        right: -600
      }, 300);
    });
    
    $('.open').on('click', function () {
      if ($(this).hasClass('active')) {
        $('.form-div').animate({
          right: 60
        }, 300);
        $(this).removeClass('active');
      } else {
        $(this).addClass('active');
        $('.form-div').animate({
          right: -600
        }, 300);
        $('.form-sub').animate({
          right: -600
        }, 300);
      }
    });
    $('body').on('click', function (e) {
      var e = e || window.event; //浏览器兼容性
      var elem = e.target || e.srcElement; // 事件源对象
    });
    // END 侧栏展开收回事件修改 by 20190513
    $('.formBottom-right').click(function () {
      $('.form-div').animate({
        right: -600
      }, 300);
      $('.form-sub').animate({
        right: leftWidth
      }, 300);
      $('.open').addClass('active');
    });
    $('.form-sub').mouseenter(function () {
      $('.open').removeClass('active');
    });
  },
  tops: function () {
    var opens = $('.opens');
    opens.on('click', function () {
      if ($(this).hasClass('active')) {
        $('.opens').addClass('active');
        $('.form-div').animate({
          right: 0
        }, 300);
        $(this).removeClass('active');
      } else if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $('.form-div').animate({
          right: -600
        }, 300);
      }
      
    });
    
  },
  scroll: function () {
    var scRoll = $(document).scrollTop();
    var header_yongyou = $('.yongyou-header').outerHeight();
    $(window).scroll(function (e) {
      var header_yy = $(document).scrollTop();
      if (header_yy == 0) {
        $('.yongyou-header').removeClass('bg');
      } else {
        
        $('.yongyou-header').addClass('bg');
      }
      if (header_yy > header_yongyou) { //下滚
        $('.yongyou-header').removeClass('active');
      } else { //上滚
        $('.yongyou-header').addClass('active');
      }
      if (header_yy > scRoll) {
        $('.yongyou-header').addClass('active');
      } else {
        $('.yongyou-header').removeClass('active');
      }
      scRoll = $(document).scrollTop();
    });
  },
  cloud: function () {
    var cloudFive = $('.sections');
    cloudFive.find('.item').hover(function () {
      $(this).addClass('active');
    }, function () {
      $(this).removeClass('active');
    });
  },
  sectionBtn: function () {
    var sectionBtn = $('.sectionFive-btn');
    sectionBtn.click(function () {
      $('.section-five').addClass('active');
    });
    $('.section-five').mouseleave(function () {
      $(this).removeClass('active');
    });
  },
  videoBtn: function () {
    var videoDivDom = $('.video-box'); // 视频容器
    var videoContDom = $('.video-cont-box'); // 视频核心容器
    var videoBtnDom = $('.video-play-btn'); // 视频播放按钮
    var videos = '';
    videoBtnDom.click(function () {
      videos = videoContDom.find('video')[0];
      videoDivDom.show();
      videoContDom.show();
      videoContDom.find('.gb').show();
      videos.play();
    });
    
    $('.gb').click(function () {
      videoDivDom.hide();
      videoContDom.hide();
      videos.pause();
    });
  },
  widths: function () {
    $(window).resize(function () {
      var Height = $(window).height();
      var Width = $(window).width();
      if (Width >= 755) {
        $('.div-wrapper').removeClass('divWrap');
        $('.yongyou-header').removeClass('divWrap');
        $('.mask').hide();
        $('body').css({
          'position': '',
          'top': '0',
          'left': '0'
        });
      }
      if (Width <= 1200) {
        $('.section-seven').hide();
      } else {
        $('.section-seven').show();
      }
    });
  },
  businessShow: function () {
    var indexBannerTwoDom = $('.index-banner-two');
    var indexBusinessShowDom = $('.business-show');
    indexBannerTwoDom.on('click', '.swiper-slide', function () {
      var curType = $(this).attr('data-type');
      if (curType != 99) {
        indexBannerTwoDom.find('.swiper-slide').removeClass('business-nav-active');
        $(this).addClass('business-nav-active');
      }
      if (curType == 1) {
        indexBusinessShowDom.attr('class', 'business-show business-show-yingxiao');
      } else if (curType == 2) {
        indexBusinessShowDom.attr('class', 'business-show business-show-zhizao');
      } else if (curType == 3) {
        indexBusinessShowDom.attr('class', 'business-show business-show-caigou');
      } else if (curType == 4) {
        indexBusinessShowDom.attr('class', 'business-show business-show-jinrong');
      } else if (curType == 5) {
        indexBusinessShowDom.attr('class', 'business-show business-show-caiwu');
      } else if (curType == 6) {
        indexBusinessShowDom.attr('class', 'business-show business-show-renli');
      } else if (curType == 7) {
        indexBusinessShowDom.attr('class', 'business-show business-show-xietong');
      } else if (curType == 8) {
        indexBusinessShowDom.attr('class', 'business-show business-show-yunpingtai');
      } else if (curType == 99) {
        window.open('http://www.yonyoucloud.com');
      } else {
        indexBusinessShowDom.attr('class', 'business-show');
      }
      // 渲染文字
      index.businessShowTxtFun(curType);
    });
  },
  businessShowTxtFun: function (ctype) {
    ctype = ctype || 1;
    var curTxtObj = indexBusinessTxtArr[ctype - 1];
    var curTxtDom = '';
    if (Object.keys(curTxtObj)) {
      // 判断是否有合作公司
      var conpanyImgDom = '',
        conpanyImgListDom = '';
      if (curTxtObj.imgs.length) {
        for (var imgi = 0, len = curTxtObj.imgs.length; imgi < len; imgi++) {
          conpanyImgListDom += '<li><img src="' + curTxtObj.imgs[imgi] + '" /></li>';
        }
        conpanyImgDom = '<div class="business-show-txt-company">' +
          '<p>' + (ctype == 1 ? '最佳实践' : '合作公司') + '</p>' +
          '<ul>' + conpanyImgListDom + '</ul>' +
          '</div>';
      }
      curTxtDom = '<div class="business-show-txt-model"><div class="business-show-txt">' +
        '<h3>' + curTxtObj.title + '</h3>' +
        '<p class="business-show-txt-h4">' + curTxtObj.btxt + '</p>' +
        '<p class="business-show-txt-p">' + curTxtObj.txt + '</p>' +
        conpanyImgDom +
        '</div></div>';
    }
    $('.business-show').html(curTxtDom);
  },
  linkTo: function () {
    var linkSlideBoxDom = $('.index-banner-three');
    linkSlideBoxDom.on('click', '.swiper-slide', function () {
      console.log($(this).attr('data-type'));
      var curType = $(this).attr('data-type');
      if (curType == 1) {
        window.open('https://www.yonyou.com/yy/newsCenterPage.html');
      } else if (curType == 2) {
        window.open('https://www.yonyou.com/yy/newsCenterPage.html?id=1');
      } else if (curType == 3) {
        window.open('https://www.yonyou.com/yy/newsCenterPage.html?id=2');
      } else if (curType == 4) {
        window.open('https://www.yonyoucloud.com/cloudClient.html');
      } else {
        window.open('https://www.yonyou.com/');
      }
    });
    // 鼠标移入移出事件
    linkSlideBoxDom.find('.swiper-slide').hover(function () {
      linkSlideBoxDom.find('.swiper-slide').removeClass('swiper-slide-three-hover');
      $(this).addClass('swiper-slide-three-hover');
    }, function () {
      $(this).removeClass('swiper-slide-three-hover');
    });
  },
  banner: function () {
    $.ajax({
      "type": "POST",
      "url": baseUrl + "picture/page",
      "contentType": "application/json;charset=UTF-8",
      "dataType": "json",
      "data": JSON.stringify({
        "key": websiteId,
        "pagenumber": 1,
        "pagesize": 51
      }),
      "success": function(res) {
        if(res.flag==0 && res.data.totalElements > 0){
          var bannerList = bubbleSort(res.data.content);
          var bannerHtml = "";
          bannerList.forEach(function(item) {
            var bannerItem = '<div class="swiper-slide">';
            var link = item.link.indexOf('http') >  -1 ? item.link : 'http://' + item.link 
            bannerItem += '<a class="index-banner-one-slide-pc" target="' + item.target + '" href="'+ link +'"><img src="'+ item.pc_path +'" alt="'+item.title+'"></a>';
            bannerItem += '<a class="index-banner-one-slide-web" target="' + item.target + '" href="'+ link +'"><img src="'+ item.mobile_path +'" alt="'+item.title+'"></a>';
            bannerItem += '</div>';
            bannerHtml += bannerItem;
          })
          $('#index_banner').html(bannerHtml);
          // 板块一：banner或视频播放
          if (bannerList.length > 0) {
            if(window.navigator.userAgent.indexOf('compatible')!=-1){
              // var mySwiper = new Swiper('.index-banner-one', {
              //   autoplay: 2000,
              //   pagination: '.index-banner-one-pagination',
              //   loop: true,
              //   grabCursor: true,
              //   paginationClickable: true
              // });
            } else {
              // var mySwiperOne = new Swiper ('.index-banner-one', {
              //   initialSlide: 0,
              //   direction: 'horizontal', // 垂直切换选项
              //   autoplay: true, // 是否自动播放
              //   loop: true, // 循环模式选项
              //   // 分页器
              //   pagination: {
              //     el: '.index-banner-one-pagination',
              //     clickable: true
              //   }
              //   })
            }
          }
        }
      }
    })
  },
  newTwo: function () {
    $.ajax({
      "type": "GET",
      "url": "https://mk.yonyou.com/content-service/cnt_article_net/page?status=1&categories=6e92937a367544648fcfd2fa47a80e39&page=1&size=1",
      "success": function(res) {
        if(res.flag==0 && res.data.totalElements > 0){
          var newTwoHtml = '';
          res.data.content.forEach(function(item, index) {
            newTwoHtml += '<div class="news">';
            newTwoHtml += '<img src="' + item.coverImg + '" alt="">';
            newTwoHtml += '</div>';
            newTwoHtml += '<dl>';
            newTwoHtml += '<span class="news_line"></span>';
            newTwoHtml += '<dt>通知</dt>';
            newTwoHtml += '<dd>';
            newTwoHtml += item.title;
            newTwoHtml += '</dd>';
            newTwoHtml += '<div class="arrows_right">';
            newTwoHtml += '<span>';
            newTwoHtml += '<a href="/news/news_detail.html?id=' + item.id + '"><img src="./static_new/img/icon/右箭头.png" alt=""></a>';
            newTwoHtml += '</span>';
            newTwoHtml += '</div>';
            newTwoHtml += '</dl>';
          })
          $('#index_new_two').html(newTwoHtml);
          $('#index_new_two').attr('onclick','window.location.href = "/news/news_detail.html?id=' + res.data.content[0].id + '";');
        }
      }
    })
  }
};
var pdfUrl = "http://subject.yonyou.com/yy/用友数字化平台白皮书印刷文件.pdf";
$(function () {
  index.banner();
  index.headers();
  index.headerSmall();
  index.oneLunbo();
  index.top();
  index.tops();
  index.sectionBtn();
  index.videoBtn();
  index.widths();
  index.businessShow(); // 业务介绍
  index.linkTo(); // 更多链接
  index.newTwo();
});
