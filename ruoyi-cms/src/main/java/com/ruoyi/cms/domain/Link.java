package com.ruoyi.cms.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 链接对象 cms_link
 *
 * @author wujiyue
 * @date 2019-11-26
 */
@Data
public class Link extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 链接ID
     */
    private Long linkId;

    /**
     * 链接分类
     */
    @Excel(name = "链接分类")
    private String linkType;

    /**
     * 链接名称
     */
    @Excel(name = "链接名称")
    private String linkName;

    /**
     * 关键词
     */
    @Excel(name = "关键词")
    private String keywords;

    /**
     * 链接
     */
    @Excel(name = "链接")
    private String link;

    /**
     * 描述
     */
    private String description;

    /**
     * Logo
     */
    @Excel(name = "Logo")
    private String logo;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private Integer auditState;

    /**
     * 详情
     */
    private String detail;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Long sort;

    /**
     * 点赞数
     */
    @Excel(name = "点赞数")
    private Long upVote;

    /**
     * 允许评论
     */
    @Excel(name = "允许评论")
    private Integer commentFlag;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer status;

}
