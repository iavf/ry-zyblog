package com.ruoyi.blog.controller;


import cn.hutool.core.date.DateUtil;
import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import com.redfin.sitemapgenerator.WebSitemapUrl;
import com.ruoyi.cms.domain.Article;
import com.ruoyi.cms.service.IArticleService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 广告位Controller
 *
 * @author wujiyue
 * @date 2019-11-16
 */
@Controller
@RequestMapping("/")
public class IndexController extends BaseController {

    @Resource
    private ResourceLoader resourceLoader;
    @Autowired
    private ISysConfigService configService;


    @Autowired
    private IArticleService articleService;

    @RequestMapping("/article/{id}")
    public ModelAndView testPathVariable(@PathVariable(value="id") Integer id){
        return  new ModelAndView("redirect:/blog/article/"+id);
    }

    public  String createSiteMapXmlContent() {
        String siteUrl = configService.selectConfigByKey("site.url");
        String baseUrl = String.format(siteUrl);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        WebSitemapGenerator wsg = null;
        try {
            wsg = new WebSitemapGenerator(baseUrl);
            // 首页 url
            WebSitemapUrl url = new WebSitemapUrl.Options(baseUrl + "/")
                    .lastMod(dateTimeFormatter.format(LocalDateTime.now())).priority(1.0).changeFreq(ChangeFreq.DAILY).build();
            wsg.addUrl(url);

            // 查询所有的问题方案数据
            Article articleNull = new Article();
            List<Article> articles = articleService.selectSimpleArticles();

            // 动态添加文章url
            for (Article article : articles) {
                WebSitemapUrl tmpUrl = new WebSitemapUrl
                        .Options(baseUrl + "/blog/article/" + article.getId())
                        .lastMod(DateUtil.format(article.getUpdateTime(), "yyyy-MM-dd"))
                        .priority(0.7)
                        .changeFreq(ChangeFreq.DAILY)
                        .build();
                wsg.addUrl(tmpUrl);
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return String.join("", wsg.writeAsStrings());
    }




    @GetMapping("/sitemap.xml")
    public void createSiteMapXml(HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.APPLICATION_XML_VALUE);
        Writer writer = response.getWriter();
        writer.append(createSiteMapXmlContent());
    }

    @RequestMapping(value = "/robots.txt")
    public void robots(HttpServletRequest request, HttpServletResponse response) {
        try {
            String siteUrl = configService.selectConfigByKey("site.url");
            Writer writer = response.getWriter();
            String lineSeparator = System.getProperty("line.separator", "\n");
            writer.append("User-agent: *").append(lineSeparator);
            writer.append("Allow:").append("/").append(lineSeparator);
            writer.append("Disallow:").append("/admin/*").append(lineSeparator);
            writer.append("Sitemap:").append(siteUrl+"/sitemap.xml").append(lineSeparator);
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }
}
