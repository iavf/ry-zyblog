package com.ruoyi.cms.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 链接分类对象 cms_link_type
 *
 * @author wujiyue
 * @date 2019-11-26
 */
@Data
public class LinkType extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 链接分类
     */
    @Excel(name = "链接分类")
    private String linkType;

    /**
     * 链接分类名称
     */
    @Excel(name = "链接分类名称")
    private String linkTypeName;
    /**
     * 链接分类描述
     */
    @Excel(name = "链接分类描述")
    private String description;
    /**
     * 封面图片
     */
    @Excel(name = "封面图片")
    private String coverImage;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sort;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer status;

    /**
     * 父类型
     */
    @Excel(name = "父类型")
    private String parentType;


    List<LinkType> chileLinkTypeList ;

    private List<Link> childrenLinkList;//扩展字段


}
