package com.ruoyi.blog.controller;


import cn.hutool.cache.CacheUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.github.pagehelper.PageInfo;
import com.ruoyi.cms.domain.*;
import com.ruoyi.cms.service.*;
import com.ruoyi.cms.util.CmsConstants;
import com.ruoyi.common.cache.bean.CacheConstant;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.system.service.ISysConfigService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 广告位Controller
 *
 * @author wujiyue
 * @date 2019-11-16
 */
@Controller
@RequestMapping("/blog")
public class BlogController extends BaseController {
    private static final String PREFIX = "blog/theme";
//    @Autowired
//    RedisUtil redisUtil;

    //项目启动的时候，自动创建博客相关缓存。
//    @PostConstruct
//    public void init() {
//        System.out.println("=============blog cache  INIT  Start =====================");
//    }

    @CreateCache(expire = 100, name = CacheConstant.ARTICLE_CACHE_NAME,cacheType = CacheType.REMOTE)
    private Cache<String, Object> articleCache;

    @Autowired
    private IArticleService articleService;
    @Autowired
    private IAlbumService albumService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private ITagsService tagsService;
    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private IResourceService resourceService;
    @Autowired
    private ISysConfigService configService;
    @Autowired
    private ILinkTypeService linkTypeService;
    @Autowired
    private ILinkService linkService;
    @Autowired
    private IBlogThemeService blogThemeService;

    @CreateCache(expire = 3600, name = "articleViewCache",cacheType = CacheType.REMOTE)
    private Cache<String, Integer> articleViewCache;

    @CreateCache(expire = 3600, name = "articleUpVoteCache",cacheType = CacheType.REMOTE)
    private Cache<String, Integer> articleUpVoteCache;

    @CreateCache(expire = 3600, name = "commentUpVoteCache",cacheType = CacheType.REMOTE)
    private Cache<String, Integer> commentUpVoteCache;

//    private static cn.hutool.cache.Cache<String, Integer> articleViewCache = CacheUtil.newLRUCache(1000, 1000 * 60 * 60);
//    private static cn.hutool.cache.Cache<String, Integer> articleUpVoteCache = CacheUtil.newLRUCache(1000, 1000 * 60 * 60);
//    private static cn.hutool.cache.Cache<String, Integer> commentUpVoteCache = CacheUtil.newLRUCache(1000, 1000 * 60 * 60);
    private static cn.hutool.cache.Cache<String, Map> bannerCache = CacheUtil.newTimedCache(1000 * 60 * 60);
    private static cn.hutool.cache.Cache<String, Object> blogCache = CacheUtil.newTimedCache(1000 * 60 * 60 * 3);

    @RequiresPermissions("cms:article:edit")
    @GetMapping("/clearBlogCache")
    @ResponseBody
    public AjaxResult clearBlogCache() {
        articleUpVoteCache.close();
        blogCache.clear();
        bannerCache.clear();
//        CaffeineUtils.clear();
//        redisUtil.deleteByPrex(CacheConstant.TAG_CACHE_KEY);
        return AjaxResult.success("清除缓存成功");
    }


    //    @Cacheable(cacheNames = CacheConstant.BLOGINFO_CACHE_NAME,key = "#root.methodName" )
    public String getTheme() {
        return configService.selectConfigByKey(CmsConstants.KEY_BLOG_THEME);
    }

    /**
     * 首页
     *
     * @param model
     * @return
     */
    @GetMapping({"/", "", "/index"})
    public String index(Model model, @RequestParam(value = "p", required = false) String p, HttpServletRequest request) {

        model.addAttribute("pageUrl", "blog/index");
        //       http://localhost:88/?p=1234

        String p1 = request.getParameter("p");
        if (StringUtils.isNotBlank(p1)) {
            redirect("/");
        }
//        Article form = new Article();
//        startPage();
        //热门文章
//        List<Article> articles = articleService.getHotArticle();
//        //最近的文章
////        List<Article> articles = articleService.selectArticlesRegionNotNull(form);
//        model.addAttribute("total", new PageInfo(articles).getTotal());
//        model.addAttribute("hasPrevious", new PageInfo(articles).isHasPreviousPage());
//        model.addAttribute("hasNext", new PageInfo(articles).isHasNextPage());
//        model.addAttribute("currentPage", new PageInfo(articles).getPageNum());
//        model.addAttribute("prePage", new PageInfo(articles).getPrePage());
//        model.addAttribute("nextPage", new PageInfo(articles).getNextPage());
//        model.addAttribute("navNums", new PageInfo(articles).getNavigatepageNums());
//        model.addAttribute("pageNo", new PageInfo(articles).getPageNum());
//        model.addAttribute("pageSize", new PageInfo(articles).getPageSize());
//        model.addAttribute("totalPages", new PageInfo(articles).getPages());
//        model.addAttribute("articleList", articles);
//        model.addAttribute("pageType", "index");
        return PREFIX + "/" + getTheme() + "/index";
    }


//    /**
//     * 热门文章
//     */
////    @GetMapping("/hot")
//    public String getHotArticle(Model model, HttpServletRequest request) {
//        startPage();
//        List<Article> hotArticle = articleService.getHotArticle();
//        model.addAttribute("total", new PageInfo(hotArticle).getTotal());
//        model.addAttribute("hasPrevious", new PageInfo(hotArticle).isHasPreviousPage());
//        model.addAttribute("hasNext", new PageInfo(hotArticle).isHasNextPage());
//        model.addAttribute("currentPage", new PageInfo(hotArticle).getPageNum());
//        model.addAttribute("prePage", new PageInfo(hotArticle).getPrePage());
//        model.addAttribute("nextPage", new PageInfo(hotArticle).getNextPage());
//        model.addAttribute("navNums", new PageInfo(hotArticle).getNavigatepageNums());
//        model.addAttribute("pageNo", new PageInfo(hotArticle).getPageNum());
//        model.addAttribute("pageSize", new PageInfo(hotArticle).getPageSize());
//        model.addAttribute("totalPages", new PageInfo(hotArticle).getPages());
//        model.addAttribute("articleList", hotArticle);
//        return PREFIX + "/" + getTheme() + "/hot";
//    }


    /**
     * 群组
     *
     * @param model
     * @return
     */
    @GetMapping({"/group"})
    public String group(Model model) {
//        return PREFIX + "/" + getTheme() + "/group";
        return "forward:/blog/article/464";
    }


    /**
     * 赞赏
     *
     * @param model
     * @return
     */
    @GetMapping({"/donate"})
    public String donate(Model model) {
        return PREFIX + "/" + getTheme() + "/donate";
    }


    /**
     * 获取一个专辑以及其关联的素材
     */
    @PostMapping("/getIndexBanner")
    @ResponseBody
    public AjaxResult getAlbum(String code) {
        if (StringUtils.isEmpty(code)) {
            return AjaxResult.error("参数code不能为空!");
        }
        Map data = bannerCache.get(code, false);
        if (data == null) {
            data = albumService.getAlbum(code);
            bannerCache.put(code, data);
        }
        return AjaxResult.success(data);
    }

    /**
     * 获取一个专辑以及其关联的素材
     */
    @PostMapping("/getAllTitle")
    @ResponseBody
    public JSONArray getAllTitle() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = articleService.selectSimpleArticlesSelectJson();
        return jsonArray;
    }


    /**
     * 文章详情
     *
     * @param model
     * @param articleId
     * @return
     */
    @GetMapping("/article/{articleId}")
    public String article(HttpServletRequest request, Model model, @PathVariable("articleId") String articleId) {

        Article article =  articleService.selectArticleById(articleId);
        if (article == null) {
            throw new BusinessException("该文章不存在!");
        }
        //访问量
        articleView(request, articleId);
        if (article.getArticlePassword() != "") {
            articlePassword(request, articleId);
        }
        Comment comment = new Comment();
        comment.setTid(articleId);
        //判断密码
//        model.addAttribute("hotArticleListByTagId",sortAtricleListByHit(hotArticleListByTagIds) );
//        model.addAttribute("hotArticleListByCategoryId", hotArticleListByCategoryId);

//        String s= (String) request.getSession().getAttribute("rememberPassword");

        model.addAttribute("article", article);
        model.addAttribute("categoryId", article.getCategoryId());
//        model.addAttribute("commentList", commentList);
        model.addAttribute("pageType", "article");
        return PREFIX + "/" + getTheme() + "/article";
    }


    public boolean articlePassword(HttpServletRequest request, String articleId) {
        ArrayList rememberPasswordList = new ArrayList();
        rememberPasswordList = (ArrayList) request.getSession().getAttribute("rememberPasswordList");
        if (CollectionUtils.isEmpty(rememberPasswordList)) {
        } else {
            boolean contains = rememberPasswordList.contains(articleId);
            if (contains) {
                return true;
            } else {
            }
        }
        return false;
    }

    /**
     * 分类列表
     *
     * @param model
     * @return
     */
    @GetMapping("/category")
    public String category(Model model) {
        model.addAttribute("categoryId", "category");
        Article form = new Article();
        startPage();
        List<Article> articles = articleService.selectArticleList(form);
        PageInfo pageInfo = new PageInfo(articles);
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());
        model.addAttribute("articleList", articles);
        model.addAttribute("pageType", "category");

        return PREFIX + "/" + getTheme() + "/category_article";
    }

    /**
     * 分类列表
     *
     * @param categoryId
     * @param model
     * @return
     */
    @GetMapping("/category/{categoryId}")
    public String categoryBy(@PathVariable("categoryId") String categoryId, Model model) {
        Category category = categoryService.selectCategoryById(Long.valueOf(categoryId));
        if (category != null) {
            model.addAttribute("categoryName", category.getCategoryName());
        }
        Article form = new Article();
        form.setCategoryId(categoryId);
        model.addAttribute("categoryId", categoryId);
        startPage();
        List<Article> articles = articleService.selectArticleList(form);
        PageInfo pageInfo = new PageInfo(articles);
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());
        model.addAttribute("articleList", articles);
        return PREFIX + "/" + getTheme() + "/category";
    }

    /**
     * 分类列表
     *
     * @param model
     * @return
     */
    @GetMapping("/resource/list")
    public String resourceList(Model model) {
        model.addAttribute("categoryId", "resource");
        Resource form = new Resource();
        form.setStatus(CmsConstants.STATUS_NORMAL);
        form.setAuditState(CmsConstants.AUDIT_STATE_AGREE.toString());
        startPage();
        List<Resource> resources = resourceService.selectResourceList(form);
        PageInfo pageInfo = new PageInfo(resources);
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());
        model.addAttribute("resourceList", resources);
        return PREFIX + "/" + getTheme() + "/list_resource";
    }

    /**
     * 资源详情
     *
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/resource/{id}")
    public String resource(HttpServletRequest request, Model model, @PathVariable("id") String id) {
        Resource resource = resourceService.selectResourceById(id);
        if (resource == null) {
            throw new BusinessException("该资源不存在!");
        }
        model.addAttribute("resource", resource);
        model.addAttribute("categoryId", "resource");
        return PREFIX + "/" + getTheme() + "/resource";
    }

    /**
     * 搜索内容
     * 目前仅支持文章标题模糊搜索
     *
     * @param content
     * @param model
     * @return
     */

    @GetMapping("/search")
    public String search(String tit, String con, String orn, String ort, String cat, String tag, Model model) {
        startPage();
        Article article = new Article();
        if (StringUtils.isNotEmpty(tit)) {
            article.setTitle(tit.trim());
        } else {
            tit = "";
        }
        if (StringUtils.isNotEmpty(cat)) {
            article.setCategoryId(cat.trim());
        } else {
            cat = "";
        }
        if (StringUtils.isNotEmpty(tag)) {
            article.setTag(tag.trim());
        } else {
            tag = "";
        }

        if (StringUtils.isNotEmpty(con)) {
            article.setContent(con.trim());
        } else {
            con = "";
        }

        if (StringUtils.isNotEmpty(orn) && StringUtils.isNotEmpty(ort)) {
            if (orn.equals("h")) {
                article.setOrderByName("hit");
            } else if (orn.equals("u")) {
                article.setOrderByName("update_time");
            } else if (orn.equals("c")) {
                article.setOrderByName("create_time");
            }
            if (ort.equals("desc")) {
                article.setOrderByType("desc");
            } else if (ort.equals("asc")) {
                article.setOrderByType("asc");
            }
        } else {
            orn = "";
            ort = "";
        }
        Category category = new Category();
        Tags blockTag = new Tags();

        List<Article> articles = articleService.selectArticleListSimple(article);

        List<Category> categorieList = categoryService.selectCategoryList(category);
        List<Tags> tagsList = tagsService.selectTagsList(blockTag);

        PageInfo pageInfo = new PageInfo(articles);
        //
        model.addAttribute("articleList", articles);
        model.addAttribute("categorieList", categorieList);
        model.addAttribute("tagsList", tagsList);
//        model.addAttribute("simpleArticles", simpleArticles);

        //备注信息
        model.addAttribute("tit", tit);
        model.addAttribute("con", con);
        model.addAttribute("tag", tag);
        model.addAttribute("cat", cat);
        model.addAttribute("ort", ort);
        model.addAttribute("orn", orn);
        //分页信息
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());

        return PREFIX + "/" + getTheme() + "/search";
    }

    /**
     * 标签列表
     *
     * @param tagId
     * @param model
     * @return
     */
    @GetMapping("/tag/{tagId}")
    public String tag(@PathVariable("tagId") String tagId, Model model) {
        model.addAttribute("tagId", tagId);
        Tags tag = tagsService.selectTagsById(Long.valueOf(tagId));
        if (tag != null) {
            model.addAttribute("tagName", tag.getTagName());
        }
        Article form = new Article();
        form.setTag(tagId);
        model.addAttribute("pageUrl", "blog/tag/" + tagId);
        startPage();
        List<Article> articles = articleService.selectArticleList(form);
        PageInfo pageInfo = new PageInfo(articles);
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());
        model.addAttribute("articleList", articles);
        model.addAttribute("pageType", "tag");
        return PREFIX + "/" + getTheme() + "/tag";
    }

    /**
     * 留言
     *
     * @param model
     * @return
     */
    @GetMapping("/siteMsg")
    public String comment(Model model) {
        model.addAttribute("categoryId", "siteMsg");
        return PREFIX + "/" + getTheme() + "/siteMsg";
    }


    public AjaxResult articleView(HttpServletRequest request, String articleId) {
        if (StringUtils.isEmpty(articleId)) {
            return AjaxResult.error("系统错误!");
        }
        String ip = IpUtils.getIpAddr(request);
        Integer n =  articleViewCache.get(ip + "|" + articleId);
        if (n == null || n == 0) {
            articleService.articleLook(articleId);
            articleViewCache.put(ip + "|" + articleId, 1);
            return AjaxResult.success("浏览数+1");
        } else {
            articleViewCache.put(ip + "|" + articleId, n++);
            return AjaxResult.error("系统错误!");
        }
    }


    @PostMapping("/article/view")
    @ResponseBody
    public AjaxResult ajaxArticleView(HttpServletRequest request, String articleId) {
        if (StringUtils.isEmpty(articleId)) {
            return AjaxResult.error("系统错误!");
        }
        String ip = IpUtils.getIpAddr(request);
        Integer n = articleViewCache.get(ip + "|" + articleId);
        if (n == null || n == 0) {
            articleService.articleLook(articleId);
            articleViewCache.put(ip + "|" + articleId, 1);
            return AjaxResult.success("浏览数+1");
        } else {
            articleViewCache.put(ip + "|" + articleId, n++);
            return AjaxResult.error("系统错误!");
        }
    }

//    @PostMapping("/article/upVote")
//    @ResponseBody
//    public AjaxResult articleUpVote(HttpServletRequest request, String articleId) {
//        if (StringUtils.isEmpty(articleId)) {
//            return AjaxResult.error("系统错误!");
//        }
//        String ip = IpUtils.getIpAddr(request);
//        Integer n = articleUpVoteCache.get(ip + "|" + articleId);
//        if (n == null || n == 0) {
//            articleService.upVote(articleId);
//            articleUpVoteCache.put(ip + "|" + articleId, 1);
//            return AjaxResult.success("点赞数+1");
//        } else {
//            articleUpVoteCache.put(ip + "|" + articleId, n++);
//            return AjaxResult.error("系统错误!");
//        }
//    }

    @PostMapping("/resource/view")
    @ResponseBody
    public AjaxResult resourceView(HttpServletRequest request, String id) {
        if (StringUtils.isEmpty(id)) {
            return AjaxResult.error("系统错误!");
        }
        String ip = IpUtils.getIpAddr(request);
        Integer n = articleViewCache.get(ip + "|" + id);
        if (n == null || n == 0) {
            resourceService.resourceLook(id);
            articleViewCache.put(ip + "|" + id, 1);
            return AjaxResult.success("浏览数+1");
        } else {
            articleViewCache.put(ip + "|" + id, n++);
            return AjaxResult.error("系统错误!");
        }
    }

    @PostMapping("/resource/upVote")
    @ResponseBody
    public AjaxResult resourceUpVote(HttpServletRequest request, String id) {
        if (StringUtils.isEmpty(id)) {
            return AjaxResult.error("系统错误!");
        }
        String ip = IpUtils.getIpAddr(request);
        Integer n = articleUpVoteCache.get(ip + "|" + id);
        if (n == null || n == 0) {
            resourceService.upVote(id);
            articleUpVoteCache.put(ip + "|" + id, 1);
            return AjaxResult.success("点赞数+1");
        } else {
            articleUpVoteCache.put(ip + "|" + id, n++);
            return AjaxResult.error("系统错误!");
        }
    }

    @PostMapping("/comments")
    @ResponseBody
    public AjaxResult comments(String tid, String type) {
        if (StringUtils.isEmpty(tid) || StringUtils.isEmpty(type)) {
            return AjaxResult.error("参数错误!");
        }
        Comment form = new Comment();
        form.setTid(tid);
        form.setType(type);
        startPage();
        List<Comment> list = commentService.selectComments(form);
        Map<String, Object> data = new HashMap<>();
        data.put("total", new PageInfo(list).getTotal());
        data.put("rows", list);
        data.put("hasNextPage", new PageInfo(list).isHasNextPage());
        data.put("nextPage", new PageInfo(list).getNextPage());
        return AjaxResult.success(data);
    }

    @PostMapping("/comments/save")
    @ResponseBody
    //@CacheEvict(cacheNames = {"article"}, key = "#comment.tid")
    public AjaxResult saveComments(HttpServletRequest request, Comment comment) {
        if (StringUtils.isEmpty(comment.getUserName())) {
            return AjaxResult.error("请输入昵称!");
        }
//        if (StringUtils.isEmpty(comment.getQq())) {
//            return AjaxResult.error("请输入QQ!");
//        }
//        if (!comment.getQq().matches("[1-9][0-9]{4,11}")) {
//            return AjaxResult.error("QQ格式不合法!");
//        }
        comment.setIp(IpUtils.getIpAddr(request));
        comment.setCreateTime(new Date());
        comment.setStatus(0);//无需审核即可显示
        comment.setAvatar("http://q1.qlogo.cn/g?b=qq&nk=" + comment.getQq() + "&s=100");

        String siteUrl = configService.selectConfigByKey("site.url");
        int n = commentService.insertComment(comment);
        String articleUrl = siteUrl + "/blog/article/" + comment.getTid();
        Long pid = comment.getPid();
        //判断是否是子回复，
        if (n > 0 && pid > 0) {
            //如果是子回复，给评论的父id的邮箱发邮件。
            Comment parentComment = commentService.selectCommentById(pid);
            MailUtil.send(parentComment.getEmail(), "您的评论有回复啦，快来看看吧。",
                    "<h1>" + "您的评论有回复了" + "</h1>" + "</br>" +
                            "内容：<p>" + comment.getContent() + "+</p>" + "</br>" +
                            "详情：<a href=" + articleUrl + ">" + articleUrl + "</a>" + "</br>" +
                            "",
                    true);
        }
        //如果评论成功给网站所有者发邮件。
        if (n > 0) {
            MailAccount account = new MailAccount();
            MailUtil.send("yao.siyuan@qq.com", "有新评论啦",
                    "<h1>" + comment.getTid() + "</h1>" + "</br>" +
                            "内容：<p>" + comment.getContent() + "+</p>" + "</br>" +
                            "详情：<a href=" + articleUrl + ">" + articleUrl + "</a>" + "</br>" +
                            "",
                    true);
            return AjaxResult.success(comment);
        } else {
            return AjaxResult.error("评论失败!");
        }

    }

    @PostMapping("/comments/upVote")
    @ResponseBody
    public AjaxResult commentsUpVote(HttpServletRequest request, String commentId) {
        if (StringUtils.isEmpty(commentId)) {
            return AjaxResult.error("系统错误!");
        }
        String ip = IpUtils.getIpAddr(request);
        Integer n = commentUpVoteCache.get(ip + "|" + commentId);
        if (n == null || n == 0) {
            commentService.upVote(commentId);
            commentUpVoteCache.put(ip + "|" + commentId, 1);
            return AjaxResult.success("支持数+1");
        } else {
            commentUpVoteCache.put(ip + "|" + commentId, n++);
            return AjaxResult.error("系统错误!");
        }
    }


    /**
     * 导航
     *
     * @param model
     * @return
     */
    @GetMapping("/nav")
    public String nav(Model model) {
        model.addAttribute("categoryId", "nav");
//        CaffeineUtils.get()
        List<LinkType> linkTypeList = (List<LinkType>) blogCache.get(CacheConstant.KEY_LINK_TYPE_LIST);
        if (CollectionUtil.isEmpty(linkTypeList)) {
            LinkType form = new LinkType();
            form.setStatus(CmsConstants.STATUS_NORMAL);
            linkTypeList = linkTypeService.selectLinkTypeList(form);
            blogCache.put(CacheConstant.KEY_LINK_TYPE_LIST, linkTypeList);
        }
        for (LinkType type : linkTypeList) {
            List<Link> tempList = (List<Link>) blogCache.get(CacheConstant.KEY_LINK_LIST + type.getLinkType());
            if (CollectionUtil.isEmpty(tempList)) {
                Link tempForm = new Link();
                tempForm.setAuditState(CmsConstants.AUDIT_STATE_AGREE);
                tempForm.setLinkType(type.getLinkType());
                tempForm.setStatus(CmsConstants.STATUS_NORMAL);
                tempList = linkService.selectLinkList(tempForm);
                blogCache.put(CacheConstant.KEY_LINK_LIST + type.getLinkType(), tempList);
            }
            tempList = tempList.stream().limit(3).collect(Collectors.toList());
            type.setChildrenLinkList(tempList);
        }
        model.addAttribute("linkTypeList", linkTypeList);

        return "nav/theme/apex/navAll";
    }

    /**
     * 导航
     *
     * @param model
     * @return
     */
    @GetMapping("/nav/{type}")
    public String navByType(@PathVariable("type") String type, Model model) {
        model.addAttribute("categoryId", "nav");
        LinkType linkType = linkTypeService.selectLinkTypeByType(type);
        if (linkType == null) {
            throw new BusinessException("不存在的分类!");
        }
        model.addAttribute("linkType", linkType);

//        List<LinkType> linkTypeList = (List<LinkType>) blogCache.get(CacheConstant.KEY_LINK_TYPE_LIST);
//        if (CollectionUtil.isEmpty(linkTypeList)) {
//            LinkType form = new LinkType();
//            form.setStatus(CmsConstants.STATUS_NORMAL);
//            linkTypeList = linkTypeService.selectLinkTypeList(form);
//            blogCache.put(CacheConstant.KEY_LINK_TYPE_LIST, linkTypeList);
//        }
        //根据type查询子分类
        String parentType = linkType.getParentType();
        List<LinkType> childLinkTypeList = new ArrayList<>();
        if (StringUtils.isNotEmpty(parentType)) {
            //如果子分类是空，则是二级分类
            childLinkTypeList.add(linkType);
        } else {
            //如果是一级分类
            LinkType childtForm = new LinkType();
            childtForm.setParentType(type);
            childtForm.setStatus(CmsConstants.STATUS_NORMAL);
            childLinkTypeList = linkTypeService.selectLinkTypeList(childtForm);
        }
        //根据子分类查询链接
        //把链接放到子分类
        for (int i = 0; i < childLinkTypeList.size(); i++) {
            Link form = new Link();
            form.setAuditState(CmsConstants.AUDIT_STATE_AGREE);
            form.setLinkType(childLinkTypeList.get(i).getLinkType());
            form.setStatus(CmsConstants.STATUS_NORMAL);
            List<Link> linkList = linkService.selectLinkList(form);
            childLinkTypeList.get(i).setChildrenLinkList(linkList);
        }
        model.addAttribute("childLinkTypeList", childLinkTypeList);


//        model.addAttribute("linkTypeList", linkTypeList);
        Link form = new Link();
        form.setAuditState(CmsConstants.AUDIT_STATE_AGREE);
        form.setLinkType(type);
        form.setStatus(CmsConstants.STATUS_NORMAL);
        startPage();
        List<Link> linkList = linkService.selectLinkList(form);


        PageInfo pageInfo = new PageInfo(linkList);
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());
        model.addAttribute("linkList", linkList);

        return "nav/theme/apex/list_nav";
    }

    @GetMapping("/blogTheme")
    public String blogTheme(Model model) {
        BlogTheme form = new BlogTheme();
        startPage();
        List<BlogTheme> themes = blogThemeService.selectBlogThemeList(form);
        PageInfo pageInfo = new PageInfo(themes);
        model.addAttribute("total", pageInfo.getTotal());
        model.addAttribute("pageNo", pageInfo.getPageNum());
        model.addAttribute("pageSize", pageInfo.getPageSize());
        model.addAttribute("totalPages", pageInfo.getPages());
        model.addAttribute("hasPrevious", pageInfo.isHasPreviousPage());
        model.addAttribute("hasNext", pageInfo.isHasNextPage());
        model.addAttribute("currentPage", pageInfo.getPageNum());
        model.addAttribute("prePage", pageInfo.getPrePage());
        model.addAttribute("nextPage", pageInfo.getNextPage());
        model.addAttribute("navNums", pageInfo.getNavigatepageNums());
        model.addAttribute("themeList", themes);
        String currentTheme = blogThemeService.queryCurrentBlogTheme();
        model.addAttribute("currentTheme", currentTheme);
        return PREFIX + "/blogTheme";
    }


    /**
     * 获取文章资源密码
     */
    @PostMapping("/getArticlePassword")
    @ResponseBody
    public AjaxResult getArticlePassword(HttpServletRequest request, String articleId, String typePassword) {
        String articlePassword = articleService.selectArticlePasswordByArticleId(articleId);
        //判断密码是否相等
        if (articlePassword.equals(typePassword)) {
            //如果密码相等
            ArrayList rememberPasswordList = (ArrayList) request.getSession().getAttribute("rememberPasswordList");

            //判断是否为空
            if (CollectionUtils.isEmpty(rememberPasswordList)) {
                ArrayList rememberPasswordListFirst = new ArrayList();
                //如果为空,直接添加
                rememberPasswordListFirst.add(articleId);
                request.getSession().setAttribute("rememberPasswordList", rememberPasswordListFirst);
            } else {
                //如果不为空，判断是否已经存在
                if (!rememberPasswordList.contains(articleId)) {
                    //如果不存在，再添加进去
                    rememberPasswordList.add(articleId);
                }
            }
            Object article = articleService.selectArticleById(articleId);
            return AjaxResult.success("自己人", article);
        } else {
            return AjaxResult.error("密码不对");
        }
    }


    /**
     * 获取文章资源内容
     */
    @PostMapping("/resourceContent")
    @ResponseBody
    public AjaxResult resourceContent(HttpServletRequest request, String articleId) {
        return AjaxResult.success("操作成功", articleService.selectArticleResourceContentByArticleId(articleId));
    }

    /**
     * 获取文章资源内容
     */
    @PostMapping("/getResourcePassword")
    @ResponseBody
    public AjaxResult getResourcePassword(HttpServletRequest request, String typeResourcePassword) {
        String resourcePassword = configService.selectConfigByKey("resource_password");
        if (StringUtils.isNotBlank(resourcePassword)) {
            if (typeResourcePassword.equals(resourcePassword)) {
                //输入的密码一样
                request.getSession().setAttribute("resourcePasswordRemember", "true");
                return AjaxResult.success("操作成功");
            } else {
                return AjaxResult.error("验证码不对，，扫个码关注下很难嘛");
            }
        }

        return AjaxResult.error("未知错误");
    }

}
