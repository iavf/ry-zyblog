//package com.ruoyi.oss.service.impl;
//
//import com.ruoyi.oss.domain.SysOss;
//import com.ruoyi.oss.mapper.SysOssMapper;
//import com.ruoyi.oss.service.ISysOssService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.ArrayList;
//import java.util.List;
//
////import tk.mybatis.mapper.entity.Example;
////import tk.mybatis.mapper.entity.Example.Criteria;
//@Component
//@Transactional
//@Service("sysOssService")
//public class SysOssServiceImpl implements ISysOssService {
//    @Autowired
//    private SysOssMapper sysOssMapper;
//
//    /*
//     *
//     * @see
//     * com.zmr.wind.modules.sys.service.ISysOssService#getList(com.zmr.wind.
//     * modules.sys.entity.SysOss)
//     */
//    @Override
//    public List<SysOss> getList(SysOss sysOss) {
//        return  new ArrayList<>();
////        Example example = new Example(SysOss.class);
////        Criteria criteria = example.createCriteria();
////        if (StringUtils.isNotBlank(sysOss.getFileName()))
////        {
////            criteria.andLike("fileName", "%" + sysOss.getFileName() + "%");
////        }
////        if (StringUtils.isNotBlank(sysOss.getFileSuffix()))
////        {
////            criteria.andEqualTo("fileSuffix", sysOss.getFileSuffix());
////        }
////        if (StringUtils.isNotBlank(sysOss.getCreateBy()))
////        {
////            criteria.andLike("createBy", sysOss.getCreateBy());
////        }
////        return sysOssMapper.selectByExample(example);
//    }
//
//    /* (non-Javadoc)
//     * @see com.ruoyi.system.service.ISysOssService#save(com.ruoyi.system.domain.SysOss)
//     */
//    @Override
//    public int save(SysOss ossEntity) {
////        return sysOssMapper.insertSelective(ossEntity);
//        return 0;
//    }
//
//    /* (non-Javadoc)
//     * @see com.ruoyi.system.service.ISysOssService#findById(java.lang.Long)
//     */
//    @Override
//    public SysOss findById(Long ossId) {
////        return sysOssMapper.selectByPrimaryKey(ossId);
//        return  new SysOss();
//    }
//
//    /* (non-Javadoc)
//     * @see com.ruoyi.system.service.ISysOssService#update(com.ruoyi.system.domain.SysOss)
//     */
//    @Override
//    public int update(SysOss sysOss) {
////        return sysOssMapper.updateByPrimaryKeySelective(sysOss);
//        return 0;
//    }
//
//    /* (non-Javadoc)
//     * @see com.ruoyi.system.service.ISysOssService#deleteByIds(java.lang.String)
//     */
//    @Override
//    public int deleteByIds(String ids) {
////        return sysOssMapper.deleteByIds(ids);
//        return 0;
//    }
//}
