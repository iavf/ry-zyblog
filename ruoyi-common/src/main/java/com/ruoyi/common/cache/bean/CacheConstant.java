package com.ruoyi.common.cache.bean;

public class CacheConstant {


    /**
     * redis index1
     * 存储：文章分类缓存：CATEGORY_KEY
     */
    //key的统一引号
    public static final int REDIS_INDEX_ONE = 1;


    /**
     * redis index2
     * 存储：标签分类缓存：TAG_KEY
     */
    //key的统一引号
    public static final int REDIS_INDEX_TWO = 2;


    /**
     * redis index3
     * 存储：
     */
    //key的统一引号
    public static final int REDIS_INDEX_THREE= 3;



    /**
     * 缓存名称
     */
    //key的统一引号
    public static final String YH = "'";

    //key的统一引号
    public static final String D = ".";

    //key的统一中间分隔符
    public static final String SUFFIX = "_";

    //博客财务缓存
    public static final String MONEY_CACHE_NAME = "money";
    //页面账务信息
    public static final String MONEY_GETMONEYS_KEY = "getMoneys";
    //页面支出缓存
    public static final String MONEY_SELECTPAYLIST_KEY = "selectPayList";
    //页面捐贈缓存
    public static final String MONEY_SELECTDONATELIST_KEY = "selectDonateList";



    //博客信息缓存NAME
    public static final String BLOGINFO_CACHE_NAME = "blogInfo";

    //分类树
    public static final String BLOGINFO_SELECTCATEGORYTREE_KEY = "selectCategoryTree";
    //所有文章简要信息key
    public static final String ALL_ARTICLE_SIMPLE_INFO = "selectSimpleArticles";
    public static final String ALL_ARTICLE_SIMPLE_INFO_JSON = "selectSimpleArticlesSelectJson";
    //博客信息缓存键key
    public static final String BLOGINFO_CACHE_KEY = "selectRecentArticleListByCreateTime";
    //最新创建文章缓存key
    public static final String SELECT_RECENT_ARTICLE_LIST_BY_CREATE_TIME_KEY = "selectRecentArticleListByCreateTime";
    //最热门文章缓存key
    public static final String GET_HOT_ARTICLE_BY_LIMIT_KEY = "getHotArticleByLimit";
    //推荐文章缓存key
    public static final String RECOMMEND_LIST_CACHE_KEY = "recommendedList";
    //最新评论缓存key
    public static final String NEW_COMMENTS_CACHE_KEY = "newComments";
    //最新更新文章缓存KEY
    public static final String SELECT_RECENT_LIST_BY_UP_DATETIME_CACHE_KEY = "selectrecentListByUpdatetime";



    //文章分类缓存name
    public static final String CATEGORY_CACHENAME = "category";
    //文章分类缓存键，name blog，key category_*
    public static final String CATEGORY_KEY = CATEGORY_CACHENAME + SUFFIX;


    //文章标签缓存name
    public static final String Tag_CACHE_NAME = "tag";
    //文章标签放入redis 的 index
    public static final int Tag_CACHE_INDEX = 3;
    //文章标签缓存键，name blog，key category_*
    public static final String TAG_CACHE_KEY = Tag_CACHE_NAME + SUFFIX;

    //系统配置缓存
    public static final String CONFIG_CACHE_NAME = "config";
    //系统配置缓存键，name blog，key category_*
    public static final String CONFIG_CACHE_KEY = CONFIG_CACHE_NAME + SUFFIX;


    //文章缓存
    public static final String ARTICLE_CACHE_NAME = "article.";
    //文章缓存键，name blog，key category_*
    public static final String ARTICLE_CACHE_KEY = ARTICLE_CACHE_NAME + SUFFIX;

    public static final String KEY_LINK_TYPE_LIST = "linkTypeList";
    public static final String KEY_LINK_LIST = "linkList_";


    //打赏缓存
    public static final String DONATE = "money";
}
