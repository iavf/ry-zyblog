var web_footer = ` <div class="am-panel-group index-banner-one-slide-web" id="accordion">
						<div class="am-panel am-panel-default">
						<div class="am-panel-hd">
							<h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-3'}">关于用友</h4>
							<span><img src="/static_new/img/icon/xiala.png" alt=""></span>
						</div>
						<div id="do-not-say-3" class="am-panel-collapse am-collapse">
							<div class="am-panel-bd">
							<ul>
								<li><a target="_blank" href="https://www.yonyou.com/yy/guanyu.html?id=0&v=0.0.7">了解用友</a></li>
								<li><a target="_blank" href="https://www.yonyou.com/yy/guanyu.html?id=3&v=0.0.7">社会责任</a></li>
								<li><a target="_blank" href="http://career.yonyou.com/">加入用友</a></li>
								<li><a target="_blank" href="https://www.yonyou.com/yy/lianxiyonyou.html?v=0.0.24">全球办公室</a></li>
								<li><a target="_blank" href="https://www.yonyou.com/yy/guanyu.html?id=2&v=0.0.7">投资者关系</a></li>
								<li><a target="_blank" href="https://www.yonyou.com/yy/dem.html">数智企业体验馆</a></li>
								
								
							</ul>
							</div>
						</div>
						</div>
						<div class="am-panel am-panel-default">
						<div class="am-panel-hd">
							<h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-4'}">资讯&活动</h4>
							<span><img src="/static_new/img/icon/xiala.png" alt=""></span>
						</div>
						<div id="do-not-say-4" class="am-panel-collapse am-collapse">
							<div class="am-panel-bd">
							<ul>
								<li><a target="_blank" href="http://subject.yonyou.com/event">最新活动</a></li>
								<li><a href="/news/news.html">资讯发布</a></li>
								<li><a href="/news/news_media.html">媒体报道</a></li>
								<li><a href="/news/news_video.html">视频中心</a></li>
								<li><a href="/news/news_social.html">社交媒体</a></li>
							</ul>
							</div>
						</div>
						</div>
						<div class="am-panel am-panel-default">
							<div class="am-panel-hd">
							<h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-5'}">案例&资料</h4>
							<span><img src="/static_new/img/icon/xiala.png" alt=""></span>
							</div>
							<div id="do-not-say-5" class="am-panel-collapse am-collapse">
							<div class="am-panel-bd">
								<ul>
									<li><a href="/case/case.html">案例中心</a></li>
									<li><a target="_blank" href="https://www.yonyoucloud.com/caselist.php">解决方案</a></li>
									<li><a target="_blank" href="javascript:;">案例下载</a></li>
									<li><a target="_blank" href="javascript:;">IDC报告</a></li>
									<li><a href="/news/news_base.html">资料库</a></li>
								</ul>
							</div>
							</div>
						</div>
						<div class="am-panel am-panel-default">
							<div class="am-panel-hd">
								<h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-6'}">服务支持</h4>
								<span><img src="/static_new/img/icon/xiala.png" alt=""></span>
							</div>
							<div id="do-not-say-6" class="am-panel-collapse am-collapse">
								<div class="am-panel-bd">
								<ul>
									<li><a target="_blank"  href="http://nc.yonyou.com/services.php">iSM  Cloud</a></li>
									<li><a target="_blank"  href="http://fwq.yonyou.com/up_service/index.php?r=up_shop/index">U8 服务圈</a></li>
									<li><a target="_blank" href="https://euc.yonyoucloud.com/register?sysid=market&service=https://market.yonyoucloud.com/market/invite/invite_h5.html?inviterCode=YN4414">加入合作伙伴</a></li>
									<li><a href="https://service.chanjet.com">小微企业服务支持</a></li>
								</ul>
								</div>
							</div>
						</div>
						<div class="am-panel am-panel-default">
							<div class="am-panel-hd">
								<h4 class="am-panel-title" data-am-collapse="{parent: '#accordion', target: '#do-not-say-7'}">热销产品</h4>
								<span><img src="/static_new/img/icon/xiala.png" alt=""></span>
							</div>
							<div id="do-not-say-7" class="am-panel-collapse am-collapse">
								<div class="am-panel-bd">
								<ul>
									<li><a target="_blank" href="https://ec.diwork.com/">友空间</a></li>
									<li><a target="_blank" href="https://ncc.yonyou.com/">NC Cloud</a></li>
									<li><a target="_blank" href="https://ncc.yonyou.com/">Yonsuit</a></li>
									<li><a target="_blank" href="https://www.yonyoucloud.com/product.php?id=7">采购寻源</a></li>
									<li><a target="_blank" href="https://www.yonyoucloud.com/product.php?id=3">数字营销</a></li>
								</ul>
								</div>
							</div>
						</div>
						</div>`;
var footer =
			'<div class="footer">'+
				'<div class="container">'+
					'<div class="footer-top">'+
						web_footer+
						'<div class="footer-one-cont">'+
							'<ul class="footerTwo" style="color: #A5A5A5;">'+
								'<li class="f_logo"><span><img src="/static_new/img/icon/logo_bai.png" /></span></li>'+
								'<li class="phone">联系电话<br/> 010-86396688</li>'+
								'<li class="phone">服务热线<br/> 400-6600-588</li>'+
								'<li class="phone">公司地址<br/> 北京市海淀区北清路68号用友产业园</li>'+
								'<li class="find"><a target="_blank" href="https://www.yonyou.com/yy/lianxiyonyou.html?v=0.0.24">查找当地号码<a/></li>'+
								'<li class="kg"></li>'+
								'<li class="find"><a href="./contact.html">联系我们<a/></li>'+
								'<li class="kg"></li>'+
								'<li class="web_s">关注用友</li>'+
								'<li class="icon">'+
									'<div class="w_x_icon"><img class="w_x_icon1" src="/static_new/img/icon/weibo.png" alt=""/><img class="w_x_icon2" src="/static_new/img/icon/weixin.png" alt=""/></div>'+
									'<div class="WeChatqrcode"><img src="/static_new/img/WeChat.jpg" alt=""></div>'+
									'<div class="microblogqrcode"><img src="/static_new/img/microblog.jpg" alt=""></div>'+
								'</li>'+
							'</ul>'+
						'</div>'+
						'<div class="footer-right index-banner-one-slide-pc">'+
							'<ul>'+
								'<li><span>关于用友</span></li>'+
								'<li><a target="_blank" href="https://www.yonyou.com/yy/guanyu.html?id=0&v=0.0.7">了解用友</a></li>'+
								'<li><a target="_blank" href="https://www.yonyou.com/yy/guanyu.html?id=3&v=0.0.7">社会责任</a></li>'+
								'<li><a target="_blank" href="http://career.yonyou.com/">加入用友</a></li>'+
								'<li><a target="_blank" href="https://www.yonyou.com/yy/lianxiyonyou.html?v=0.0.24">全球办公室</a></li>'+
								'<li><a target="_blank" href="https://www.yonyou.com/yy/guanyu.html?id=2&v=0.0.7">经营公告</a></li>'+
								'<li><a target="_blank" href="https://www.yonyou.com/yy/dem.html">数智企业体验馆</a></li>'+
							'</ul>'+
							'<ul>'+
								'<li><span>资讯&活动</span></li>'+
								'<li><a target="_blank" href="http://subject.yonyou.com/event">最新活动</a></li>'+
								'<li><a href="/news/news.html">资讯发布</a></li>'+
								'<li><a href="/news/news_media.html">媒体报道</a></li>'+
								'<li><a href="/news/news_video.html">用友视讯</a></li>'+
								'<li><a href="/news/news_social.html">社交媒体</a></li>'+
							'</ul>'+
							'<ul>'+
								'<li><span>案例&资料</span></li>'+
								'<li><a href="/case/case.html">案例中心</a></li>'+
								'<li><a target="_blank" href="https://www.yonyoucloud.com/caselist.php">解决方案</a></li>'+
								'<li><a href="/case/case.html">案例下载</a></li>'+
								'<li><a href="javascript:contactUs(1);">IDC报告</a></li>'+
								'<li><a href="/news/news_base.html">资料库</a></li>'+
							'</ul>'+
							'<ul>'+
								'<li><span>服务支持</span></li>'+
								'<li><a target="_blank" href="http://nc.yonyou.com/services.php">iSM Cloud</a></li>'+
								'<li><a target="_blank" href="http://fwq.yonyou.com/up_service/index.php?r=up_shop/index">U8 服务圈</a></li>'+
								'<li><a target="_blank" href="https://euc.yonyoucloud.com/register?sysid=market&service=https://market.yonyoucloud.com/market/invite/invite_h5.html?inviterCode=YN4414">云市场入驻</a></li>'+
								'<li><a target="_blank" href="https://market.yonyoucloud.com/">加入合作伙伴</a></li>'+
								'<li><a target="_blank" href="https://service.chanjet.com/">小微企业服务支持</a></li>'+
							'</ul>'+
							'<ul>'+
								'<li><span>热门产品</span></li>'+
								'<li><a target="_blank" href="https://ec.diwork.com/">友空间</a></li>'+
								'<li><a target="_blank" href="https://ncc.yonyou.com/">NC Cloud</a></li>'+
								'<li><a target="_blank" href="https://yonsuite.diwork.com/">Yonsuite</a></li>'+
								'<li><a target="_blank" href="https://www.yonyoucloud.com/product.php?id=7">采购寻源</a></li>'+
								'<li><a target="_blank" href="https://www.yonyoucloud.com/product.php?id=3">数字营销</a></li>'+
							'</ul>'+
						'</div>'+
				'</div>'+
			'</div>'+
			'<div class="footer-bottom">'+
                '<div class="container">'+
                '<div class="am-g index-banner-one-slide-pc">'+
					'<ul class="fl am-u-sm-4">'+
						'<li><a href="http://register.yonyou.com/chaxun.aspx" target="_blank">产品注册与防伪查询</a><span>|</span></li>'+
						'<li><a href="http://nc.yonyou.com/about/shengming.aspx" target="_blank">法律声明</a><span>|</span></li>'+
						'<li><a href="http://park.yonyou.com/luxian.asp" target="_blank">用友地图</a></li>'+
					'</ul>'+
                    '<div class="copyright am-u-sm-8 fl">版权所有：用友网络科技股份有限公司©2015 京ICP备05007539号-7  京公网安备11010802021935号</div>'+
                    '</div>'+
				'</div>'+
				'<div class="index-banner-one-slide-web">'+
					'<ul>'+
						'<li><a href="http://register.yonyou.com/chaxun.aspx" target="_blank">产品注册与防伪查询</a><span>|</span><a href="http://park.yonyou.com/luxian.asp" target="_blank">用友地图</a><a href="http://nc.yonyou.com/about/shengming.aspx" target="_blank">法律声明</a><span>|</span></li>'+
								'<li>版权所有：用友网络科技股份有限公司©2015 京ICP备05007539号-7 </li>'+
								'<li> 京公网安备11010802021935号</li>'+
					'</ul>'+
                    '</div>'+
				'</div>'+
			'</div>'+
		'</div>';

$(function() {
  $("#footerjs").html(footer);
})
