var argonConfig = {
    wp_path: "/",
    zoomify: {
        duration: 350,
        easing: "cubic-bezier(0.4, 0, 0, 1)",
        scale: 0.9
    },
    pangu: "false",
    lazyload: {
        threshold: 800,
        effect: "fadeIn"
    },
    fold_long_comments: true
}