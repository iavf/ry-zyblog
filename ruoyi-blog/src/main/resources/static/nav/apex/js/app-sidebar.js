/*
 =========================================================
 * Apex Angular 4 Bootstrap theme - V1.0
 =========================================================

 * Product Page: https://www.pixinvent.com/product/apex
 * Copyright 2017 Pixinvent Creative Studio (https://www.pixinvent.com)

 =========================================================
*/
$(document).ready(function () {

    $(".search-type").perfectScrollbar();
    $(".left-side .side-content").perfectScrollbar();
    $(".my-links-box").perfectScrollbar();
    if ($(".navigation-main").data("scroll-to-active") === true) {
        var h;
        if ($(".navigation-main").find("li.active").parents("li").length > 0) {
            h = $(".navigation-main").find("li.active").parents("li").last().position()
        } else {
            h = $(".navigation-main").find("li.active").position()
        }
        setTimeout(function () {
            if (h !== undefined) {
                $(".sidebar-content.ps-container").animate({scrollTop: h.top}, 300)
            }
        }, 300)
    }
    var a = $(".app-sidebar"), b = $(".sidebar-content"), c = a.data("image"), d = $(".sidebar-background"),
        e = $(".wrapper");
    b.perfectScrollbar();
    if (d.length !== 0 && c !== undefined) {
        d.css("background-image", 'url("' + c + '")')
    }
    if (!e.hasClass("nav-collapsed")) {
        b.find("li.active").parents("li").addClass("open")
    }
    setTimeout(function () {
        $(".row.match-height").each(function () {
            $(this).find(".card").not(".card .card").matchHeight()
        })
    }, 500);
    b.on("click", ".navigation li a", function () {
        var i = $(this), j = i.parent("li");
        if (j.hasClass("has-sub") && j.hasClass("open")) {
            f(j)
        } else {
            if (j.hasClass("has-sub")) {
                g(j)
            }
            if (b.data("collapsible")) {
                return false
            } else {
                openListItems = j.siblings(".open");
                f(openListItems);
                j.siblings(".open").find("li.open").removeClass("open")
            }
        }
    });

    function f(i, k) {
        var j = i.children("ul");
        j.show().slideUp(200, function () {
            $(this).css("display", "");
            $(this).find("> li").removeClass("is-shown");
            i.removeClass("open");
            if (k) {
                k()
            }
        })
    }

    function g(j, l) {
        var k = j.children("ul");
        var i = k.children("li").addClass("is-hidden");
        j.addClass("open");
        k.hide().slideDown(200, function () {
            $(this).css("display", "");
            if (l) {
                l()
            }
        });
        setTimeout(function () {
            i.addClass("is-shown");
            i.removeClass("is-hidden")
        }, 0)
    }

    $(".logo-text").on("click", function () {
        var j = b.find("li.open.has-sub"), i = b.find("li.active");
        if (j.hasClass("has-sub") && j.hasClass("open")) {
            f(j);
            j.removeClass("open");
            if (i.closest("li.has-sub")) {
                openItem = i.closest("li.has-sub");
                g(openItem);
                openItem.addClass("open")
            }
        } else {
            if (i.closest("li.has-sub")) {
                openItem = i.closest("li.has-sub");
                g(openItem);
                openItem.addClass("open")
            }
        }
    });
    $(".nav-toggle").on("click", function () {
        var i = $(this), k = i.find(".toggle-icon"), j = k.attr("data-toggle");
        compact_menu_checkbox = $(".cz-compact-menu");
        if (j === "expanded") {
            e.addClass("nav-collapsed");
            $(".nav-toggle").find(".toggle-icon").removeClass("ft-toggle-right").addClass("ft-toggle-left");
            k.attr("data-toggle", "collapsed");
            if (compact_menu_checkbox.length > 0) {
                compact_menu_checkbox.prop("checked", true)
            }
        } else {
            e.removeClass("nav-collapsed menu-collapsed");
            $(".nav-toggle").find(".toggle-icon").removeClass("ft-toggle-left").addClass("ft-toggle-right");
            k.attr("data-toggle", "expanded");
            if (compact_menu_checkbox.length > 0) {
                compact_menu_checkbox.prop("checked", false)
            }
        }
    });
    a.on("mouseenter", function () {
        if (e.hasClass("nav-collapsed")) {
            e.removeClass("menu-collapsed");
            var i = $(".navigation li.nav-collapsed-open"), j = i.children("ul");
            j.hide().slideDown(300, function () {
                $(this).css("display", "")
            });
            b.find("li.active").parents("li").addClass("open");
            i.addClass("open").removeClass("nav-collapsed-open")
        }
    }).on("mouseleave", function (k) {
        if (e.hasClass("nav-collapsed")) {
            e.addClass("menu-collapsed");
            var i = $(".navigation li.open"), j = i.children("ul");
            i.addClass("nav-collapsed-open");
            j.show().slideUp(300, function () {
                $(this).css("display", "")
            });
            i.removeClass("open")
        }
    });
    if ($(window).width() < 992) {
        a.addClass("hide-sidebar");
        e.removeClass("nav-collapsed menu-collapsed")
    }
    $(window).resize(function () {
        if ($(window).width() < 992) {
            a.addClass("hide-sidebar");
            e.removeClass("nav-collapsed menu-collapsed")
        }
        if ($(window).width() > 992) {
            a.removeClass("hide-sidebar");
            if ($(".toggle-icon").attr("data-toggle") === "collapsed" && e.not(".nav-collapsed menu-collapsed")) {
                e.addClass("nav-collapsed menu-collapsed")
            }
        }
    });
    $(document).on("click", ".navigation li:not(.has-sub)", function () {
        if ($(window).width() < 992) {
            a.addClass("hide-sidebar")
        }
    });
    $(document).on("click", ".logo-text", function () {
        if ($(window).width() < 992) {
            a.addClass("hide-sidebar")
        }
    });
    $(".navbar-toggle").on("click", function (i) {
        i.stopPropagation();
        a.toggleClass("hide-sidebar")
    });
    $("html").on("click", function (i) {
        if ($(window).width() < 992) {
            if (!a.hasClass("hide-sidebar") && a.has(i.target).length === 0) {
                a.addClass("hide-sidebar")
            }
        }
    });
    $("#sidebarClose").on("click", function () {
        a.addClass("hide-sidebar")
    });
    $(".noti-list").perfectScrollbar();
    $(".apptogglefullscreen").on("click", function (i) {
        if (typeof screenfull != "undefined") {
            if (screenfull.enabled) {
                screenfull.toggle()
            }
        }
    });
    if (typeof screenfull != "undefined") {
        if (screenfull.enabled) {
            $(document).on(screenfull.raw.fullscreenchange, function () {
                if (screenfull.isFullscreen) {
                    $(".apptogglefullscreen").find("i").toggleClass("ft-minimize ft-maximize")
                } else {
                    $(".apptogglefullscreen").find("i").toggleClass("ft-maximize ft-minimize")
                }
            })
        }
    }
});